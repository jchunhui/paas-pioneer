﻿namespace Paas.Pioneer.User.Domain.Shared.UserInfo;

public class UserInfoModel
{
    public string NickName { get; set; }

    public byte Gender { get; set; }

    public string Language { get; set; }

    public string City { get; set; }

    public string Province { get; set; }

    public string Country { get; set; }

    public string AvatarUrl { get; set; }
}