﻿using System.ComponentModel;

namespace Paas.Pioneer.User.Domain.Shared.Enum;

public enum EWalletRecordType
{
    /// <summary>
    /// 收入
    /// </summary>
    [Description("收入")]
    Income = 1,

    /// <summary>
    /// 支出
    /// </summary>
    [Description("支出")]
    Expend = 2,
    
    /// <summary>
    /// 赠送
    /// </summary>
    [Description("赠送")]
    Give=3
}