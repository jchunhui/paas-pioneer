﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp.AspNetCore.Mvc;

namespace Paas.Pioneer.User.HttpApi.Controllers.AuthCode
{
    [Route("/api/user/[controller]")]
    public class AuthCodeController : AbpController
    {
        [HttpGet]
        public async Task GetAuthCodeAsync()
        {
            await Task.Delay(50);
        }
    }
}