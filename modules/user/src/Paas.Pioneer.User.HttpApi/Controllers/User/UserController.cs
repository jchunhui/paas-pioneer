using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Paas.Pioneer.Domain.Shared.Configs;
using Paas.Pioneer.Domain.Shared.Helpers;
using Paas.Pioneer.User.Application.Contracts.User;
using Paas.Pioneer.User.Application.Contracts.User.Dtos;
using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.AspNetCore.Mvc;

namespace Paas.Pioneer.User.HttpApi.Controllers.User
{
    [Route("/api/user/[controller]")]
    [Authorize]
    public class UserController : AbpController
    {
        private readonly IUserInfoService _userInfoService;
        private readonly UploadConfig _uploadConfig;
        private readonly UploadHelper _uploadHelper;

        public UserController(
            IUserInfoService userInfoService,
            UploadConfig uploadConfig,
            UploadHelper uploadHelper
        )
        {
            _userInfoService = userInfoService;
            _uploadConfig = uploadConfig;
            _uploadHelper = uploadHelper;
        }

        [HttpPut("sync-info")]
        [AllowAnonymous]
        public async Task SyncUserWeChatInfoAsync([FromBody] SyncUserWeChatInfoInput input)
        {
            await _userInfoService.SyncUserWeChatInfoAsync(input);
        }

        [HttpPut("avatar")]
        public async Task UpdateAvatarUrlAsync([FromBody] UpdateAvatarUrlInput input)
        {
            await _userInfoService.UpdateAvatarUrlAsync(input);
        }

        [HttpPut("name")]
        public async Task UpdateNameAsync([FromBody] UpdateNameInput input)
        {
            await _userInfoService.UpdateNameAsync(input);
        }

        [HttpPut("mobile")]
        public async Task UpdateMobileAsync([FromBody] UpdateMobileInput input)
        {
            await _userInfoService.UpdateMobileAsync(input);
        }

        [HttpPut("password")]
        [AllowAnonymous]
        public async Task UpdatePasswordAsync([FromBody] UpdatePasswordInput input)
        {
            await _userInfoService.UpdatePasswordAsync(input);
        }

        /// <summary>
        /// 上传头像
        /// </summary>
        /// <param name="file">文件流</param>
        /// <returns></returns>
        [HttpPost("upload-avatar")]
        public async Task<string> AvatarUpload([FromForm] IFormFile file)
        {
            var config = _uploadConfig.Avatar;
            var res = await _uploadHelper.UploadAsync(file, config, new { CurrentUser.Id });
            if (res.Success)
            {
                return HttpContext.Request.Scheme + "://" + HttpContext.Request.Host + res.Data.FileRequestPath;
            }

            throw new BusinessException(res.Msg ?? "上传失败！");
        }
    }
}