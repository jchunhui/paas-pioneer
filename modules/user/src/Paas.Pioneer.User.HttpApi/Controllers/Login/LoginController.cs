using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Paas.Pioneer.User.Application.Contracts.Login;
using Paas.Pioneer.User.Application.Contracts.Login.Dtos;
using Volo.Abp.AspNetCore.Mvc;

namespace Paas.Pioneer.User.HttpApi.Controllers.Login
{
    [Route("/api/user/[controller]")]
    [Authorize]
    public class LoginController : AbpController
    {
        private readonly ILoginService _service;

        public LoginController(ILoginService service)
        {
            _service = service;
        }

        [HttpPost]
        [AllowAnonymous]
        public Task<LoginOutput> LoginAsync([FromBody] LoginInput input)
        {
            return _service.LoginAsync(input);
        }

        [HttpPost]
        [Route("refresh-token")]
        [AllowAnonymous]
        public Task<string> RefreshAsync([FromBody] RefreshTokenInput input)
        {
            return _service.RefreshTokenAsync(input);
        }

        [HttpPost("mobile-register")]
        [AllowAnonymous]
        public async Task MobileRegisterAsync([FromBody] MobileRegisterInput input)
        {
            await _service.MobileRegisterAsync(input);
        }

        [HttpPost("account")]
        [AllowAnonymous]
        public async Task<LoginOutput> AccountLoginAsync([FromBody] AccountLoginInput input)
        {
            return await _service.AccountLoginAsync(input);
        }

        [HttpPost("token-verify")]
        public async Task TokenVerifyAsync()
        {
            await Task.CompletedTask;
        }
    }
}