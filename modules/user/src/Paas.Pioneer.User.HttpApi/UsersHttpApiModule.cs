﻿using Paas.Pioneer.User.Application.Contracts;
using Senparc.Weixin.Sample.MP;
using Volo.Abp;
using Volo.Abp.FeatureManagement;
using Volo.Abp.Localization;
using Volo.Abp.Modularity;
using Volo.Abp.TenantManagement;
using Senparc.Weixin.AspNet;
using Senparc.Weixin.MP.MessageHandlers.Middleware;

namespace Paas.Pioneer.User.HttpApi
{
    [DependsOn(
        typeof(UsersApplicationContractsModule),
        typeof(AbpTenantManagementHttpApiModule),
        typeof(AbpFeatureManagementHttpApiModule)
        )]
    public class UsersHttpApiModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            ConfigureLocalization();
        }

        public override void OnApplicationInitialization(ApplicationInitializationContext context)
        {
            var app = context.GetApplicationBuilder();
            var env = context.GetEnvironment();

            //#region 使用 MessageHadler 中间件，用于取代创建独立的 Controller

            ////MessageHandler 中间件介绍：https://www.cnblogs.com/szw/p/Wechat-MessageHandler-Middleware.html
            ////使用公众号的 MessageHandler 中间件（不再需要创建 Controller）
            //app.UseMessageHandlerForMp("/WeixinAsync", CustomMessageHandler.GenerateMessageHandler,
            //    options => { options.AccountSettingFunc = context => Senparc.Weixin.Config.SenparcWeixinSetting; });

            //#endregion
        }

        private void ConfigureLocalization()
        {
            Configure<AbpLocalizationOptions>(options =>
            {

            });
        }
    }
}
