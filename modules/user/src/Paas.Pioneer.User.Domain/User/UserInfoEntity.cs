using System;
using System.ComponentModel.DataAnnotations.Schema;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using Paas.Pioneer.Domain;
using Paas.Pioneer.User.Domain.Shared.UserInfo;

namespace Paas.Pioneer.User.Domain.User;

/// <summary>
/// 用户信息
/// </summary>
[Comment("用户信息")]
[Table("User_Info")]
public class UserInfoEntity : BaseEntity
{
    /// <summary>
    /// 名称
    /// </summary>
    [Comment("名称")]
    [Column("Name", TypeName = "varchar(50)")]
    [CanBeNull]
    public virtual string Name { get; set; }

    /// <summary>
    /// 名称
    /// </summary>
    [Comment("名称")]
    [Column("NickName", TypeName = "varchar(50)")]
    [CanBeNull]
    public virtual string NickName { get; set; }

    /// <summary>
    /// 手机号
    /// </summary>
    [Comment("手机号")]
    [Column("Mobile", TypeName = "varchar(11)")]
    public string Mobile { get; set; }

    /// <summary>
    /// 密码
    /// </summary>
    [Comment("密码")]
    [Column("PassWord", TypeName = "varchar(30)")]
    public string PassWord { get; set; }

    /// <summary>
    /// 性别
    /// </summary>
    [Comment("性别")]
    public virtual byte Gender { get; set; }

    /// <summary>
    /// 生日
    /// </summary>
    [Comment("生日")]
    public DateTime? Birthday { get; set; }

    /// <summary>
    /// 头像
    /// </summary>
    [Comment("头像")]
    [Column("AvatarUrl", TypeName = "varchar(500)")]
    [CanBeNull]
    public virtual string AvatarUrl { get; set; }

    /// <summary>
    /// 语言
    /// </summary>
    [Comment("语言")]
    [Column("Language", TypeName = "varchar(50)")]
    [CanBeNull]
    public virtual string Language { get; set; }

    /// <summary>
    /// 城市
    /// </summary>
    [Comment("城市")]
    [Column("City", TypeName = "varchar(50)")]
    [CanBeNull]
    public virtual string City { get; set; }

    /// <summary>
    /// 省份
    /// </summary>
    [Comment("城市")]
    [Column("Province", TypeName = "varchar(50)")]
    [CanBeNull]
    public virtual string Province { get; set; }

    /// <summary>
    /// 市
    /// </summary>
    [Comment("市")]
    [Column("Country", TypeName = "varchar(50)")]
    [CanBeNull]
    public virtual string Country { get; set; }
    
    /// <summary>
    /// 钱包金额
    /// </summary>
    [Comment("钱包金额")]
    [Column("WalletMoney", TypeName = "decimal(5, 2)")]
    public decimal WalletMoney { get; set; }
    
    /// <summary>
    /// 赠送金额
    /// </summary>
    [Comment("赠送金额")]
    [Column("GiveMoney", TypeName = "decimal(5, 2)")]
    public decimal GiveMoney { get; set; }

    public UserInfoEntity()
    {

    }

    public UserInfoEntity(Guid id)
    {
        Id = id;
    }

    public UserInfoEntity(Guid id,
        Guid? tenantId,
        UserInfoModel model)
    {
        Id = id;
        TenantId = tenantId;
        UpdateInfo(model);
    }

    public void UpdateInfo(UserInfoModel model)
    {
        Gender = model.Gender;
        AvatarUrl = model.AvatarUrl;
    }
}
