using Paas.Pioneer.User.Domain.BaseExtensions;
using System;
using Volo.Abp.Domain.Repositories;

namespace Paas.Pioneer.User.Domain.User;

public interface IUserInfoRepository : IRepository<UserInfoEntity, Guid>, IBaseExtensionRepository<UserInfoEntity>
{
}