﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Paas.Pioneer.Domain;
using Paas.Pioneer.User.Domain.Shared.Enum;

namespace Paas.Pioneer.User.Domain.Wallet;

/// <summary>
/// 钱包记录
/// </summary>
[Comment("钱包记录")]
[Table("User_WalletRecord")]
public class WalletRecordEntity : BaseEntityNotTenantCore
{
    /// <summary>
    /// 用户id
    /// </summary>
    [Comment("用户id")]
    [Column("UserId", TypeName = "char(36)")]
    public Guid UserId { get; set; }

    /// <summary>
    /// 钱包记录类型
    /// </summary>
    [Comment("微信app类型")]
    [Column("Type", TypeName = "int")]
    public virtual EWalletRecordType EWalletRecordType { get; set; }

    /// <summary>
    /// 金额
    /// </summary>
    [Comment("金额")]
    [Column("Money", TypeName = "decimal(5, 2)")]
    public decimal Money { get; set; }
}