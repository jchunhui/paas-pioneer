using System;
using Paas.Pioneer.User.Domain.User;
using Paas.Pioneer.User.EntityFrameworkCore.BaseExtensions;
using Paas.Pioneer.User.EntityFrameworkCore.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace Paas.Pioneer.User.EntityFrameworkCore.User
{
    public class UserInfoRepository : BaseExtensionsRepository<UserInfoEntity>, IUserInfoRepository
    {
        public UserInfoRepository(IDbContextProvider<UsersDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}