﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Paas.Pioneer.User.EntityFrameworkCore.Migrations
{
    public partial class updateUserAvatarUrlLength : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "AvatarUrl",
                table: "User_Info",
                type: "varchar(500)",
                nullable: true,
                comment: "头像",
                oldClrType: typeof(string),
                oldType: "varchar(50)",
                oldNullable: true,
                oldComment: "头像")
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "AvatarUrl",
                table: "User_Info",
                type: "varchar(50)",
                nullable: true,
                comment: "头像",
                oldClrType: typeof(string),
                oldType: "varchar(500)",
                oldNullable: true,
                oldComment: "头像")
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");
        }
    }
}
