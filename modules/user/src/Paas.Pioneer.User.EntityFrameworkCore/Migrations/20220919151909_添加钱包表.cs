﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Paas.Pioneer.User.EntityFrameworkCore.Migrations
{
    public partial class 添加钱包表 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "GiveMoney",
                table: "User_Info",
                type: "decimal(5,2)",
                nullable: false,
                defaultValue: 0m,
                comment: "赠送金额");

            migrationBuilder.AddColumn<decimal>(
                name: "WalletMoney",
                table: "User_Info",
                type: "decimal(5,2)",
                nullable: false,
                defaultValue: 0m,
                comment: "钱包金额");

            migrationBuilder.CreateTable(
                name: "User_WalletRecord",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    UserId = table.Column<Guid>(type: "char(36)", nullable: false, comment: "用户id", collation: "ascii_general_ci"),
                    Type = table.Column<int>(type: "int", nullable: false, comment: "微信app类型"),
                    Money = table.Column<decimal>(type: "decimal(5,2)", nullable: false, comment: "金额"),
                    IsDeleted = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValue: false, comment: "是否删除"),
                    CreationTime = table.Column<DateTime>(type: "datetime(6)", nullable: false, comment: "创建时间"),
                    CreatorId = table.Column<Guid>(type: "char(36)", nullable: true, comment: "创建人", collation: "ascii_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User_WalletRecord", x => x.Id);
                },
                comment: "钱包记录")
                .Annotation("MySql:CharSet", "utf8mb4");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "User_WalletRecord");

            migrationBuilder.DropColumn(
                name: "GiveMoney",
                table: "User_Info");

            migrationBuilder.DropColumn(
                name: "WalletMoney",
                table: "User_Info");
        }
    }
}
