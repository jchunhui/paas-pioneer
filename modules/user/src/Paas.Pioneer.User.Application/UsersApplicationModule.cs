﻿using EasyAbp.Abp.WeChat.MiniProgram;
using Paas.Pioneer.User.Application.Contracts;
using Paas.Pioneer.User.Domain;
using Paas.Pioneer.WeChat.HttpApi.Client;
using Volo.Abp.AutoMapper;
using Volo.Abp.Modularity;
using Volo.Abp.TenantManagement;

namespace Paas.Pioneer.User.Application;

[DependsOn(
    typeof(UsersDomainModule),
    typeof(UsersApplicationContractsModule),
    typeof(AbpTenantManagementApplicationModule),
    typeof(WeChatsHttpApiClientModule),
    typeof(AbpWeChatMiniProgramModule)
    )]
public class UsersApplicationModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        Configure<AbpAutoMapperOptions>(options =>
        {
            options.AddMaps<UsersApplicationModule>();
        });
    }
}
