using Paas.Pioneer.Domain.Shared.Auth;
using Paas.Pioneer.User.Application.Contracts.User;
using Paas.Pioneer.User.Application.Contracts.User.Dtos;
using Paas.Pioneer.User.Domain.User;
using System;
using System.Linq;
using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.Application.Services;

namespace Paas.Pioneer.User.Application.User;

public class UserInfoService : ApplicationService, IUserInfoService
{
    private readonly IUserInfoRepository _userInfoRepository;
    private readonly IUserToken _userToken;

    public UserInfoService(IUserInfoRepository userInfoRepository,
        IUserToken userToken)
    {
        _userInfoRepository = userInfoRepository;
        _userToken = userToken;
    }

    public async Task SyncUserWeChatInfoAsync(SyncUserWeChatInfoInput input)
    {
        if (input.TemporaryToken.IsNullOrEmpty())
        {
            throw new BusinessException("错误token");
        }
       
        var userClaims = _userToken.Decode(input.TemporaryToken);
        if (userClaims == null || !userClaims.Any())
        {
            throw new BusinessException("错误token");
        }
        
        var userId = userClaims.FirstOrDefault(a => a.Type == ClaimAttributes.UserId)?.Value;
        if (userId.IsNullOrEmpty())
        {
            throw new BusinessException("登录信息错误");
        }

        var userInfo = await _userInfoRepository.GetAsync(Guid.Parse(userId));
        userInfo.AvatarUrl = input.UserInfo.AvatarUrl;
        userInfo.City = input.UserInfo.City;
        userInfo.Country = input.UserInfo.Country;
        userInfo.Gender = input.UserInfo.Gender;
        userInfo.Language = input.UserInfo.Language;
        userInfo.NickName = input.UserInfo.NickName;
        userInfo.Province = input.UserInfo.Province;
    }

    public async Task UpdatePasswordAsync(UpdatePasswordInput input)
    {
        UserInfoEntity userInfo = null;
        if (CurrentUser != null)
        {
            userInfo = await _userInfoRepository.GetAsync(predicate: x => x.Id == CurrentUser.Id);
        }
        else
        {
            if (await _userInfoRepository.AnyAsync(x => x.Mobile == input.Mobile))
            {
                throw new BusinessException("账号不存在");
            }

            userInfo = await _userInfoRepository.GetAsync(predicate: x => x.Mobile == input.Mobile);
        }

        userInfo.PassWord = input.Password;
    }

    public async Task UpdateNameAsync(UpdateNameInput input)
    {
        if (!await _userInfoRepository.AnyAsync(x => x.Id == CurrentUser.Id))
        {
            throw new BusinessException("账号不存在");
        }

        var userInfo = await _userInfoRepository.GetAsync(predicate: x => x.Id == CurrentUser.Id);
        userInfo.Name = input.Name;
    }

    public async Task UpdateAvatarUrlAsync(UpdateAvatarUrlInput input)
    {
        if (!await _userInfoRepository.AnyAsync(x => x.Id == CurrentUser.Id))
        {
            throw new BusinessException("账号不存在");
        }

        var userInfo = await _userInfoRepository.GetAsync(predicate: x => x.Id == CurrentUser.Id);
        userInfo.AvatarUrl = input.AvatarUrl;
    }

    public async Task UpdateMobileAsync(UpdateMobileInput input)
    {
        var userInfo = await _userInfoRepository.GetAsync(predicate: x => x.Id == CurrentUser.Id);
        if (userInfo == null)
        {
            throw new BusinessException("用户不存在");
        }

        userInfo.Mobile = input.Mobile;
    }
}