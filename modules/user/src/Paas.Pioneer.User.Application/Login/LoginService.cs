using EasyAbp.Abp.WeChat.MiniProgram;
using Microsoft.Extensions.Options;
using Paas.Pioneer.Domain.Shared.Auth;
using Paas.Pioneer.User.Application.Contracts.Login;
using Paas.Pioneer.User.Application.Contracts.Login.Dtos;
using Paas.Pioneer.User.Domain.User;
using Paas.Pioneer.WeChat.HttpApi.Client.ServiceProxies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.Application.Services;
using Volo.Abp.Data;
using Paas.Pioneer.Domain.Shared.Helpers;
using Paas.Pioneer.Domain.Shared.Extensions;
using Senparc.Weixin.MP.AdvancedAPIs.OAuth;
using Paas.Pioneer.WeChat.Application.Contracts.WeChatAppUsers.Dtos.Input;

namespace Paas.Pioneer.User.Application.Login;

public class LoginService : ApplicationService, ILoginService
{
    private readonly EasyAbp.Abp.WeChat.MiniProgram.Services.Login.LoginService _loginService;
    private readonly AbpWeChatMiniProgramOptions _abpWeChatMiniProgramOptions;
    private readonly IWeChatAppUserServiceProxy _weChatAppUserServiceProxy;
    private readonly IWeChatAppServiceProxy _weChatAppServiceProxy;
    private readonly IUserInfoRepository _userInfoRepository;
    private readonly IDataFilter _dataFilter;
    private readonly IUserToken _userToken;

    public LoginService(
        EasyAbp.Abp.WeChat.MiniProgram.Services.Login.LoginService loginService,
        IOptions<AbpWeChatMiniProgramOptions> abpWeChatMiniProgramOptions,
        IWeChatAppUserServiceProxy weChatAppUserServiceProxy,
        IWeChatAppServiceProxy weChatAppServiceProxy,
        IUserInfoRepository userInfoRepository,
        IUserToken userToken,
        IDataFilter dataFilter)
    {
        _loginService = loginService;
        _abpWeChatMiniProgramOptions = abpWeChatMiniProgramOptions.Value;
        _weChatAppUserServiceProxy = weChatAppUserServiceProxy;
        _userInfoRepository = userInfoRepository;
        _userToken = userToken;
        _dataFilter = dataFilter;
        _weChatAppServiceProxy = weChatAppServiceProxy;
    }

    public async Task<LoginOutput> LoginAsync(LoginInput input)
    {
        LoginOutput result = new LoginOutput();
        var code2SessionResponse =
            await _loginService.Code2SessionAsync(_abpWeChatMiniProgramOptions.AppId,
                _abpWeChatMiniProgramOptions.AppSecret, input.Code);

        if (code2SessionResponse == null)
        {
            throw new BusinessException("获取微信用户信息失败");
        }

        var userInfo = new UserInfoEntity();
        if (!await _weChatAppUserServiceProxy.AnyAsync(code2SessionResponse.OpenId))
        {
            await _userInfoRepository.InsertAsync(userInfo);
            await _weChatAppUserServiceProxy.WeChatAppUserCreate(
                new CreateWeChatAppUserInput
                {
                    SessionKey = code2SessionResponse.SessionKey,
                    OpenId = code2SessionResponse.OpenId,
                    UnionId = code2SessionResponse.UnionId,
                    UserId = userInfo.Id,
                    WeChatAppId = await _weChatAppServiceProxy.GetIdAsync(_abpWeChatMiniProgramOptions.AppId)
                });
            result.TemporaryToken = GetToken(userInfo);
            return result;
        }

        var userId = await _weChatAppUserServiceProxy.GetUserIdAsync(code2SessionResponse.OpenId);
        userInfo = await _userInfoRepository.GetAsync(x => x.Id == userId, x => new UserInfoEntity(x.Id)
        {
            NickName = x.NickName,
            AvatarUrl = x.AvatarUrl,
            Mobile = x.Mobile,
            TenantId = x.TenantId
        });
        if (userInfo.AvatarUrl.IsNullOrEmpty())
        {
            result.TemporaryToken = GetToken(userInfo);
            return result;
        }

        result.UserInfo.AvatarUrl = userInfo.AvatarUrl;
        result.UserInfo.Mobile = userInfo.Mobile;
        result.UserInfo.NickName = userInfo.NickName;
        result.Token = GetToken(userInfo);
        return result;
    }

    public async Task<LoginOutput> H5LoginAsync(OAuthUserInfo input, string accessToken, string appId)
    {
        LoginOutput result = new LoginOutput
        {
            UserInfo =
            {
                AvatarUrl = input.headimgurl,
                NickName = input.nickname
            }
        };
        var userInfo = new UserInfoEntity()
        {
            NickName = input.nickname,
            AvatarUrl = input.headimgurl,
            Language = input.language,
        };
        if (!await _weChatAppUserServiceProxy.AnyAsync(input.openid))
        {
            await _userInfoRepository.InsertAsync(userInfo);
            await _weChatAppUserServiceProxy.WeChatAppUserCreate(
                new CreateWeChatAppUserInput
                {
                    SessionKey = accessToken,
                    OpenId = input.openid,
                    UnionId = input.unionid,
                    UserId = userInfo.Id,
                    WeChatAppId = await _weChatAppServiceProxy.GetIdAsync(appId)
                });
            result.TemporaryToken = GetToken(userInfo);
            return result;
        }

        var userId = await _weChatAppUserServiceProxy.GetUserIdAsync(input.openid);
        userInfo = await _userInfoRepository.GetAsync(x => x.Id == userId, x => new UserInfoEntity(x.Id)
        {
            NickName = x.NickName,
            AvatarUrl = x.AvatarUrl,
            Mobile = x.Mobile,
            TenantId = x.TenantId
        });
        if (userInfo.AvatarUrl.IsNullOrEmpty())
        {
            result.TemporaryToken = GetToken(userInfo);
            return result;
        }
        result.Token = GetToken(userInfo);
        result.UserInfo.Mobile = userInfo.Mobile;
        return result;
    }

    /// <summary>
    /// 获得token
    /// </summary>
    /// <returns></returns>
    private string GetToken(UserInfoEntity user)
    {
        if (user == null)
        {
            throw new BusinessException("用户信息错误");
        }

        IEnumerable<Claim> claimList = new[]
        {
            new Claim(ClaimAttributes.UserId, user.Id.ToString()),
            new Claim(ClaimAttributes.TenantId, user.TenantId.ToString()),
        };
        var token = _userToken.Create(claimList);
        return token;
    }

    public async Task<string> RefreshTokenAsync(RefreshTokenInput input)
    {
        var userClaims = _userToken.Decode(input.RefreshToken);
        if (userClaims == null || !userClaims.Any())
        {
            throw new BusinessException("错误token");
        }

        var refreshExpires = userClaims.FirstOrDefault(a => a.Type == ClaimAttributes.RefreshExpires)?.Value;
        if (refreshExpires.IsNullOrEmpty())
        {
            throw new BusinessException("错误token");
        }

        if (refreshExpires.ToLong() <= DateTime.Now.ToTimestamp())
        {
            throw new BusinessException("登录信息已过期");
        }

        var userId = userClaims.FirstOrDefault(a => a.Type == ClaimAttributes.UserId)?.Value;
        if (userId.IsNullOrEmpty())
        {
            throw new BusinessException("登录信息已失效");
        }

        var userInfo = await _userInfoRepository.GetAsync(x => x.Id == Guid.Parse(userId), x => new UserInfoEntity(x.Id)
        {
            Name = x.Name,
        });
        return GetToken(userInfo);
    }

    public async Task<LoginOutput> AccountLoginAsync(AccountLoginInput input)
    {
        LoginOutput result = new LoginOutput();
        var userInfo = await _userInfoRepository.GetAsync(x => x.Mobile == input.Mobile && x.PassWord == input.Password,
            x => new UserInfoEntity(x.Id)
            {
                NickName = x.NickName,
                AvatarUrl = x.AvatarUrl,
                Mobile = x.Mobile,
                TenantId = x.TenantId
            });
        if (userInfo == null)
        {
            throw new BusinessException("手机号码或者密码错误");
        }

        result.UserInfo.AvatarUrl = userInfo.AvatarUrl;
        result.UserInfo.Mobile = userInfo.Mobile;
        result.UserInfo.NickName = userInfo.NickName;
        result.Token = GetToken(userInfo);
        return result;
    }

    public async Task MobileRegisterAsync(MobileRegisterInput input)
    {
        if (await _userInfoRepository.AnyAsync(x => x.Mobile == input.Mobile))
        {
            throw new BusinessException("手机号码已经注册");
        }

        await _userInfoRepository.InsertAsync(new UserInfoEntity
        {
            Mobile = input.Mobile,
            PassWord = input.Password
        });
    }
}