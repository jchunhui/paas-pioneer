using Paas.Pioneer.User.Application.Contracts.Login.Dtos;
using System.Threading.Tasks;
using Senparc.Weixin.MP.AdvancedAPIs.OAuth;
using Volo.Abp.Application.Services;

namespace Paas.Pioneer.User.Application.Contracts.Login;

public interface ILoginService : IApplicationService
{
    Task<LoginOutput> LoginAsync(LoginInput input);

    Task<string> RefreshTokenAsync(RefreshTokenInput input);

    Task MobileRegisterAsync(MobileRegisterInput input);

    Task<LoginOutput> AccountLoginAsync(AccountLoginInput input);

    Task<LoginOutput> H5LoginAsync(OAuthUserInfo input, string accessToken, string appId);
}