﻿namespace Paas.Pioneer.User.Application.Contracts.Login.Dtos
{
    public class LoginOutput
    {
        public LoginOutput()
        {
            UserInfo = new LoginUserInfo();
        }
        public string TemporaryToken { get; set; }

        public string Token { get; set; }

        public LoginUserInfo UserInfo { get; set; }
    }

    public class LoginUserInfo
    {
        /// <summary>
        /// 名称
        /// </summary>
        public virtual string NickName { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string Mobile { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public virtual byte Gender { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        public virtual string AvatarUrl { get; set; }
    }
}