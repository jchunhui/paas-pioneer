﻿namespace Paas.Pioneer.User.Application.Contracts.Login.Dtos
{
    public class AccountLoginInput
    {
        public string Mobile { get; set; }

        public string Password { get; set; }
    }
}
