﻿namespace Paas.Pioneer.User.Application.Contracts.Login.Dtos
{
    public class MobileRegisterInput
    {
        public string Mobile { get; set; }

        public string Password { get; set; }

        public string VerifyCode { get; set; }
    }
}
