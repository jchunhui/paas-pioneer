﻿using System.ComponentModel.DataAnnotations;

namespace Paas.Pioneer.User.Application.Contracts.Login.Dtos
{
    public class RefreshTokenInput
    {
        [Required]
        public string RefreshToken { get; set; }
    }
}