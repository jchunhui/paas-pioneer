﻿namespace Paas.Pioneer.User.Application.Contracts.User.Dtos
{
    public class SyncUserWeChatInfoInput
    {
        public string TemporaryToken { get; set; }

        public string EncryptedData { get; set; }

        public string IV { get; set; }

        public string Signature { get; set; }

        public SyncUserWeChatInfoUserInfo UserInfo { get; set; }
    }

    public class SyncUserWeChatInfoUserInfo
    {
        public string AvatarUrl { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public byte Gender { get; set; }

        public string Language { get; set; }

        public string NickName { get; set; }

        public string Province { get; set; }
    }
}
