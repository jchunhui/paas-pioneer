﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paas.Pioneer.User.Application.Contracts.User.Dtos
{
    public class UpdatePasswordInput
    {
        public string Mobile { get; set; }

        public string Password { get; set; }

        public string VerifyCode { get; set; }
    }
}
