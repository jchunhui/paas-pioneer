﻿namespace Paas.Pioneer.User.Application.Contracts.User.Dtos;

public class UpdateMobileInput
{
    public string Mobile { get; set; }
    
    public string VerifyCode { get; set; }
}