﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paas.Pioneer.User.Application.Contracts.User.Dtos
{
    public class UpdateAvatarUrlInput
    {
        public string AvatarUrl { get; set; }
    }
}
