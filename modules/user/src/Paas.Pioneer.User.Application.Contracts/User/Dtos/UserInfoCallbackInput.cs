﻿namespace Paas.Pioneer.User.Application.Contracts.User.Dtos;

public class UserInfoCallbackInput
{
    public string Code { get; set; }

    public string ReturnUrl { get; set; }
}