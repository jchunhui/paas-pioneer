﻿namespace Paas.Pioneer.User.Application.Contracts.User.Dtos
{
    public class UpdateNameInput
    {
        public string Name { get; set; }
    }
}
