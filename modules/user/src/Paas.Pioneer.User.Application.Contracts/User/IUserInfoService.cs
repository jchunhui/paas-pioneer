using Paas.Pioneer.User.Application.Contracts.User.Dtos;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Paas.Pioneer.User.Application.Contracts.User;

public interface IUserInfoService : IApplicationService
{
    Task SyncUserWeChatInfoAsync(SyncUserWeChatInfoInput input);

    Task UpdatePasswordAsync(UpdatePasswordInput input);

    Task UpdateNameAsync(UpdateNameInput input);

    Task UpdateAvatarUrlAsync(UpdateAvatarUrlInput input);

    Task UpdateMobileAsync(UpdateMobileInput input);
}