using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Paas.Pioneer.WeChat.Application.Contracts.WeChatAppUsers;
using Paas.Pioneer.WeChat.Application.Contracts.WeChatAppUsers.Dtos;
using Paas.Pioneer.WeChat.Application.Contracts.WeChatAppUsers.Dtos.Input;
using Volo.Abp.Application.Dtos;
using Volo.Abp.AspNetCore.Mvc;

namespace Paas.Pioneer.WeChat.HttpApi.Controllers.WeChatAppUsers;

[Route("/api/wechat-app-user")]
public class WeChatAppUserController : AbpController
{
    private readonly IWeChatAppUserAppService _service;

    public WeChatAppUserController(IWeChatAppUserAppService service)
    {
        _service = service;
    }

    [HttpPost("/rpc/wechat-app-user")]
    public async Task Add([FromBody] CreateWeChatAppUserInput input)
    {
        await _service.CreateAsync(input);
    }

    [HttpGet("/rpc/wechat-app-user/exist/{openIdOrUnionId}")]
    public Task<bool> AnyAsync(string openIdOrUnionId)
    {
        return _service.AnyAsync(openIdOrUnionId);
    }

    [HttpGet("/rpc/wechat-app-user/UserId/{openIdOrUnionId}")]
    public async Task<Guid> GetUserIdAsync(string openIdOrUnionId)
    {
        return await _service.GetUserIdAsync(openIdOrUnionId);
    }

    /// <summary>
    /// ��ȡ����
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet]
    public Task<PagedResultDto<WeChatAppUserDto>> GetListAsync(PagedAndSortedResultRequestDto input)
    {
        return _service.GetListAsync(input);
    }
}