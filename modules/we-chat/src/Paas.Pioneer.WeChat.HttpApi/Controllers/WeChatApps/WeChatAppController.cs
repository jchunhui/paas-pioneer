using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Paas.Pioneer.Domain.Shared.Dto.Input;
using Paas.Pioneer.Domain.Shared.Dto.Output;
using Paas.Pioneer.WeChat.Application.Contracts.WeChatApps;
using Paas.Pioneer.WeChat.Application.Contracts.WeChatApps.Dto.Input;
using Paas.Pioneer.WeChat.Application.Contracts.WeChatApps.Dto.Output;
using Volo.Abp.AspNetCore.Mvc;

namespace Paas.Pioneer.WeChat.HttpApi.Controllers.WeChatApps;

[Route("/api/wechat/[controller]")]
public class WeChatAppController : AbpController
{
    private readonly IWeChatAppService _weChatAppService;

    public WeChatAppController(IWeChatAppService weChatAppService)
    {
        _weChatAppService = weChatAppService;
    }

    [HttpGet]
    [Route("/rpc/wechat/wechat-app/{appId}/id")]
    public async Task<Guid> GetIdAsync(string appId)
    {
        return await _weChatAppService.GetIdAsync(appId);
    }

    /// <summary>
    /// 查询微信App
    /// </summary>
    /// <param name="id">主键</param>
    /// <returns></returns>
    [HttpGet("{id}")]
    public async Task<GetWeChatAppOutput> Get(Guid id)
    {
        return await _weChatAppService.GetAsync(id);
    }

    /// <summary>
    /// 查询分页微信App
    /// </summary>
    /// <param name="input">入参</param>
    /// <returns></returns>
    [HttpPost("page-list")]
    public async Task<Page<GetWeChatAppPageListOutput>> GetPageList(
        [FromBody] PageInput<GetWeChatAppPageListInput> input)
    {
        return await _weChatAppService.GetPageListAsync(input);
    }

    /// <summary>
    /// 新增微信App
    /// </summary>
    /// <param name="input">入参</param>
    /// <returns></returns>
    [HttpPost]
    public async Task Add([FromBody] AddWeChatAppInput input)
    {
        await _weChatAppService.AddAsync(input);
    }

    /// <summary>
    /// 修改微信App
    /// </summary>
    /// <param name="input">入参</param>
    /// <returns></returns>
    [HttpPut]
    public async Task Update([FromBody] UpdateWeChatAppInput input)
    {
        await _weChatAppService.UpdateAsync(input);
    }

    /// <summary>
    /// 删除微信App
    /// </summary>
    /// <param name="id">主键</param>
    /// <returns></returns>
    [HttpDelete("{id}")]
    public async Task SoftDelete(Guid id)
    {
        await _weChatAppService.DeleteAsync(id);
    }

    /// <summary>
    /// 批量删除微信App
    /// </summary>
    /// <param name="ids">主键集合</param>
    /// <returns></returns>
    [HttpDelete("ids")]
    public async Task BatchSoftDelete([FromBody] Guid[] ids)
    {
        await _weChatAppService.BatchSoftDeleteAsync(ids);
    }
}