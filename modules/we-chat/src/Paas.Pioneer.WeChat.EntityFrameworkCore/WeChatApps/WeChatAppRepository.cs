using Paas.Pioneer.WeChat.Domain.WeChatApps;
using Paas.Pioneer.WeChat.EntityFrameworkCore.BaseExtensions;
using Paas.Pioneer.WeChat.EntityFrameworkCore.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace Paas.Pioneer.WeChat.EntityFrameworkCore.WeChatApps
{
    public class WeChatAppRepository : BaseExtensionsRepository<WeChatAppEntity>, IWeChatAppRepository
    {
        public WeChatAppRepository(IDbContextProvider<WeChatsDbContext> dbContextProvider) : base(dbContextProvider)
        {

        }
    }
}