using System;
using System.Threading.Tasks;
using Paas.Pioneer.WeChat.Application.Contracts.WeChatAppUsers.Dtos;
using Paas.Pioneer.WeChat.Application.Contracts.WeChatAppUsers.Dtos.Input;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace Paas.Pioneer.WeChat.Application.Contracts.WeChatAppUsers
{
    public interface IWeChatAppUserAppService :
        IReadOnlyAppService<
            WeChatAppUserDto,
            Guid,
            PagedAndSortedResultRequestDto>
    {
        Task CreateAsync(CreateWeChatAppUserInput input);

        Task<bool> AnyAsync(string openIdOrUnionId);

        Task<Guid> GetUserIdAsync(string openIdOrUnionId);
    }
}