﻿using Newtonsoft.Json;
using System;

namespace Paas.Pioneer.WeChat.Application.Contracts.WeChatAppUsers.Dtos.Input
{
    public class CreateWeChatAppUserInput
    {
        public Guid WeChatAppId { get; set; }

        public Guid UserId { get; set; }

        public string OpenId { get; set; }

        public string SessionKey { get; set; }

        public string UnionId { get; set; }
    }
}
