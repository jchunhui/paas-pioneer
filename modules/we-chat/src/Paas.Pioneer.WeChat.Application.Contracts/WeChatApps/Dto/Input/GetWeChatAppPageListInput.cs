﻿using System;
using Paas.Pioneer.Domain.Shared.ModelValidation;

namespace Paas.Pioneer.WeChat.Application.Contracts.WeChatApps.Dto.Input
{
    /// <summary>
    /// 微信App分页
    /// </summary>
    public class GetWeChatAppPageListInput
    {
        /// <summary>
        /// 微信组件Id
        /// </summary>
        public Guid? WeChatComponentId { get; set; }

        /// <summary>
        /// 微信app类型
        /// </summary>
        public Domain.Shared.WeChatApps.WeChatAppType Type { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 显示名称
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// 开放Id或者名称
        /// </summary>
        public string OpenAppIdOrName { get; set; }

        /// <summary>
        /// 微信AppId
        /// </summary>
        public string AppId { get; set; }

        /// <summary>
        /// App密钥
        /// </summary>
        public string AppSecret { get; set; }

        /// <summary>
        /// 微信Token
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// 微信密钥
        /// </summary>
        public string EncodingAesKey { get; set; }

        /// <summary>
        /// 是否静态
        /// </summary>
        public bool IsStatic { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? LastModificationTime { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreationTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Guid Id { get; set; }

    }
}