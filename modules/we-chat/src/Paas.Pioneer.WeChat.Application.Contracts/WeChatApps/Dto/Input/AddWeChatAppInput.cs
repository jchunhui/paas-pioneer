﻿using System;
using Paas.Pioneer.Domain.Shared.ModelValidation;
using System.ComponentModel.DataAnnotations;

namespace Paas.Pioneer.WeChat.Application.Contracts.WeChatApps.Dto.Input
{
    /// <summary>
    /// 微信App添加
    /// </summary>
    public class AddWeChatAppInput
    {
        /// <summary>
        /// 微信组件Id
        /// </summary>
        public Guid? WeChatComponentId { get; set; }

        /// <summary>
        /// 微信app类型
        /// </summary>
        [Required(ErrorMessage = "请输入微信app类型")]
        public Domain.Shared.WeChatApps.WeChatAppType Type { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Required(ErrorMessage = "请输入名称")]
        public string Name { get; set; }

        /// <summary>
        /// 显示名称
        /// </summary>
        [Required(ErrorMessage = "请输入显示名称")]
        public string DisplayName { get; set; }

        /// <summary>
        /// 开放平台Id或名称
        /// </summary>
        // [Required(ErrorMessage = "开放平台Id或名称")]
        public string OpenAppIdOrName { get; set; }

        /// <summary>
        /// 微信AppId
        /// </summary>
        [Required(ErrorMessage = "请输入微信AppId")]
        public string AppId { get; set; }

        /// <summary>
        /// App密钥
        /// </summary>
        [Required(ErrorMessage = "请输入App密钥")]
        public string AppSecret { get; set; }

        /// <summary>
        /// 微信Token
        /// </summary>
        [Required(ErrorMessage = "请输入微信Token")]
        public string Token { get; set; }

        /// <summary>
        /// 微信密钥
        /// </summary>
        [Required(ErrorMessage = "请输入微信密钥")]
        public string EncodingAesKey { get; set; }

        /// <summary>
        /// 是否静态
        /// </summary>
        public bool IsStatic { get; set; }
    }
}