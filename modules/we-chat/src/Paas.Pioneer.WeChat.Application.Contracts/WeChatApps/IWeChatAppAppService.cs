using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Paas.Pioneer.Domain.Shared.Dto.Input;
using Paas.Pioneer.Domain.Shared.Dto.Output;
using Paas.Pioneer.WeChat.Application.Contracts.WeChatApps.Dto.Input;
using Paas.Pioneer.WeChat.Application.Contracts.WeChatApps.Dto.Output;
using Volo.Abp.Application.Services;

namespace Paas.Pioneer.WeChat.Application.Contracts.WeChatApps
{
    /// <summary>
    /// 微信App接口
    /// </summary>
    public interface IWeChatAppService : IApplicationService
    {
        /// <summary>
        /// 查询微信App
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        Task<GetWeChatAppOutput> GetAsync(Guid id);

        /// <summary>
        /// 查询分页微信App
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        Task<Page<GetWeChatAppPageListOutput>> GetPageListAsync(PageInput<GetWeChatAppPageListInput> input);

        /// <summary>
        /// 新增微信App
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        Task AddAsync(AddWeChatAppInput input);

        /// <summary>
        /// 修改微信App
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        Task UpdateAsync(UpdateWeChatAppInput input);

        /// <summary>
        /// 删除微信App
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        Task DeleteAsync(Guid id);

        /// <summary>
        /// 批量删除微信App
        /// </summary>
        /// <param name="ids">主键集合</param>
        /// <returns></returns>
        Task BatchSoftDeleteAsync(IEnumerable<Guid> ids);

        Task<Guid> GetIdAsync(string appId);
    }
}