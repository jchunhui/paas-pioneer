﻿using Refit;
using System;
using System.Threading.Tasks;

namespace Paas.Pioneer.WeChat.HttpApi.Client.ServiceProxies
{
    public interface IWeChatAppServiceProxy : IRefitServiceProxy
    {
        [Get("/rpc/wechat/wechat-app/{appId}/id")]
        Task<Guid> GetIdAsync(string appId);
    }
}