﻿using Microsoft.AspNetCore.Mvc;
using Paas.Pioneer.WeChat.Application.Contracts.WeChatAppUsers.Dtos.Input;
using Refit;
using System;
using System.Threading.Tasks;

namespace Paas.Pioneer.WeChat.HttpApi.Client.ServiceProxies
{
    public interface IWeChatAppUserServiceProxy : IRefitServiceProxy
    {
        [Post("/rpc/wechat-app-user")]
        Task WeChatAppUserCreate([Body] CreateWeChatAppUserInput input);

        [Get("/rpc/wechat-app-user/exist/{openIdOrUnionId}")]
        Task<bool> AnyAsync(string openIdOrUnionId);

        [Get("/rpc/wechat-app-user/UserId/{openIdOrUnionId}")]
        Task<Guid> GetUserIdAsync(string openIdOrUnionId);
    }
}
