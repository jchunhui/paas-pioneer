using Paas.Pioneer.WeChat.Domain.BaseExtensions;
using System;
using Volo.Abp.Domain.Repositories;

namespace Paas.Pioneer.WeChat.Domain.WeChatApps
{
    public interface IWeChatAppRepository : IRepository<WeChatAppEntity, Guid>, IBaseExtensionRepository<WeChatAppEntity>
    {
    }
}