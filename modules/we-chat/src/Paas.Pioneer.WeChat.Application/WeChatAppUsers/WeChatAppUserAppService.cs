using System;
using System.Threading.Tasks;
using Paas.Pioneer.WeChat.Application.Contracts.WeChatAppUsers;
using Paas.Pioneer.WeChat.Application.Contracts.WeChatAppUsers.Dtos;
using Paas.Pioneer.WeChat.Application.Contracts.WeChatAppUsers.Dtos.Input;
using Paas.Pioneer.WeChat.Domain.WeChatAppUsers;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace Paas.Pioneer.WeChat.Application.WeChatAppUsers
{
    public class WeChatAppUserAppService : ReadOnlyAppService<WeChatAppUser, WeChatAppUserDto, Guid, PagedAndSortedResultRequestDto>,
        IWeChatAppUserAppService
    {
        private readonly IWeChatAppUserRepository _repository;

        public WeChatAppUserAppService(IWeChatAppUserRepository repository) : base(repository)
        {
            _repository = repository;
        }

        public async Task<bool> AnyAsync(string openIdOrUnionId)
        {
            return await _repository.AnyAsync(x => x.OpenId == openIdOrUnionId || x.UnionId == openIdOrUnionId);
        }

        public async Task CreateAsync(CreateWeChatAppUserInput input)
        {
            var entity = ObjectMapper.Map<CreateWeChatAppUserInput, WeChatAppUser>(input);
            await _repository.InsertAsync(entity);
        }

        public async Task<Guid> GetUserIdAsync(string openIdOrUnionId)
        {
            return await _repository.GetAsync(x => x.OpenId == openIdOrUnionId || x.UnionId == openIdOrUnionId, x => x.UserId);
        }
    }
}