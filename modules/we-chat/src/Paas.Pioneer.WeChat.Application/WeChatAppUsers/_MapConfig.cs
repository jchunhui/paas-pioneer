﻿using AutoMapper;
using Paas.Pioneer.WeChat.Application.Contracts.WeChatAppUsers.Dtos.Input;
using Paas.Pioneer.WeChat.Domain.WeChatAppUsers;

namespace Paas.Pioneer.WeChat.Application.WeChatAppUsers
{
    public class _MapConfig : Profile
    {
        public _MapConfig()
        {
            CreateMap<CreateWeChatAppUserInput, WeChatAppUser>();
        }
    }
}
