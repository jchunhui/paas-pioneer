﻿using AutoMapper;
using Paas.Pioneer.WeChat.Application.Contracts.WeChatApps.Dto.Input;
using Paas.Pioneer.WeChat.Application.Contracts.WeChatApps.Dto.Output;
using Paas.Pioneer.WeChat.Domain.WeChatApps;

namespace Paas.Pioneer.WeChat.Application.WeChatApps;

public class _MapConfig : Profile
{
    public _MapConfig()
    {
        CreateMap<WeChatAppEntity, GetWeChatAppOutput>();

        CreateMap<AddWeChatAppInput, WeChatAppEntity>();
        CreateMap<UpdateWeChatAppInput, WeChatAppEntity>();
    }
}