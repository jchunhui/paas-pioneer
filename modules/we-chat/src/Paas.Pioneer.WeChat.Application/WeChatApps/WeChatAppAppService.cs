using System;
using Paas.Pioneer.WeChat.Application.Contracts.WeChatApps;
using Paas.Pioneer.WeChat.Domain.WeChatApps;
using Volo.Abp.Application.Services;
using System.Threading.Tasks;
using Paas.Pioneer.Domain.Shared.Dto.Input;
using Paas.Pioneer.Domain.Shared.Dto.Output;
using System.Collections.Generic;
using System.Linq;
using Paas.Pioneer.WeChat.Application.Contracts.WeChatApps.Dto.Input;
using Paas.Pioneer.WeChat.Application.Contracts.WeChatApps.Dto.Output;
using Volo.Abp;

namespace Paas.Pioneer.WeChat.Application.WeChatApps
{
    /// <summary>
    /// 微信App服务
    /// </summary>
    public class WeChatAppService : ApplicationService, IWeChatAppService
    {
        private readonly IWeChatAppRepository _weChatAppRepository;

        /// <summary>
        /// 构造函数
        /// </summary>
        public WeChatAppService(IWeChatAppRepository weChatAppRepository)
        {
            _weChatAppRepository = weChatAppRepository;
        }

        /// <summary>
        /// 查询微信App
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public async Task<GetWeChatAppOutput> GetAsync(Guid id)
        {
            var result = await _weChatAppRepository.GetAsync(p => p.Id == id, x => new GetWeChatAppOutput()
            {
                WeChatComponentId = x.WeChatComponentId,
                Type = x.Type,
                Name = x.Name,
                DisplayName = x.DisplayName,
                OpenAppIdOrName = x.OpenAppIdOrName,
                AppId = x.AppId,
                AppSecret = x.AppSecret,
                Token = x.Token,
                EncodingAesKey = x.EncodingAesKey,
                IsStatic = x.IsStatic,
                LastModificationTime = x.LastModificationTime,
                CreationTime = x.CreationTime,
                Id = x.Id,
            });
            return result;
        }

        /// <summary>
        /// 查询分页微信App
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        public async Task<Page<GetWeChatAppPageListOutput>> GetPageListAsync(PageInput<GetWeChatAppPageListInput> input)
        {
            var data = await _weChatAppRepository.GetResponseOutputPageListAsync(x => new GetWeChatAppPageListOutput
            {
                WeChatComponentId = x.WeChatComponentId,
                Type = x.Type,
                Name = x.Name,
                DisplayName = x.DisplayName,
                OpenAppIdOrName = x.OpenAppIdOrName,
                AppId = x.AppId,
                AppSecret = x.AppSecret,
                Token = x.Token,
                EncodingAesKey = x.EncodingAesKey,
                IsStatic = x.IsStatic,
                LastModificationTime = x.LastModificationTime,
                CreationTime = x.CreationTime,
                Id = x.Id,
            }, input: input);
            return data;
        }

        /// <summary>
        /// 新增微信App
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        public async Task AddAsync(AddWeChatAppInput input)
        {
            var weChatApp = ObjectMapper.Map<AddWeChatAppInput, WeChatAppEntity>(input);
            await _weChatAppRepository.InsertAsync(weChatApp);
        }

        /// <summary>
        /// 修改微信App
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        public async Task UpdateAsync(UpdateWeChatAppInput input)
        {
            var entity = await _weChatAppRepository.GetAsync(input.Id);
            if (entity?.Id == Guid.Empty)
            {
                throw new BusinessException("数据不存在！");
            }

            ObjectMapper.Map(input, entity);
            await _weChatAppRepository.UpdateAsync(entity);
        }

        /// <summary>
        /// 删除微信App
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public async Task DeleteAsync(Guid id)
        {
            await _weChatAppRepository.DeleteAsync(m => m.Id == id);
        }

        /// <summary>
        /// 批量删除微信App
        /// </summary>
        /// <param name="ids">主键集合</param>
        /// <returns></returns>
        public async Task BatchSoftDeleteAsync(IEnumerable<Guid> ids)
        {
            await _weChatAppRepository.DeleteAsync(x => ids.Contains(x.Id));
        }

        public async Task<Guid> GetIdAsync(string appId)
        {
            return await _weChatAppRepository.GetAsync(x => x.AppId == appId, x => x.Id);
        }
    }
}