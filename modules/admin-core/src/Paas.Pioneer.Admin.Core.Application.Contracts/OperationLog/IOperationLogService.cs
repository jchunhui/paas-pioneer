﻿using System.Threading.Tasks;
using Paas.Pioneer.Admin.Core.Application.Contracts.OperationLog.Dto.Input;
using Paas.Pioneer.Admin.Core.Application.Contracts.OperationLog.Dto.Output;
using Paas.Pioneer.Domain.Shared.Dto.Input;
using Paas.Pioneer.Domain.Shared.Dto.Output;
using Volo.Abp.Application.Services;

namespace Paas.Pioneer.Admin.Core.Application.Contracts.OperationLog;

public interface IOperationLogService : IApplicationService
{
    Task<Page<OperationLogListOutput>> GetPageListAsync(PageInput<OperationLogInput> input);
}