﻿using System;

namespace Paas.Pioneer.Admin.Core.Application.Contracts.OperationLog.Dto.Output
{
    public class OperationLogListOutput
    {
        /// <summary>
        /// Id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 操作人名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 应用模块
        /// </summary>
        public string ApplicationName { get; set; }

        /// <summary>
        /// 创建者
        /// </summary>
        public string TenantName { get; set; }

        /// <summary>
        /// 客户端地址
        /// </summary>
        public string ClientIpAddress { get; set; }

        /// <summary>
        /// 浏览器
        /// </summary>
        public string BrowserInfo { get; set; }

        /// <summary>
        /// 操作系统
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 异常信息
        /// </summary>
        public string Exceptions { get; set; }

        /// <summary>
        /// 状态码
        /// </summary>
        public int HttpStatusCode { get; set; }

        /// <summary>
        /// 操作状态
        /// </summary>
        public string Status => this.HttpStatusCode == 200 ? "成功" : "失败";

        /// <summary>
        /// 操作时间
        /// </summary>
        public DateTimeOffset ExecutionTime { get; set; }

        /// <summary>
        /// 消耗时间（毫秒）
        /// </summary>
        public int ExecutionDuration { get; set; }
    }
}