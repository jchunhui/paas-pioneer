﻿using System;

namespace Paas.Pioneer.Admin.Core.Application.Contracts.OperationLog.Dto.Input;

public class OperationLogInput
{
    public string UserName { get; set; }
}