﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Paas.Pioneer.Admin.Core.Application.Contracts.OperationLog;
using Paas.Pioneer.Admin.Core.Application.Contracts.OperationLog.Dto.Input;
using Paas.Pioneer.Admin.Core.Application.Contracts.OperationLog.Dto.Output;
using Paas.Pioneer.Domain.Shared.Dto.Input;
using Paas.Pioneer.Domain.Shared.Dto.Output;
using Volo.Abp.Application.Services;
using Volo.Abp.AuditLogging;

namespace Paas.Pioneer.Admin.Core.Application.OperationLog;

public class OperationLogService : ApplicationService, IOperationLogService
{
    private readonly IAuditLogRepository _auditLogRepository;

    /// <summary>
    /// 构造函数
    /// </summary>
    /// <param name="auditLogRepository"></param>
    public OperationLogService(IAuditLogRepository auditLogRepository)
    {
        _auditLogRepository = auditLogRepository;
    }

    public async Task<Page<OperationLogListOutput>> GetPageListAsync(PageInput<OperationLogInput> input)
    {
        return new Page<OperationLogListOutput>()
        {
            Total = await _auditLogRepository.GetCountAsync(userName: input.Filter.UserName.IsNullOrEmpty()
                ? null
                : input.Filter.UserName),
            List = ObjectMapper.Map<List<AuditLog>, List<OperationLogListOutput>>(
                await _auditLogRepository.GetListAsync(
                    userName: input.Filter.UserName.IsNullOrEmpty() ? null : input.Filter.UserName,
                    skipCount: (input.CurrentPage - 1) * input.PageSize,
                    maxResultCount: input.PageSize)),
        };
    }
}