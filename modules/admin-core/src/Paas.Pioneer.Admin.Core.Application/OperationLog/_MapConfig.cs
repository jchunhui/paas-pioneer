﻿using AutoMapper;
using Paas.Pioneer.Admin.Core.Application.Contracts.OperationLog.Dto.Output;
using Volo.Abp.AuditLogging;

namespace Paas.Pioneer.Admin.Core.Application.OperationLog;

public class _MapConfig : Profile
{
    public _MapConfig()
    {
        CreateMap<AuditLog, OperationLogListOutput>();
    }
}