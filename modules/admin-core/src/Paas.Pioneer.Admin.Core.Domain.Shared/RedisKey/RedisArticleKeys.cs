﻿using Microsoft.Extensions.Options;
using Paas.Pioneer.Domain.Shared.Configs;
using Volo.Abp.DependencyInjection;
using Volo.Abp.MultiTenancy;

namespace Paas.Pioneer.Admin.Core.Domain.Shared.RedisKey
{
    /// <summary>
    /// 新闻常量
    /// </summary>
    public class RedisArticleKeys : ISingletonDependency
    {
        private readonly ICurrentTenant _currentTenant;
        private readonly AppConfig _appConfig;

        public RedisArticleKeys(ICurrentTenant currentTenant,
            IOptions<AppConfig> appConfig)
        {
            _currentTenant = currentTenant;
            _appConfig = appConfig.Value;
        }

        /// <summary>
        /// 根目录
        /// </summary>
        private string Article()
        {
            return _appConfig.Tenant ? $"{_currentTenant.Id}:Article" : "Article";
        }

        /// <summary>
        /// ArticleSlideshow
        /// </summary>
        public string Slideshow => $"{Article()}:Slideshow:";

        /// <summary>
        /// ArticleType
        /// </summary>
        public string Type => $"{Article()}:Type";

        /// <summary>
        /// ArticleArticle
        /// </summary>
        public string Articles => $"{Article()}:Article:";
    }
}
