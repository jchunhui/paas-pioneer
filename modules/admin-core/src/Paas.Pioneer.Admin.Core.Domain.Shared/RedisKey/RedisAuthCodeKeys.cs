﻿using Microsoft.Extensions.Options;
using Paas.Pioneer.Domain.Shared.Configs;
using Volo.Abp.DependencyInjection;
using Volo.Abp.MultiTenancy;

namespace Paas.Pioneer.Admin.Core.Domain.Shared.RedisKey
{
    /// <summary>
    /// 验证码key
    /// </summary>
    public class RedisAuthCodeKeys : ISingletonDependency
    {
        private readonly ICurrentTenant _currentTenant;
        private readonly AppConfig _appConfig;

        public RedisAuthCodeKeys(ICurrentTenant currentTenant,
            IOptions<AppConfig> appConfig)
        {
            _currentTenant = currentTenant;
            _appConfig = appConfig.Value;
        }

        /// <summary>
        /// 根目录
        /// </summary>
        private string RedisAuth()
        {
            return _appConfig.Tenant ? $"{_currentTenant.Id}:RedisAuth" : "RedisAuth";
        }

        /// <summary>
        /// 注册
        /// </summary>
        public string Register => $"{RedisAuth()}:Register:";

        /// <summary>
        /// 登录
        /// </summary>
        public string Login => $"{RedisAuth()}:Login:";

        /// <summary>
        /// 修改账号密码
        /// </summary>
        public string UpdatePassword => $"{RedisAuth()}:UpdatePassword:";

        /// <summary>
        /// 绑定手机号
        /// </summary>
        public string BindPhone => $"{RedisAuth()}:BindPhone:";

        /// <summary>
        /// 图形验证码
        /// </summary>
        public string ImageVerifyCode => $"{RedisAuth()}:ImageVerifyCode:";
    }
}