﻿using System.ComponentModel;

namespace Paas.Pioneer.Admin.Core.Domain.Shared.Enum
{
    /// <summary>
    /// 平台费用
    /// </summary>
    public enum EPlatformCostType
    {
        /// <summary>
        /// 曝光位费用
        /// </summary>
        [Description("曝光位费用")]
        Exposure = 1,

    }
}
