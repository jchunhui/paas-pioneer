﻿using System.ComponentModel;

namespace Paas.Pioneer.Admin.Core.Domain.Shared.Enum
{
    /// <summary>
    /// 用户类型
    /// </summary>
    public enum EUserType
    {
        /// <summary>
        /// 微信用户
        /// </summary>
        [Description("微信用户")]
        UserWeChat = 1
    }
}
