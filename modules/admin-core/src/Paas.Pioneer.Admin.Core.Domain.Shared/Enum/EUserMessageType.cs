﻿using System.ComponentModel;

namespace Paas.Pioneer.Admin.Core.Domain.Shared.Enum
{
    /// <summary>
    /// 用户消息类型
    /// </summary>
    public enum EUserMessageType
    {
        /// <summary>
        /// 个人通知
        /// </summary>
        [Description("个人通知")]
        PersonalNotice = 1,
        /// <summary>
        /// 全站通知
        /// </summary>
        [Description("全站通知")]
        SitewideNotification = 2,
    }
}
