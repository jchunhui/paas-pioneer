﻿using System.ComponentModel;

namespace Paas.Pioneer.Admin.Core.Domain.Shared.Enum
{
    /// <summary>
    /// 作品发布状态 1草稿，2发布
    /// </summary>
    public enum  EWorksState
    {
        /// <summary>
        /// 草稿
        /// </summary>
        [Description("草稿")]
        Draft = 1,

        /// <summary>
        /// 发布
        /// </summary>
        [Description("发布")]
        Release = 2
    }
}
