﻿using System.ComponentModel;

namespace Paas.Pioneer.Admin.Core.Domain.Shared.Enum
{
    /// <summary>
    /// 公共信息枚举
    /// </summary>
    public enum ECommonMessageType
    {
        /// <summary>
        /// 行程平台介绍
        /// </summary>
        [Description("行程平台介绍")]
        JourneyIntroduce = 1,
    }
}
