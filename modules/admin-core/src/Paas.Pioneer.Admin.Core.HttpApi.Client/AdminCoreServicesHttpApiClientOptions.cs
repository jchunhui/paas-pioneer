﻿using System;
using System.Net.Http;

namespace Paas.Pioneer.Admin.Core.HttpApi.Client
{
    public class AdminCoreServicesHttpApiClientOptions
    {
        public string RemoteServiceName { get; } = "AdminCore";
        public string RemoteSectionUrl => $"RemoteServices:{RemoteServiceName}:BaseUrl";

        public Func<DelegatingHandler> DelegatingHandlerFunc { get; set; }
    }
}