﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Paas.Pioneer.Admin.Core.Application.Contracts.Personnel.Position;
using Paas.Pioneer.Admin.Core.Application.Contracts.Personnel.Position.Dto;
using Paas.Pioneer.Admin.Core.Application.Contracts.Personnel.Position.Dto.Input;
using Paas.Pioneer.Admin.Core.Application.Contracts.Personnel.Position.Dto.Output;
using Paas.Pioneer.Domain.Shared.Dto.Input;
using System;
using System.Threading.Tasks;
using Volo.Abp.AspNetCore.Mvc;
using Paas.Pioneer.Domain.Shared.Dto.Output;
using Volo.Abp.Auditing;

namespace Paas.Pioneer.Admin.Core.HttpApi.Controllers.Position;

/// <summary>
/// 职位管理
/// </summary>
[Route("api/personnel/[controller]")]
[Authorize]
public class PositionController : AbpControllerBase
{
    private readonly IPositionService _positionService;

    /// <summary>
    /// 构造函数
    /// </summary>
    /// <param name="positionService"></param>
    public PositionController(IPositionService positionService)
    {
        _positionService = positionService;
    }

    /// <summary>
    /// 查询单条职位
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("{id}")]
    public async Task<PositionDataOutput> Get(Guid id)
    {
        return await _positionService.GetAsync(id);
    }

    /// <summary>
    /// 查询分页职位
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("page-list")]

    public async Task<Page<PositionListOutput>> GetPageList([FromBody] PageInput<PositionDataOutput> model)
    {
        return await _positionService.GetPageListAsync(model);
    }

    /// <summary>
    /// 新增职位
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task Add([FromBody] PositionAddInput input)
    {
        await _positionService.AddAsync(input);
    }

    /// <summary>
    /// 修改职位
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPut]
    public async Task Update([FromBody] PositionUpdateInput input)
    {
        await _positionService.UpdateAsync(input);
    }

    /// <summary>
    /// 删除职位
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpDelete("{id}")]
    public async Task SoftDelete(Guid id)
    {
        await _positionService.DeleteAsync(id);
    }

    /// <summary>
    /// 批量删除职位
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    [HttpDelete("ids")]
    public async Task BatchSoftDelete([FromBody] Guid[] ids)
    {
        await _positionService.BatchSoftDeleteAsync(ids);
    }
}