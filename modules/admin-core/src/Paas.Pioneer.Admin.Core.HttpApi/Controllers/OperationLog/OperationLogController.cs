﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Paas.Pioneer.Admin.Core.Application.Contracts.OperationLog;
using Paas.Pioneer.Admin.Core.Application.Contracts.OperationLog.Dto.Input;
using Paas.Pioneer.Admin.Core.Application.Contracts.OperationLog.Dto.Output;
using Paas.Pioneer.Domain.Shared.Dto.Input;
using Paas.Pioneer.Domain.Shared.Dto.Output;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Auditing;

namespace Paas.Pioneer.Admin.Core.HttpApi.Controllers.OperationLog;

/// <summary>
/// 登录日志管理
/// </summary>
[Route("api/admin/[controller]")]
[Authorize]
public class OperationLogController : AbpControllerBase
{
    private readonly IOperationLogService _operationLogService;

    /// <summary>
    /// 构造函数
    /// </summary>
    /// <param name="operationLogService"></param>
    public OperationLogController(IOperationLogService operationLogService)
    {
        _operationLogService = operationLogService;
    }

    /// <summary>
    /// 查询分页登录日志
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("page-list")]

    public async Task<Page<OperationLogListOutput>> GetPageList([FromBody] PageInput<OperationLogInput> input)
    {
        return await _operationLogService.GetPageListAsync(input);
    }
}