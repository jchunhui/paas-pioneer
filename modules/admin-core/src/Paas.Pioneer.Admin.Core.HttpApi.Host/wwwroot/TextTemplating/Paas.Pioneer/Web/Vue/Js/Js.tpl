﻿import request from '@/utils/request'
import scope from './scope'
const apiPrefix = `${process.env.VUE_APP_BASE_API}/${scope}/{{model.taxon}}/`

// {{model.function_module}}管理
export const getPageList = (params, config = {}) => {
    return request.post(apiPrefix + 'page-list', params, config)
}
export const get = (params, config = {}) => {
    return request.get(apiPrefix + params.id, { params: params, ...config })
}
export const add = (params, config = {}) => {
    return request.post(apiPrefix, params, config)
}
export const update = (params, config = {}) => {
    return request.put(apiPrefix, params, config)
}
export const softDelete = (params, config = {}) => {
    return request.delete(apiPrefix + params.id, { params: params, ...config })
}
export const batchSoftDelete = (params, config = {}) => {
    return request.delete(apiPrefix + 'ids', { data: params, ...config })
}