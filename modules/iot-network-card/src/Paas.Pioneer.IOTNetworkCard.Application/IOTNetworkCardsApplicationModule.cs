﻿using Paas.Pioneer.Admin.Core.HttpApi.Client;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts;
using Paas.Pioneer.IOTNetworkCard.Domain;
using Paas.Pioneer.IOTNetworkCard.RequestFactory;
using Volo.Abp.AutoMapper;
using Volo.Abp.Modularity;
using Volo.Abp.TenantManagement;

namespace Paas.Pioneer.IOTNetworkCard.Application
{
    [DependsOn(
        typeof(IOTNetworkCardsDomainModule),
        typeof(IOTNetworkCardsApplicationContractsModule),
        typeof(AbpTenantManagementApplicationModule),
        typeof(AdminsHttpApiClientModule),
        typeof(RequestFactoryModule)
    )]
    public class IOTNetworkCardsApplicationModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpAutoMapperOptions>(options => { options.AddMaps<IOTNetworkCardsApplicationModule>(); });
        }
    }
}