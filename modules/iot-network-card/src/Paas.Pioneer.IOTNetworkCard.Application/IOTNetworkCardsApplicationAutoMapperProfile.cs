﻿using AutoMapper;

namespace Paas.Pioneer.IOTNetworkCard.Application
{
    public class IOTNetworkCardsApplicationAutoMapperProfile : Profile
    {
        public IOTNetworkCardsApplicationAutoMapperProfile()
        {
            /* You can configure your AutoMapper mapping configuration here.
             * Alternatively, you can split your mapping configurations
             * into multiple profile classes for a better organization. */
        }
    }
}
