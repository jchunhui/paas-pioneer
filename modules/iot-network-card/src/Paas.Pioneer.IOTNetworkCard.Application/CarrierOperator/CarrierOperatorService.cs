﻿using Paas.Pioneer.Domain.Shared.Dto.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Paas.Pioneer.Admin.Core.HttpApi.Client.ServiceProxies;
using Volo.Abp.Application.Services;
using Volo.Abp;
using Paas.Pioneer.Domain.Shared.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Domain.CarrierOperator;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.CarrierOperator.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.CarrierOperator.Dto.Input;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.CarrierOperator;

namespace Paas.Pioneer.IOTNetworkCard.Application.CarrierOperator
{
    /// <summary>
    /// 运营商管理服务
    /// </summary>
    public class CarrierOperatorService : ApplicationService, ICarrierOperatorService
    {
        private readonly ICarrierOperatorRepository _carrierOperatorRepository;
        private readonly IDictionaryServiceProxy _dictionaryServiceProxy;

        /// <summary>
        /// 构造函数
        /// </summary>
        public CarrierOperatorService(ICarrierOperatorRepository carrierOperatorRepository,
            IDictionaryServiceProxy dictionaryServiceProxy)
        {
            _carrierOperatorRepository = carrierOperatorRepository;
            _dictionaryServiceProxy = dictionaryServiceProxy;
        }

        /// <summary>
        /// 查询运营商管理
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public async Task<GetCarrierOperatorOutput> GetAsync(Guid id)
        {
            var result = await _carrierOperatorRepository.GetAsync(p => p.Id == id, x => new GetCarrierOperatorOutput()
            {
                Name = x.Name,
                Account = x.Account,
                Password = x.Password,
                SecretKey = x.SecretKey,
                DictionaryId = x.DictionaryId,
                BindPhoneDictionaryId = x.BindPhoneDictionaryId,
                Sort = x.Sort,
                ShutdownThreshold = x.ShutdownThreshold,
                IsDisable = x.IsDisable,
                Remark = x.Remark,
                LastModificationTime = x.LastModificationTime,
                CreationTime = x.CreationTime,
                Id = x.Id,
            });
            return result;
        }

        /// <summary>
        /// 查询分页运营商管理
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        public async Task<Page<GetCarrierOperatorPageListOutput>> GetPageListAsync(
            PageInput<GetCarrierOperatorPageListInput> input)
        {
            var data = await _carrierOperatorRepository.GetResponseOutputPageListAsync(x =>
                new GetCarrierOperatorPageListOutput
                {
                    Name = x.Name,
                    Account = x.Account,
                    Password = x.Password,
                    SecretKey = x.SecretKey,
                    DictionaryId = x.DictionaryId,
                    BindPhoneDictionaryId = x.BindPhoneDictionaryId,
                    Sort = x.Sort,
                    ShutdownThreshold = x.ShutdownThreshold,
                    IsDisable = x.IsDisable,
                    Remark = x.Remark,
                    LastModificationTime = x.LastModificationTime,
                    CreationTime = x.CreationTime,
                    Id = x.Id,
                }, input: input);

            if (data.Total == 0)
            {
                return data;
            }

            var dictionaryList =
                await _dictionaryServiceProxy.GetListAsync(data.List.Select(x => x.DictionaryId).Distinct());

            var bindPhoneDictionaryList =
                await _dictionaryServiceProxy.GetListAsync(data.List.Select(x => x.BindPhoneDictionaryId).Distinct());

            foreach (var item in data.List)
            {
                item.DictionaryName = dictionaryList?.FirstOrDefault(x => x.Id == item.DictionaryId)?.Name;
                item.BindPhoneDictionaryName = bindPhoneDictionaryList?.FirstOrDefault(x => x.Id == item.BindPhoneDictionaryId)?.Name;
            }

            return data;
        }

        /// <summary>
        /// 新增运营商管理
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        public async Task AddAsync(AddCarrierOperatorInput input)
        {
            var carrierOperator = ObjectMapper.Map<AddCarrierOperatorInput, CarrierOperatorEntity>(input);
            await _carrierOperatorRepository.InsertAsync(carrierOperator);
        }

        /// <summary>
        /// 修改运营商管理
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        public async Task UpdateAsync(UpdateCarrierOperatorInput input)
        {
            var entity = await _carrierOperatorRepository.GetAsync(input.Id);
            if (entity?.Id == Guid.Empty)
            {
                throw new BusinessException("数据不存在！");
            }

            ObjectMapper.Map(input, entity);
            await _carrierOperatorRepository.UpdateAsync(entity);
        }

        /// <summary>
        /// 删除运营商管理
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public async Task DeleteAsync(Guid id)
        {
            await _carrierOperatorRepository.DeleteAsync(m => m.Id == id);
        }

        /// <summary>
        /// 批量删除运营商管理
        /// </summary>
        /// <param name="ids">主键集合</param>
        /// <returns></returns>
        public async Task BatchSoftDeleteAsync(IEnumerable<Guid> ids)
        {
            await _carrierOperatorRepository.DeleteAsync(x => ids.Contains(x.Id));
        }

        public async Task<IEnumerable<GetCarrierOperatorListOutput>> GetCarrierOperatorListAsync()
        {
            var result = await _carrierOperatorRepository.GetListAsync(p => true,
                x => new GetCarrierOperatorListOutput
                {
                    Name = x.Name,
                    Id = x.Id,
                });
            return result;
        }
    }
}