﻿using AutoMapper;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.CarrierOperator.Dto.Input;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.CarrierOperator.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Domain.CarrierOperator;

namespace Paas.Pioneer.IOTNetworkCard.Application.CarrierOperator
{
    public class _MapConfig : Profile
    {
        public _MapConfig()
        {
            CreateMap<CarrierOperatorEntity, GetCarrierOperatorOutput>();

            CreateMap<AddCarrierOperatorInput, CarrierOperatorEntity>();
            CreateMap<UpdateCarrierOperatorInput, CarrierOperatorEntity>();
        }
    }
}