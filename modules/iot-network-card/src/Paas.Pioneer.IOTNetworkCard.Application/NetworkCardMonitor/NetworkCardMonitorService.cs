﻿using Microsoft.EntityFrameworkCore;
using Paas.Pioneer.Domain.Shared.Dto.Input;
using Paas.Pioneer.Domain.Shared.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;
using Volo.Abp;
using Paas.Pioneer.Domain.Shared.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Domain.NetworkCardMonitor;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCardMonitor.Dto.Input;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCardMonitor;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCardMonitor.Dto.Output;

namespace Paas.Pioneer.IOTNetworkCard.Application.NetworkCardMonitor
{
    /// <summary>
    /// 网卡监控服务
    /// </summary>
    public class NetworkCardMonitorService : ApplicationService, INetworkCardMonitorService
    {

        private readonly INetworkCardMonitorRepository _networkCardMonitorRepository;

        /// <summary>
        /// 构造函数
        /// </summary>
        public NetworkCardMonitorService(INetworkCardMonitorRepository networkCardMonitorRepository)
        {
            _networkCardMonitorRepository = networkCardMonitorRepository;
        }

        /// <summary>
        /// 查询网卡监控
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public async Task<GetNetworkCardMonitorOutput> GetAsync(Guid id)
        {
            var result = await _networkCardMonitorRepository.GetAsync(p => p.Id == id, x => new GetNetworkCardMonitorOutput()
            {
                NetworkCardId = x.NetworkCardId,
                UseFlow = x.UseFlow,
                CreationTime = x.CreationTime,
                Id = x.Id,
            });
            return result;
        }

        /// <summary>
        /// 查询分页网卡监控
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        public async Task<Page<GetNetworkCardMonitorPageListOutput>> GetPageListAsync(PageInput<GetNetworkCardMonitorPageListInput> input)
        {
            var data = await _networkCardMonitorRepository.GetResponseOutputPageListAsync(x => new GetNetworkCardMonitorPageListOutput
            {
                NetworkCardId = x.NetworkCardId,
                UseFlow = x.UseFlow,
                CreationTime = x.CreationTime,
                Id = x.Id,
            }, input: input);
            return data;
        }

        /// <summary>
        /// 新增网卡监控
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        public async Task AddAsync(AddNetworkCardMonitorInput input)
        {
            var networkCardMonitor = ObjectMapper.Map<AddNetworkCardMonitorInput, NetworkCardMonitorEntity>(input);
            await _networkCardMonitorRepository.InsertAsync(networkCardMonitor);
        }

        /// <summary>
        /// 修改网卡监控
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        public async Task UpdateAsync(UpdateNetworkCardMonitorInput input)
        {
            var entity = await _networkCardMonitorRepository.GetAsync(input.Id);
            if (entity?.Id == Guid.Empty)
            {
                throw new BusinessException("数据不存在！");
            }
            ObjectMapper.Map(input, entity);
            await _networkCardMonitorRepository.UpdateAsync(entity);
        }

        /// <summary>
        /// 删除网卡监控
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public async Task DeleteAsync(Guid id)
        {
            await _networkCardMonitorRepository.DeleteAsync(m => m.Id == id);
        }

        /// <summary>
        /// 批量删除网卡监控
        /// </summary>
        /// <param name="ids">主键集合</param>
        /// <returns></returns>
        public async Task BatchSoftDeleteAsync(IEnumerable<Guid> ids)
        {
            await _networkCardMonitorRepository.DeleteAsync(x => ids.Contains(x.Id));
        }
    }
}