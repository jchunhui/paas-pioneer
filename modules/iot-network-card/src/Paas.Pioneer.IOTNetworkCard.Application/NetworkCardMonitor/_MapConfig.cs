﻿using AutoMapper;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCardMonitor.Dto.Input;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCardMonitor.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Domain.NetworkCardMonitor;

namespace Paas.Pioneer.IOTNetworkCard.Application.NetworkCardMonitor
{
    public class _MapConfig : Profile
    {
        public _MapConfig()
        {
            CreateMap<NetworkCardMonitorEntity, GetNetworkCardMonitorOutput>();

            CreateMap<AddNetworkCardMonitorInput, NetworkCardMonitorEntity>();
            CreateMap<UpdateNetworkCardMonitorInput, NetworkCardMonitorEntity>();
        }
    }
}