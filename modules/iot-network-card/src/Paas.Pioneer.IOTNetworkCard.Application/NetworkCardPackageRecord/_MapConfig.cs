﻿using AutoMapper;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCardPackageRecord.Dto.Input;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCardPackageRecord.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Domain.NetworkCardPackageRecord;

namespace Paas.Pioneer.IOTNetworkCard.Application.NetworkCardPackageRecord
{
    public class _MapConfig : Profile
    {
        public _MapConfig()
        {
            CreateMap<NetworkCardPackageRecordEntity, GetNetworkCardPackageRecordOutput>();

            CreateMap<AddNetworkCardPackageRecordInput, NetworkCardPackageRecordEntity>();
            CreateMap<UpdateNetworkCardPackageRecordInput, NetworkCardPackageRecordEntity>();
        }
    }
}