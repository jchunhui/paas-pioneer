﻿using Microsoft.EntityFrameworkCore;
using Paas.Pioneer.Domain.Shared.Dto.Input;
using Paas.Pioneer.Domain.Shared.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;
using Volo.Abp;
using Paas.Pioneer.Domain.Shared.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Domain.NetworkCardPackageRecord;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCardPackageRecord.Dto.Input;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCardPackageRecord;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCardPackageRecord.Dto.Output;

namespace Paas.Pioneer.IOTNetworkCard.Application.NetworkCardPackageRecord
{
    /// <summary>
    /// 网卡套餐记录服务
    /// </summary>
    public class NetworkCardPackageRecordService : ApplicationService, INetworkCardPackageRecordService
    {

        private readonly INetworkCardPackageRecordRepository _networkCardPackageRecordRepository;

        /// <summary>
        /// 构造函数
        /// </summary>
        public NetworkCardPackageRecordService(INetworkCardPackageRecordRepository networkCardPackageRecordRepository)
        {
            _networkCardPackageRecordRepository = networkCardPackageRecordRepository;
        }

        /// <summary>
        /// 查询网卡套餐记录
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public async Task<GetNetworkCardPackageRecordOutput> GetAsync(Guid id)
        {
            var result = await _networkCardPackageRecordRepository.GetAsync(p => p.Id == id, x => new GetNetworkCardPackageRecordOutput()
            {
                NetworkCardId = x.NetworkCardId,
                PackageId = x.PackageId,
                CreationTime = x.CreationTime,
                Id = x.Id,
            });
            return result;
        }

        /// <summary>
        /// 查询分页网卡套餐记录
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        public async Task<Page<GetNetworkCardPackageRecordPageListOutput>> GetPageListAsync(PageInput<GetNetworkCardPackageRecordPageListInput> input)
        {
            var data = await _networkCardPackageRecordRepository.GetResponseOutputPageListAsync(x => new GetNetworkCardPackageRecordPageListOutput
            {
                NetworkCardId = x.NetworkCardId,
                PackageId = x.PackageId,
                CreationTime = x.CreationTime,
                Id = x.Id,
            }, input: input);
            return data;
        }

        /// <summary>
        /// 新增网卡套餐记录
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        public async Task AddAsync(AddNetworkCardPackageRecordInput input)
        {
            var networkCardPackageRecord = ObjectMapper.Map<AddNetworkCardPackageRecordInput, NetworkCardPackageRecordEntity>(input);
            await _networkCardPackageRecordRepository.InsertAsync(networkCardPackageRecord);
        }

        /// <summary>
        /// 修改网卡套餐记录
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        public async Task UpdateAsync(UpdateNetworkCardPackageRecordInput input)
        {
            var entity = await _networkCardPackageRecordRepository.GetAsync(input.Id);
            if (entity?.Id == Guid.Empty)
            {
                throw new BusinessException("数据不存在！");
            }
            ObjectMapper.Map(input, entity);
            await _networkCardPackageRecordRepository.UpdateAsync(entity);
        }

        /// <summary>
        /// 删除网卡套餐记录
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public async Task DeleteAsync(Guid id)
        {
            await _networkCardPackageRecordRepository.DeleteAsync(m => m.Id == id);
        }

        /// <summary>
        /// 批量删除网卡套餐记录
        /// </summary>
        /// <param name="ids">主键集合</param>
        /// <returns></returns>
        public async Task BatchSoftDeleteAsync(IEnumerable<Guid> ids)
        {
            await _networkCardPackageRecordRepository.DeleteAsync(x => ids.Contains(x.Id));
        }
    }
}