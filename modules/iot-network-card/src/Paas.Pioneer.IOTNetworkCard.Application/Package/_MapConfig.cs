﻿using AutoMapper;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.Package.Dto.Input;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.Package.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Domain.Package;

namespace Paas.Pioneer.IOTNetworkCard.Application.Package
{
    public class _MapConfig : Profile
    {
        public _MapConfig()
        {
            CreateMap<PackageEntity, GetPackageOutput>();

            CreateMap<AddPackageInput, PackageEntity>();
            CreateMap<UpdatePackageInput, PackageEntity>();
        }
    }
}