﻿using Paas.Pioneer.Domain.Shared.Dto.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Paas.Pioneer.Admin.Core.HttpApi.Client.ServiceProxies;
using Volo.Abp.Application.Services;
using Volo.Abp;
using Paas.Pioneer.Domain.Shared.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Domain.Package;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.Package.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.Package;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.Package.Dto.Input;
using Paas.Pioneer.IOTNetworkCard.Domain.CarrierOperator;

namespace Paas.Pioneer.IOTNetworkCard.Application.Package
{
    /// <summary>
    /// 套餐管理服务
    /// </summary>
    public class PackageService : ApplicationService, IPackageService
    {
        private readonly IPackageRepository _packageRepository;
        private readonly IDictionaryServiceProxy _dictionaryServiceProxy;
        private readonly ICarrierOperatorRepository _carrierOperatorRepository;

        /// <summary>
        /// 构造函数
        /// </summary>
        public PackageService(IPackageRepository packageRepository, IDictionaryServiceProxy dictionaryServiceProxy,
            ICarrierOperatorRepository carrierOperatorRepository)
        {
            _packageRepository = packageRepository;
            _dictionaryServiceProxy = dictionaryServiceProxy;
            _carrierOperatorRepository = carrierOperatorRepository;
        }

        /// <summary>
        /// 查询套餐管理
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public async Task<GetPackageOutput> GetAsync(Guid id)
        {
            var result = await _packageRepository.GetAsync(p => p.Id == id, x => new GetPackageOutput()
            {
                CarrierOperatorId = x.CarrierOperatorId,
                CarrierOperatorPackageDictionaryId = x.CarrierOperatorPackageDictionaryId,
                RechargeDictionaryId = x.RechargeDictionaryId,
                Name = x.Name,
                Sort = x.Sort,
                DictionaryId = x.DictionaryId,
                IsShow = x.IsShow,
                MaxRechargeCount = x.MaxRechargeCount,
                Count = x.Count,
                CostPrice = x.CostPrice,
                PackageAmount = x.PackageAmount,
                EmptyFlow = x.EmptyFlow,
                RealFlow = x.RealFlow,
                IsDisable = x.IsDisable,
                IsHeat = x.IsHeat,
                IsOpenAccount = x.IsOpenAccount,
                OpenDateTime = x.OpenDateTime,
                CloseDateTime = x.CloseDateTime,
                Remark = x.Remark,
                LastModificationTime = x.LastModificationTime,
                CreationTime = x.CreationTime,
                Id = x.Id,
            });
            return result;
        }

        /// <summary>
        /// 查询分页套餐管理
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        public async Task<Page<GetPackagePageListOutput>> GetPageListAsync(PageInput<GetPackagePageListInput> input)
        {
            var data = await _packageRepository.GetResponseOutputPageListAsync(x => new GetPackagePageListOutput
            {
                CarrierOperatorId = x.CarrierOperatorId,
                CarrierOperatorPackageDictionaryId = x.CarrierOperatorPackageDictionaryId,
                RechargeDictionaryId = x.RechargeDictionaryId,
                Name = x.Name,
                Sort = x.Sort,
                DictionaryId = x.DictionaryId,
                IsShow = x.IsShow,
                MaxRechargeCount = x.MaxRechargeCount,
                Count = x.Count,
                CostPrice = x.CostPrice,
                PackageAmount = x.PackageAmount,
                EmptyFlow = x.EmptyFlow,
                RealFlow = x.RealFlow,
                IsDisable = x.IsDisable,
                IsHeat = x.IsHeat,
                IsOpenAccount = x.IsOpenAccount,
                OpenDateTime = x.OpenDateTime,
                CloseDateTime = x.CloseDateTime,
                Remark = x.Remark,
                LastModificationTime = x.LastModificationTime,
                CreationTime = x.CreationTime,
                Id = x.Id,
            }, input: input);

            if (data.Total == 0)
            {
                return data;
            }

            var dictionaryList =
                await _dictionaryServiceProxy.GetListAsync(data.List.Select(x => x.DictionaryId).Distinct());

            var rechargeDictionaryList =
                await _dictionaryServiceProxy.GetListAsync(data.List.Select(x => x.RechargeDictionaryId).Distinct());

            var carrierOperatorPackageDictionaryList =
                await _dictionaryServiceProxy.GetListAsync(data.List.Select(x => x.CarrierOperatorPackageDictionaryId).Distinct());

            var _carrierOperatorList =
                await _carrierOperatorRepository.GetListAsync(
                    x => data.List.Select(x => x.CarrierOperatorId).Contains(x.Id),
                    s => new
                    {
                        s.Id,
                        s.Name
                    });

            foreach (var item in data.List)
            {
                item.DictionaryName = dictionaryList?.FirstOrDefault(x => x.Id == item.DictionaryId)?.Name;

                item.RechargeDictionaryName =
                    rechargeDictionaryList?.FirstOrDefault(x => x.Id == item.RechargeDictionaryId)?.Name;

                item.CarrierOperatorPackageDictionaryName = carrierOperatorPackageDictionaryList?.FirstOrDefault(
                    x =>
                        x.Id == item.CarrierOperatorPackageDictionaryId)?.Name;

                item.CarrierOperatorName =
                    _carrierOperatorList.FirstOrDefault(x => x.Id == item.CarrierOperatorId)?.Name;
            }


            return data;
        }

        /// <summary>
        /// 新增套餐管理
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        public async Task AddAsync(AddPackageInput input)
        {
            var package = ObjectMapper.Map<AddPackageInput, PackageEntity>(input);
            await _packageRepository.InsertAsync(package);
        }

        /// <summary>
        /// 修改套餐管理
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        public async Task UpdateAsync(UpdatePackageInput input)
        {
            var entity = await _packageRepository.GetAsync(input.Id);
            if (entity?.Id == Guid.Empty)
            {
                throw new BusinessException("数据不存在！");
            }

            ObjectMapper.Map(input, entity);
            await _packageRepository.UpdateAsync(entity);
        }

        /// <summary>
        /// 删除套餐管理
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public async Task DeleteAsync(Guid id)
        {
            await _packageRepository.DeleteAsync(m => m.Id == id);
        }

        /// <summary>
        /// 批量删除套餐管理
        /// </summary>
        /// <param name="ids">主键集合</param>
        /// <returns></returns>
        public async Task BatchSoftDeleteAsync(IEnumerable<Guid> ids)
        {
            await _packageRepository.DeleteAsync(x => ids.Contains(x.Id));
        }
    }
}