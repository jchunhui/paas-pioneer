﻿using AutoMapper;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.ImportNetworkCard.Dto.Input;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.ImportNetworkCard.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Domain.ImportNetworkCard;
using Paas.Pioneer.IOTNetworkCard.Domain.NetworkCard;

namespace Paas.Pioneer.IOTNetworkCard.Application.ImportNetworkCard
{
    public class _MapConfig : Profile
    {
        public _MapConfig()
        {
            CreateMap<ImportNetworkCardEntity, GetImportNetworkCardOutput>();

            CreateMap<AddImportNetworkCardInput, ImportNetworkCardEntity>();
            CreateMap<UpdateImportNetworkCardInput, ImportNetworkCardEntity>();

            CreateMap<AddSingleNetworkCardInput, ImportNetworkCardEntity>();

        }
    }
}