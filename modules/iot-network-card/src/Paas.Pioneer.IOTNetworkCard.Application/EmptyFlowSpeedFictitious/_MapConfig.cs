﻿using AutoMapper;
using Paas.Pioneer.IOTNetworkCard.Domain.EmptyFlowSpeedFictitious;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.EmptyFlowSpeedFictitious.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.EmptyFlowSpeedFictitious.Dto.Input;

namespace Paas.Pioneer.IOTNetworkCard.Application.EmptyFlowFictitious
{
    public class _MapConfig : Profile
    {
        public _MapConfig()
        {
            CreateMap<EmptyFlowSpeedFictitiousEntity, GetEmptyFlowSpeedFictitiousOutput>();

            CreateMap<AddEmptyFlowFictitiousInput, EmptyFlowSpeedFictitiousEntity>();

            CreateMap<AddSpeedFictitiousInput, EmptyFlowSpeedFictitiousEntity>();
        }
    }
}