﻿using AutoMapper;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.ImportNetworkCard.Dto.Input;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCard.Dto.Input;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCard.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Domain.NetworkCard;

namespace Paas.Pioneer.IOTNetworkCard.Application.NetworkCard
{
    public class _MapConfig : Profile
    {
        public _MapConfig()
        {
            CreateMap<NetworkCardEntity, GetNetworkCardOutput>();

            CreateMap<AddNetworkCardInput, NetworkCardEntity>();
            CreateMap<UpdateNetworkCardInput, NetworkCardEntity>();

            CreateMap<AddSingleNetworkCardInput, NetworkCardEntity>();
        }
    }
}