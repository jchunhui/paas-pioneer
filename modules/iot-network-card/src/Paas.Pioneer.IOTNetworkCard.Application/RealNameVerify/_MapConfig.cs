﻿using AutoMapper;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.RealNameVerify.Dto.Input;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.RealNameVerify.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Domain.RealNameVerify;

namespace Paas.Pioneer.IOTNetworkCard.Application.RealNameVerify
{
    public class _MapConfig : Profile
    {
        public _MapConfig()
        {
            CreateMap<RealNameVerifyEntity, GetRealNameVerifyOutput>();

            CreateMap<AddRealNameVerifyInput, RealNameVerifyEntity>();
            CreateMap<UpdateRealNameVerifyInput, RealNameVerifyEntity>();
        }
    }
}