﻿using Microsoft.EntityFrameworkCore;
using Paas.Pioneer.Domain.Shared.Dto.Input;
using Paas.Pioneer.Domain.Shared.Extensions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;
using Volo.Abp;
using Paas.Pioneer.Domain.Shared.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Domain.RealNameVerify;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.RealNameVerify.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.RealNameVerify.Dto.Input;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.RealNameVerify;

namespace Paas.Pioneer.IOTNetworkCard.Application.RealNameVerify
{
    /// <summary>
    /// 运营商实名验证管理服务
    /// </summary>
    public class RealNameVerifyService : ApplicationService, IRealNameVerifyService
    {
        private readonly IRealNameVerifyRepository _realNameVerifyRepository;

        /// <summary>
        /// 构造函数
        /// </summary>
        public RealNameVerifyService(IRealNameVerifyRepository realNameVerifyRepository)
        {
            _realNameVerifyRepository = realNameVerifyRepository;
        }

        /// <summary>
        /// 查询运营商实名验证管理
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public async Task<GetRealNameVerifyOutput> GetAsync(Guid id)
        {
            var result = await _realNameVerifyRepository.GetAsync(p => p.Id == id,
                x => new GetRealNameVerifyOutput()
                {
                    CarrierOperatorId = x.CarrierOperatorId,
                    VerifyDictionaryId = x.VerifyDictionaryId,
                    VerifyPhoneDictionaryId = x.VerifyPhoneDictionaryId,
                    VerifyUrl = x.VerifyUrl,
                    VerifyTip = x.VerifyTip,
                    CreationTime = x.CreationTime,
                    Id = x.Id,
                });
            return result;
        }

        public async Task<GetRealNameVerifyOutput> GetCarrierOperatorRealNameVerifyAsync(Guid carrierOperatorId)
        {
            var result = await _realNameVerifyRepository.GetAsync(p => p.CarrierOperatorId == carrierOperatorId, x =>
                new GetRealNameVerifyOutput()
                {
                    CarrierOperatorId = x.CarrierOperatorId,
                    VerifyDictionaryId = x.VerifyDictionaryId,
                    VerifyPhoneDictionaryId = x.VerifyPhoneDictionaryId,
                    VerifyUrl = x.VerifyUrl,
                    VerifyTip = x.VerifyTip,
                    CreationTime = x.CreationTime,
                    Id = x.Id,
                });
            return result;
        }

        /// <summary>
        /// 查询分页运营商实名验证管理
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        public async Task<Page<GetRealNameVerifyPageListOutput>> GetPageListAsync(
            PageInput<GetRealNameVerifyPageListInput> input)
        {
            var data = await _realNameVerifyRepository.GetResponseOutputPageListAsync(x =>
                new GetRealNameVerifyPageListOutput
                {
                    CarrierOperatorId = x.CarrierOperatorId,
                    VerifyDictionaryId = x.VerifyDictionaryId,
                    VerifyPhoneDictionaryId = x.VerifyPhoneDictionaryId,
                    VerifyUrl = x.VerifyUrl,
                    VerifyTip = x.VerifyTip,
                    CreationTime = x.CreationTime,
                    Id = x.Id,
                }, input: input);
            return data;
        }

        /// <summary>
        /// 新增运营商实名验证管理
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        public async Task AddAsync(AddRealNameVerifyInput input)
        {
            var realNameVerify = ObjectMapper.Map<AddRealNameVerifyInput, RealNameVerifyEntity>(input);
            await _realNameVerifyRepository.InsertAsync(realNameVerify);
        }

        /// <summary>
        /// 修改运营商实名验证管理
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        public async Task UpdateAsync(UpdateRealNameVerifyInput input)
        {
            Debug.Assert(input.Id != null, "input.Id != null");
            var entity = await _realNameVerifyRepository.GetAsync(input.Id.Value);
            if (entity?.Id == Guid.Empty)
            {
                throw new BusinessException("数据不存在！");
            }

            ObjectMapper.Map(input, entity);
            await _realNameVerifyRepository.UpdateAsync(entity);
        }

        /// <summary>
        /// 删除运营商实名验证管理
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public async Task DeleteAsync(Guid id)
        {
            await _realNameVerifyRepository.DeleteAsync(m => m.Id == id);
        }

        /// <summary>
        /// 批量删除运营商实名验证管理
        /// </summary>
        /// <param name="ids">主键集合</param>
        /// <returns></returns>
        public async Task BatchSoftDeleteAsync(IEnumerable<Guid> ids)
        {
            await _realNameVerifyRepository.DeleteAsync(x => ids.Contains(x.Id));
        }
    }
}