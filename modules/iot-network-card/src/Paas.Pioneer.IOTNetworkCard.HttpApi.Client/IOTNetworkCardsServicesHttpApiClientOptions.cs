﻿using System;
using System.Net.Http;

namespace Paas.Pioneer.IOTNetworkCard.HttpApi.Client
{
    public class IOTNetworkCardsServicesHttpApiClientOptions
    {
        public string RemoteServiceName { get; set; } = "IOTNetworkCards";
        public string RemoteSectionUrl
        {
            get
            {
                return $"RemoteServices:{RemoteServiceName}:BaseUrl";
            }
        }

        public Func<DelegatingHandler> DelegatingHandlerFunc { get; set; }

        public IOTNetworkCardsServicesHttpApiClientOptions()
        {

        }
    }
}
