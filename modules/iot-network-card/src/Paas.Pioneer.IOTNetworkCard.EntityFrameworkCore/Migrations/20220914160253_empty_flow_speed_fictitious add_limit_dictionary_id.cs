﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Paas.Pioneer.IOTNetworkCard.EntityFrameworkCore.Migrations
{
    public partial class empty_flow_speed_fictitiousadd_limit_dictionary_id : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "LimitDictionaryId",
                table: "IOT_EmptyFlowSpeedFictitious",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                comment: "限速字典Id",
                collation: "ascii_general_ci");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LimitDictionaryId",
                table: "IOT_EmptyFlowSpeedFictitious");
        }
    }
}
