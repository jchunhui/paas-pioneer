﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Paas.Pioneer.IOTNetworkCard.EntityFrameworkCore.Migrations
{
    public partial class init_iot_network : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterDatabase()
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "IOT_CarrierOperator",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    Name = table.Column<string>(type: "varchar(50)", nullable: true, comment: "名称")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Account = table.Column<string>(type: "varchar(50)", nullable: true, comment: "账户")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Password = table.Column<string>(type: "varchar(50)", nullable: true, comment: "密码")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    SecretKey = table.Column<string>(type: "varchar(225)", nullable: true, comment: "密钥")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    DictionaryId = table.Column<Guid>(type: "char(36)", nullable: false, comment: "所属类别字典Id", collation: "ascii_general_ci"),
                    BindPhoneDictionaryId = table.Column<Guid>(type: "char(36)", nullable: false, comment: "绑定手机字典Id", collation: "ascii_general_ci"),
                    Sort = table.Column<int>(type: "int", nullable: false, comment: "排序"),
                    ShutdownThreshold = table.Column<int>(type: "int", nullable: false, comment: "停机阈值"),
                    IsDisable = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    Remark = table.Column<string>(type: "varchar(225)", nullable: true, comment: "备注")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    IsDeleted = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValue: false, comment: "是否删除"),
                    CreationTime = table.Column<DateTime>(type: "datetime(6)", nullable: false, comment: "创建时间"),
                    CreatorId = table.Column<Guid>(type: "char(36)", nullable: true, comment: "创建人", collation: "ascii_general_ci"),
                    LastModifierId = table.Column<Guid>(type: "char(36)", nullable: true, comment: "修改人", collation: "ascii_general_ci"),
                    LastModificationTime = table.Column<DateTime>(type: "datetime(6)", nullable: true, comment: "修改时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IOT_CarrierOperator", x => x.Id);
                },
                comment: "运营商管理")
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "IOT_EmptyFlowSpeedFictitious",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    PackageId = table.Column<Guid>(type: "char(36)", nullable: false, comment: "套餐Id", collation: "ascii_general_ci"),
                    EmptyFlowFictitiousJson = table.Column<string>(type: "varchar(4000)", nullable: true, comment: "虚拟Json")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    SpeedFictitiousJson = table.Column<string>(type: "varchar(4000)", nullable: true, comment: "限速Json")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    IsDeleted = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValue: false, comment: "是否删除"),
                    CreationTime = table.Column<DateTime>(type: "datetime(6)", nullable: false, comment: "创建时间"),
                    CreatorId = table.Column<Guid>(type: "char(36)", nullable: true, comment: "创建人", collation: "ascii_general_ci"),
                    LastModifierId = table.Column<Guid>(type: "char(36)", nullable: true, comment: "修改人", collation: "ascii_general_ci"),
                    LastModificationTime = table.Column<DateTime>(type: "datetime(6)", nullable: true, comment: "修改时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IOT_EmptyFlowSpeedFictitious", x => x.Id);
                },
                comment: "虚量限速配置表")
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "IOT_ImportNetworkCard",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    ImportSumCount = table.Column<int>(type: "int", nullable: false, comment: "导入总卡数"),
                    ImportSuccessCount = table.Column<int>(type: "int", nullable: false, comment: "导入成功卡数"),
                    CarrierOperatorId = table.Column<Guid>(type: "char(36)", nullable: false, comment: "运营商Id", collation: "ascii_general_ci"),
                    CarrierOperatorPackageDictionaryId = table.Column<Guid>(type: "char(36)", nullable: false, comment: "运营商套餐系列", collation: "ascii_general_ci"),
                    NetworkCardDictionaryId = table.Column<Guid>(type: "char(36)", nullable: false, comment: "网卡类型", collation: "ascii_general_ci"),
                    Remark = table.Column<string>(type: "varchar(225)", nullable: true, comment: "备注")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    IsDeleted = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValue: false, comment: "是否删除"),
                    CreationTime = table.Column<DateTime>(type: "datetime(6)", nullable: false, comment: "创建时间"),
                    CreatorId = table.Column<Guid>(type: "char(36)", nullable: true, comment: "创建人", collation: "ascii_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IOT_ImportNetworkCard", x => x.Id);
                },
                comment: "导入网卡记录")
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "IOT_NetworkCard",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    ICCID = table.Column<string>(type: "varchar(150)", nullable: true, comment: "ICCID")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    AccessPhone = table.Column<string>(type: "varchar(150)", nullable: true, comment: "接入号码")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    EmptyPhone = table.Column<string>(type: "varchar(100)", nullable: true, comment: "虚拟号")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    IMEI = table.Column<string>(type: "varchar(100)", nullable: true, comment: "IMEI")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    WalletBalance = table.Column<decimal>(type: "decimal(65,30)", nullable: false, comment: "钱包余额"),
                    GiftBalance = table.Column<decimal>(type: "decimal(65,30)", nullable: false, comment: "赠送余额"),
                    CarrierOperatorId = table.Column<Guid>(type: "char(36)", nullable: false, comment: "运营商Id", collation: "ascii_general_ci"),
                    CarrierOperatorPackageDictionaryId = table.Column<Guid>(type: "char(36)", nullable: false, comment: "运营商套餐系列", collation: "ascii_general_ci"),
                    IsApiRecharge = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    IsPollingCard = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    ImportCardId = table.Column<Guid>(type: "char(36)", nullable: false, comment: "导入网卡记录id", collation: "ascii_general_ci"),
                    Status = table.Column<int>(type: "int", nullable: false, comment: "卡状态"),
                    IsRealName = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    IsChanneling = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    NetworkCardDictionaryId = table.Column<Guid>(type: "char(36)", nullable: false, comment: "网卡类型", collation: "ascii_general_ci"),
                    Remark = table.Column<string>(type: "varchar(225)", nullable: true, comment: "备注")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    IsDeleted = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValue: false, comment: "是否删除"),
                    CreationTime = table.Column<DateTime>(type: "datetime(6)", nullable: false, comment: "创建时间"),
                    CreatorId = table.Column<Guid>(type: "char(36)", nullable: true, comment: "创建人", collation: "ascii_general_ci"),
                    LastModifierId = table.Column<Guid>(type: "char(36)", nullable: true, comment: "修改人", collation: "ascii_general_ci"),
                    LastModificationTime = table.Column<DateTime>(type: "datetime(6)", nullable: true, comment: "修改时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IOT_NetworkCard", x => x.Id);
                },
                comment: "网卡管理")
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "IOT_NetworkCardMonitor",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    NetworkCardId = table.Column<Guid>(type: "char(36)", nullable: false, comment: "网卡Id", collation: "ascii_general_ci"),
                    UseFlow = table.Column<int>(type: "int", nullable: false, comment: "使用流量"),
                    IsDeleted = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValue: false, comment: "是否删除"),
                    CreationTime = table.Column<DateTime>(type: "datetime(6)", nullable: false, comment: "创建时间"),
                    CreatorId = table.Column<Guid>(type: "char(36)", nullable: true, comment: "创建人", collation: "ascii_general_ci"),
                    LastModifierId = table.Column<Guid>(type: "char(36)", nullable: true, comment: "修改人", collation: "ascii_general_ci"),
                    LastModificationTime = table.Column<DateTime>(type: "datetime(6)", nullable: true, comment: "修改时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IOT_NetworkCardMonitor", x => x.Id);
                },
                comment: "网卡监控")
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "IOT_NetworkCardPackageRecord",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    NetworkCardId = table.Column<Guid>(type: "char(36)", nullable: false, comment: "网卡Id", collation: "ascii_general_ci"),
                    PackageId = table.Column<Guid>(type: "char(36)", nullable: false, comment: "套餐id", collation: "ascii_general_ci"),
                    IsDeleted = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValue: false, comment: "是否删除"),
                    CreationTime = table.Column<DateTime>(type: "datetime(6)", nullable: false, comment: "创建时间"),
                    CreatorId = table.Column<Guid>(type: "char(36)", nullable: true, comment: "创建人", collation: "ascii_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IOT_NetworkCardPackageRecord", x => x.Id);
                },
                comment: "网卡套餐记录")
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "IOT_Package",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    CarrierOperatorId = table.Column<Guid>(type: "char(36)", nullable: false, comment: "运营商Id", collation: "ascii_general_ci"),
                    CarrierOperatorPackageDictionaryId = table.Column<Guid>(type: "char(36)", nullable: false, comment: "运营商套餐系列", collation: "ascii_general_ci"),
                    RechargeDictionaryId = table.Column<Guid>(type: "char(36)", nullable: false, comment: "充值类型", collation: "ascii_general_ci"),
                    Name = table.Column<string>(type: "varchar(50)", nullable: true, comment: "名称")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Sort = table.Column<int>(type: "int", nullable: false, comment: "排序"),
                    DictionaryId = table.Column<Guid>(type: "char(36)", nullable: false, comment: "套餐类型", collation: "ascii_general_ci"),
                    IsShow = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    MaxRechargeCount = table.Column<int>(type: "int", nullable: false, comment: "每月最大充值数"),
                    Count = table.Column<int>(type: "int", nullable: false, comment: "套餐数量"),
                    CostPrice = table.Column<decimal>(type: "decimal(65,30)", nullable: false, comment: "成本价"),
                    PackageAmount = table.Column<decimal>(type: "decimal(65,30)", nullable: false, comment: "套餐金额"),
                    EmptyFlow = table.Column<int>(type: "int", nullable: false, comment: "虚量（MB）"),
                    RealFlow = table.Column<int>(type: "int", nullable: false, comment: "真实流量（MB）"),
                    IsDisable = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    IsHeat = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    IsOpenAccount = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    OpenDateTime = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    CloseDateTime = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    Remark = table.Column<string>(type: "varchar(225)", nullable: true, comment: "备注")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    IsDeleted = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValue: false, comment: "是否删除"),
                    CreationTime = table.Column<DateTime>(type: "datetime(6)", nullable: false, comment: "创建时间"),
                    CreatorId = table.Column<Guid>(type: "char(36)", nullable: true, comment: "创建人", collation: "ascii_general_ci"),
                    LastModifierId = table.Column<Guid>(type: "char(36)", nullable: true, comment: "修改人", collation: "ascii_general_ci"),
                    LastModificationTime = table.Column<DateTime>(type: "datetime(6)", nullable: true, comment: "修改时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IOT_Package", x => x.Id);
                },
                comment: "套餐管理")
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "IOT_RealNameVerify",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    CarrierOperatorId = table.Column<Guid>(type: "char(36)", nullable: false, comment: "运营商Id", collation: "ascii_general_ci"),
                    VerifyDictionaryId = table.Column<Guid>(type: "char(36)", nullable: false, comment: "验证方式字典Id", collation: "ascii_general_ci"),
                    VerifyPhoneDictionaryId = table.Column<Guid>(type: "char(36)", nullable: false, comment: "官方实名跳转复制号码字典Id", collation: "ascii_general_ci"),
                    VerifyUrl = table.Column<string>(type: "varchar(225)", nullable: true, comment: "验证地址")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    VerifyTip = table.Column<string>(type: "varchar(500)", nullable: true, comment: "验证提示")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    IsDeleted = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValue: false, comment: "是否删除"),
                    CreationTime = table.Column<DateTime>(type: "datetime(6)", nullable: false, comment: "创建时间"),
                    CreatorId = table.Column<Guid>(type: "char(36)", nullable: true, comment: "创建人", collation: "ascii_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IOT_RealNameVerify", x => x.Id);
                },
                comment: "运营商实名验证管理")
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateIndex(
                name: "IDX_Account",
                table: "IOT_CarrierOperator",
                column: "Account");

            migrationBuilder.CreateIndex(
                name: "IDX_PackageId",
                table: "IOT_EmptyFlowSpeedFictitious",
                column: "PackageId");

            migrationBuilder.CreateIndex(
                name: "IDX_CarrierOperatorId",
                table: "IOT_ImportNetworkCard",
                column: "CarrierOperatorId");

            migrationBuilder.CreateIndex(
                name: "IDX_CarrierOperatorId1",
                table: "IOT_NetworkCard",
                column: "CarrierOperatorId");

            migrationBuilder.CreateIndex(
                name: "IDX_ICCIDId",
                table: "IOT_NetworkCard",
                column: "ICCID");

            migrationBuilder.CreateIndex(
                name: "IDX_NetworkCardId",
                table: "IOT_NetworkCardMonitor",
                column: "NetworkCardId");

            migrationBuilder.CreateIndex(
                name: "IDX_NetworkCardId1",
                table: "IOT_NetworkCardPackageRecord",
                column: "NetworkCardId");

            migrationBuilder.CreateIndex(
                name: "IDX_CarrierOperatorId2",
                table: "IOT_Package",
                column: "CarrierOperatorId");

            migrationBuilder.CreateIndex(
                name: "IDX_CarrierOperatorId3",
                table: "IOT_RealNameVerify",
                column: "CarrierOperatorId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "IOT_CarrierOperator");

            migrationBuilder.DropTable(
                name: "IOT_EmptyFlowSpeedFictitious");

            migrationBuilder.DropTable(
                name: "IOT_ImportNetworkCard");

            migrationBuilder.DropTable(
                name: "IOT_NetworkCard");

            migrationBuilder.DropTable(
                name: "IOT_NetworkCardMonitor");

            migrationBuilder.DropTable(
                name: "IOT_NetworkCardPackageRecord");

            migrationBuilder.DropTable(
                name: "IOT_Package");

            migrationBuilder.DropTable(
                name: "IOT_RealNameVerify");
        }
    }
}
