﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Paas.Pioneer.IOTNetworkCard.EntityFrameworkCore.Migrations
{
    public partial class 优化小数点字段精度 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "PackageAmount",
                table: "IOT_Package",
                type: "decimal(5,2)",
                nullable: false,
                comment: "套餐金额",
                oldClrType: typeof(decimal),
                oldType: "decimal(65,30)",
                oldComment: "套餐金额");

            migrationBuilder.AlterColumn<decimal>(
                name: "CostPrice",
                table: "IOT_Package",
                type: "decimal(5,2)",
                nullable: false,
                comment: "成本价",
                oldClrType: typeof(decimal),
                oldType: "decimal(65,30)",
                oldComment: "成本价");

            migrationBuilder.AlterColumn<decimal>(
                name: "WalletBalance",
                table: "IOT_NetworkCard",
                type: "decimal(5,2)",
                nullable: false,
                comment: "钱包余额",
                oldClrType: typeof(decimal),
                oldType: "decimal(65,30)",
                oldComment: "钱包余额");

            migrationBuilder.AlterColumn<decimal>(
                name: "GiftBalance",
                table: "IOT_NetworkCard",
                type: "decimal(5,2)",
                nullable: false,
                comment: "赠送余额",
                oldClrType: typeof(decimal),
                oldType: "decimal(65,30)",
                oldComment: "赠送余额");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "PackageAmount",
                table: "IOT_Package",
                type: "decimal(65,30)",
                nullable: false,
                comment: "套餐金额",
                oldClrType: typeof(decimal),
                oldType: "decimal(5,2)",
                oldComment: "套餐金额");

            migrationBuilder.AlterColumn<decimal>(
                name: "CostPrice",
                table: "IOT_Package",
                type: "decimal(65,30)",
                nullable: false,
                comment: "成本价",
                oldClrType: typeof(decimal),
                oldType: "decimal(5,2)",
                oldComment: "成本价");

            migrationBuilder.AlterColumn<decimal>(
                name: "WalletBalance",
                table: "IOT_NetworkCard",
                type: "decimal(65,30)",
                nullable: false,
                comment: "钱包余额",
                oldClrType: typeof(decimal),
                oldType: "decimal(5,2)",
                oldComment: "钱包余额");

            migrationBuilder.AlterColumn<decimal>(
                name: "GiftBalance",
                table: "IOT_NetworkCard",
                type: "decimal(65,30)",
                nullable: false,
                comment: "赠送余额",
                oldClrType: typeof(decimal),
                oldType: "decimal(5,2)",
                oldComment: "赠送余额");
        }
    }
}
