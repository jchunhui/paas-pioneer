﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Paas.Pioneer.IOTNetworkCard.EntityFrameworkCore.Migrations
{
    public partial class update_networkCard_UserId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "UserId",
                table: "IOT_NetworkCard",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                comment: "用户Id",
                collation: "ascii_general_ci");

            migrationBuilder.AlterColumn<Guid>(
                name: "LimitDictionaryId",
                table: "IOT_EmptyFlowSpeedFictitious",
                type: "char(36)",
                nullable: true,
                comment: "限速字典Id",
                collation: "ascii_general_ci",
                oldClrType: typeof(Guid),
                oldType: "char(36)",
                oldComment: "限速字典Id")
                .OldAnnotation("Relational:Collation", "ascii_general_ci");

            migrationBuilder.AddColumn<bool>(
                name: "IsBindPhone",
                table: "IOT_CarrierOperator",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserId",
                table: "IOT_NetworkCard");

            migrationBuilder.DropColumn(
                name: "IsBindPhone",
                table: "IOT_CarrierOperator");

            migrationBuilder.AlterColumn<Guid>(
                name: "LimitDictionaryId",
                table: "IOT_EmptyFlowSpeedFictitious",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                comment: "限速字典Id",
                collation: "ascii_general_ci",
                oldClrType: typeof(Guid),
                oldType: "char(36)",
                oldNullable: true,
                oldComment: "限速字典Id")
                .OldAnnotation("Relational:Collation", "ascii_general_ci");
        }
    }
}
