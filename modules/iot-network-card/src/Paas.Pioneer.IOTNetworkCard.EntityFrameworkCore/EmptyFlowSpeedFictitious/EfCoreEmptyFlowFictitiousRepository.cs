﻿using Volo.Abp.EntityFrameworkCore;
using Paas.Pioneer.IOTNetworkCard.EntityFrameworkCore.EntityFrameworkCore;
using Paas.Pioneer.IOTNetworkCard.EntityFrameworkCore.BaseExtensions;
using Paas.Pioneer.IOTNetworkCard.Domain.EmptyFlowSpeedFictitious;

namespace Paas.Pioneer.IOTNetworkCard.EntityFrameworkCore.EmptyFlowSpeedFictitious
{
    public class EfCoreEmptyFlowSpeedFictitiousRepository : BaseExtensionsRepository<EmptyFlowSpeedFictitiousEntity>, IEmptyFlowSpeedFictitiousRepository
    {
        public EfCoreEmptyFlowSpeedFictitiousRepository(IDbContextProvider<IOTNetworkCardsDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
    }
}

