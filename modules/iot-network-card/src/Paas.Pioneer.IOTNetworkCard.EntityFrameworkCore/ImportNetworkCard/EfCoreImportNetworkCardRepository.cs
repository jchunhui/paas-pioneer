﻿using System;
using System.Threading.Tasks;
using Paas.Pioneer.Domain.Shared.Dto.Input;
using Paas.Pioneer.Domain.Shared.Dto.Output;
using Paas.Pioneer.Domain.Shared.Extensions;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.ImportNetworkCard.Dto.Input;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.ImportNetworkCard.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Domain.CarrierOperator;
using Volo.Abp.EntityFrameworkCore;
using Paas.Pioneer.IOTNetworkCard.Domain.ImportNetworkCard;
using Paas.Pioneer.IOTNetworkCard.EntityFrameworkCore.BaseExtensions;
using Paas.Pioneer.IOTNetworkCard.EntityFrameworkCore.EntityFrameworkCore;
using Volo.Abp.Domain.Repositories;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Paas.Pioneer.IOTNetworkCard.EntityFrameworkCore.ImportNetworkCard
{
    public class EfCoreImportNetworkCardRepository : BaseExtensionsRepository<ImportNetworkCardEntity>,
        IImportNetworkCardRepository
    {
        private readonly IRepository<CarrierOperatorEntity, Guid> _carrierRepository;

        public EfCoreImportNetworkCardRepository(IDbContextProvider<IOTNetworkCardsDbContext> dbContextProvider,
            IRepository<CarrierOperatorEntity, Guid> carrierRepository)
            : base(dbContextProvider)
        {
            _carrierRepository = carrierRepository;
        }

        public async Task<Page<GetImportNetworkCardPageListOutput>> GetPageListAsync(
            PageInput<GetImportNetworkCardPageListInput> input)
        {
            var importNetworkCardQueryable = await GetQueryableAsync();
            var carrierOperatorQueryable = await _carrierRepository.GetQueryableAsync();
            var data = from importNetworkCard in importNetworkCardQueryable.WhereDynamicFilter(input.DynamicFilter)
                join carrierOperator in carrierOperatorQueryable
                    on importNetworkCard.CarrierOperatorId equals
                    carrierOperator.Id
                select new GetImportNetworkCardPageListOutput
                {
                    ImportSumCount = importNetworkCard.ImportSumCount,
                    ImportSuccessCount = importNetworkCard.ImportSuccessCount,
                    CarrierOperatorId = importNetworkCard.CarrierOperatorId,
                    CarrierOperatorPackageDictionaryId = importNetworkCard.CarrierOperatorPackageDictionaryId,
                    NetworkCardDictionaryId = importNetworkCard.NetworkCardDictionaryId,
                    Remark = importNetworkCard.Remark,
                    CreationTime = importNetworkCard.CreationTime,
                    Id = importNetworkCard.Id,
                    CarrierOperatorName = carrierOperator.Name
                };

            var s = await data
                .OrderByDescending(x => x.CreationTime)
                .Skip((input.CurrentPage - 1) * input.PageSize)
                .Take(input.PageSize)
                .ToListAsync();

            return new Page<GetImportNetworkCardPageListOutput>
            {
                Total = await data.CountAsync(),
                List = await data
                    .OrderByDescending(x => x.CreationTime)
                    .Skip((input.CurrentPage - 1) * input.PageSize)
                    .Take(input.PageSize)
                    .ToListAsync()
            };
        }
    }
}