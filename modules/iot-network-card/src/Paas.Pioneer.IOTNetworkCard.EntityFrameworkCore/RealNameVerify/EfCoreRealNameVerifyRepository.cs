﻿using Volo.Abp.EntityFrameworkCore;
using Paas.Pioneer.IOTNetworkCard.Domain.RealNameVerify;
using Paas.Pioneer.IOTNetworkCard.EntityFrameworkCore.BaseExtensions;
using Paas.Pioneer.IOTNetworkCard.EntityFrameworkCore.EntityFrameworkCore;

namespace Paas.Pioneer.IOTNetworkCard.EntityFrameworkCore.RealNameVerify
{
    public class EfCoreRealNameVerifyRepository : BaseExtensionsRepository<RealNameVerifyEntity>, IRealNameVerifyRepository
    {
        public EfCoreRealNameVerifyRepository(IDbContextProvider<IOTNetworkCardsDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
    }
}

