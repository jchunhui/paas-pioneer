﻿using Volo.Abp.EntityFrameworkCore;
using Paas.Pioneer.IOTNetworkCard.Domain.NetworkCard;
using Paas.Pioneer.IOTNetworkCard.EntityFrameworkCore.BaseExtensions;
using Paas.Pioneer.IOTNetworkCard.EntityFrameworkCore.EntityFrameworkCore;

namespace Paas.Pioneer.IOTNetworkCard.EntityFrameworkCore.NetworkCard
{
    public class EfCoreNetworkCardRepository : BaseExtensionsRepository<NetworkCardEntity>, INetworkCardRepository
    {
        public EfCoreNetworkCardRepository(IDbContextProvider<IOTNetworkCardsDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
    }
}

