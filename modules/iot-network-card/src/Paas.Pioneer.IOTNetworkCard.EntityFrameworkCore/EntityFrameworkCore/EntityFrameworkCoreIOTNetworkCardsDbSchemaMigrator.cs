﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Paas.Pioneer.IOTNetworkCard.Domain.Data;
using Volo.Abp.DependencyInjection;

namespace Paas.Pioneer.IOTNetworkCard.EntityFrameworkCore.EntityFrameworkCore
{
    public class EntityFrameworkCoreIOTNetworkCardsDbSchemaMigrator
        : IIOTNetworkCardsDbSchemaMigrator, ITransientDependency
    {
        private readonly IServiceProvider _serviceProvider;

        public EntityFrameworkCoreIOTNetworkCardsDbSchemaMigrator(
            IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task MigrateAsync()
        {
            /* We intentionally resolving the IOTNetworkCardsDbContext
             * from IServiceProvider (instead of directly injecting it)
             * to properly get the connection string of the current tenant in the
             * current scope.
             */

            await _serviceProvider
                .GetRequiredService<IOTNetworkCardsDbContext>()
                .Database
                .MigrateAsync();
        }
    }
}
