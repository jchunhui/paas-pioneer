﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Logging;
using System;
using Paas.Pioneer.IOTNetworkCard.Domain.CarrierOperator;
using Paas.Pioneer.IOTNetworkCard.Domain.ImportNetworkCard;
using Paas.Pioneer.IOTNetworkCard.Domain.NetworkCard;
using Paas.Pioneer.IOTNetworkCard.Domain.NetworkCardMonitor;
using Paas.Pioneer.IOTNetworkCard.Domain.NetworkCardPackageRecord;
using Paas.Pioneer.IOTNetworkCard.Domain.Package;
using Paas.Pioneer.IOTNetworkCard.Domain.RealNameVerify;
using Volo.Abp.Data;
using Volo.Abp.DependencyInjection;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.TenantManagement;
using Volo.Abp.TenantManagement.EntityFrameworkCore;
using Paas.Pioneer.IOTNetworkCard.Domain.EmptyFlowSpeedFictitious;
using Volo.Abp.AuditLogging.EntityFrameworkCore;

namespace Paas.Pioneer.IOTNetworkCard.EntityFrameworkCore.EntityFrameworkCore
{
    [ReplaceDbContext(typeof(ITenantManagementDbContext))]
    [ConnectionStringName("Default")]
    public class IOTNetworkCardsDbContext :
        AbpDbContext<IOTNetworkCardsDbContext>,
        ITenantManagementDbContext
    {
        /* Add DbSet properties for your Aggregate Roots / Entities here. */

        #region Entities from the modules

        // Tenant Management
        public DbSet<Tenant> Tenants { get; set; }
        public DbSet<TenantConnectionString> TenantConnectionStrings { get; set; }

        #endregion

        public DbSet<CarrierOperatorEntity> CarrierOperatorEntitys { get; set; }

        public DbSet<ImportNetworkCardEntity> ImportNetworkCardEntitys { get; set; }

        public DbSet<NetworkCardEntity> NetworkCardEntitys { get; set; }

        public DbSet<NetworkCardMonitorEntity> NetworkCardMonitorEntitys { get; set; }

        public DbSet<NetworkCardPackageRecordEntity> NetworkCardPackageRecordEntitys { get; set; }

        public DbSet<PackageEntity> PackageEntitys { get; set; }

        public DbSet<RealNameVerifyEntity> RealNameVerifyEntitys { get; set; }

        public DbSet<EmptyFlowSpeedFictitiousEntity> EmptyFlowSpeedFictitiousEntitys { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder
                .ConfigureWarnings(b => b.Throw(RelationalEventId.MultipleCollectionIncludeWarning))
                .LogTo(Console.WriteLine, LogLevel.Information)
                .EnableSensitiveDataLogging();


        public IOTNetworkCardsDbContext(DbContextOptions<IOTNetworkCardsDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            /* Include modules to your migration db context */
            builder.ConfigureAuditLogging();
            builder.ConfigureTenantManagement();

            /* Configure your own tables/entities inside here */

            //builder.Entity<YourEntity>(b =>
            //{
            //    b.ToTable(IOTNetworkCardsConsts.DbTablePrefix + "YourEntities", IOTNetworkCardsConsts.DbSchema);
            //    b.ConfigureByConvention(); //auto configure for the base class props
            //    //...
            //});
        }
    }
}