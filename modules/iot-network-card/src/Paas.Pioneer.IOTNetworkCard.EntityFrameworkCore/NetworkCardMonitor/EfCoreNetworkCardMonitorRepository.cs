﻿using Volo.Abp.EntityFrameworkCore;
using Paas.Pioneer.IOTNetworkCard.Domain.NetworkCardMonitor;
using Paas.Pioneer.IOTNetworkCard.EntityFrameworkCore.BaseExtensions;
using Paas.Pioneer.IOTNetworkCard.EntityFrameworkCore.EntityFrameworkCore;

namespace Paas.Pioneer.IOTNetworkCard.EntityFrameworkCore.NetworkCardMonitor
{
    public class EfCoreNetworkCardMonitorRepository : BaseExtensionsRepository<NetworkCardMonitorEntity>, INetworkCardMonitorRepository
    {
        public EfCoreNetworkCardMonitorRepository(IDbContextProvider<IOTNetworkCardsDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
    }
}

