﻿using Volo.Abp.EntityFrameworkCore;
using Paas.Pioneer.IOTNetworkCard.Domain.NetworkCardPackageRecord;
using Paas.Pioneer.IOTNetworkCard.EntityFrameworkCore.BaseExtensions;
using Paas.Pioneer.IOTNetworkCard.EntityFrameworkCore.EntityFrameworkCore;

namespace Paas.Pioneer.IOTNetworkCard.EntityFrameworkCore.NetworkCardPackageRecord
{
    public class EfCoreNetworkCardPackageRecordRepository : BaseExtensionsRepository<NetworkCardPackageRecordEntity>, INetworkCardPackageRecordRepository
    {
        public EfCoreNetworkCardPackageRecordRepository(IDbContextProvider<IOTNetworkCardsDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
    }
}

