﻿using Paas.Pioneer.IOTNetworkCard.RequestFactory.Context;
using Paas.Pioneer.IOTNetworkCard.RequestFactory.Context.Enum;
using Paas.Pioneer.IOTNetworkCard.RequestFactory.Dto.Input;
using Paas.Pioneer.IOTNetworkCard.RequestFactory.Dto.Output;
using Volo.Abp.DependencyInjection;

namespace Paas.Pioneer.IOTNetworkCard.RequestFactory.Factory
{
    internal class MoveRequestService : IRequestFactory, ISingletonDependency
    {
        public EContextRequestType Type => EContextRequestType.Move;

        public async Task<DisabledNumberOutDto?> DisabledNumberAsync(DisabledNumberDto disabledNumberDto)
        {
            throw new NotImplementedException();
        }

        public Task<IMEIReRecOrdDtoOutDto> IMEIReRecOrdAsync(IMEIReRecOrdDto iMEIReRecOrdDto)
        {
            throw new NotImplementedException();
        }

        public Task<QueryTrafficByDateOutDto> QueryTrafficByDateAsync(QueryTrafficByDateDto queryTrafficByDateDto)
        {
            throw new NotImplementedException();
        }

        public async Task<SvcContQueryIotRealNameDto?> QueryIotRealNameAsync(QueryIotRealNameInput input)
        {
            throw new NotImplementedException();
        }

        public Task<GetTelephoneOutDto> GetTelephoneAsync(GetTelephoneDto getTelephoneDto)
        {
            throw new NotImplementedException();
        }

        public Task<ProdInstQueryOutDto> ProdInstQueryAsync(ProdInstQueryDto prodInstQueryDto)
        {
            throw new NotImplementedException();
        }

        public Task<QueryOweOutDto> QueryOweAsync(QueryOweDto queryOweDto)
        {
            throw new NotImplementedException();
        }

        public Task<GetSIMListOutDto> GetSIMListAsync(GetSIMListDto getSIMListDto)
        {
            throw new NotImplementedException();
        }

        public Task<QueryTrafficOutDto> QueryTrafficAsync(QueryTrafficDto queryTrafficInput)
        {
            throw new NotImplementedException();
        }
    }
}
