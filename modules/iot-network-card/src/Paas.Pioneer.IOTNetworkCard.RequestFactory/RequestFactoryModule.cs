﻿using Microsoft.Extensions.DependencyInjection;
using Paas.Pioneer.IOTNetworkCard.Domain.Shared;
using Paas.Pioneer.IOTNetworkCard.RequestFactory.Context;
using Paas.Pioneer.IOTNetworkCard.RequestFactory.Factory;
using Volo.Abp.Modularity;

namespace Paas.Pioneer.IOTNetworkCard.RequestFactory
{
    [DependsOn(typeof(IOTNetworkCardsDomainSharedModule))]
    public class RequestFactoryModule : AbpModule
    {
        public override void PreConfigureServices(ServiceConfigurationContext context)
        {
        }

        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddSingleton<IRequestFactory, TelecomRequestService>();
            context.Services.AddSingleton<IRequestFactory, MoveRequestService>();
        }
    }
}