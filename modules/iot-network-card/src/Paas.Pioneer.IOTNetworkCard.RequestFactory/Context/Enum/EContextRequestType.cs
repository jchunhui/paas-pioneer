﻿using System.ComponentModel;

namespace Paas.Pioneer.IOTNetworkCard.RequestFactory.Context.Enum
{
    /// <summary>
    /// 运行商-1.移动运营商 2.电信运营商
    /// </summary>
    public enum EContextRequestType
    {
        /// <summary>
        /// 移动运营商
        /// </summary>
        [Description("移动运营商")]
        Move = 1,

        /// <summary>
        /// 电信运营商
        /// </summary>
        [Description("电信运营商")]
        Telecom = 2
    }
}
