﻿using Paas.Pioneer.IOTNetworkCard.RequestFactory.Context.Enum;
using Volo.Abp;
using Volo.Abp.DependencyInjection;

namespace Paas.Pioneer.IOTNetworkCard.RequestFactory.Context
{
    /// <summary>
    /// 请求工厂上下文
    /// </summary>
    public class ContextRequestFactory : ISingletonDependency
    {
        private readonly IEnumerable<IRequestFactory> _requestFactorieList;
        public ContextRequestFactory(IEnumerable<IRequestFactory> requestFactorieList)
        {
            _requestFactorieList = requestFactorieList;
        }

        public IRequestFactory GetRequestFactory(EContextRequestType type)
        {
            if (!_requestFactorieList.Any(x => x.Type == type))
            {
                throw new BusinessException("协议不存在");
            }
            return _requestFactorieList.FirstOrDefault(x => x.Type == type);
        }
 
    }
}
