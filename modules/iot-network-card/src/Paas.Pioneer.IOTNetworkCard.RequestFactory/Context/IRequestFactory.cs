﻿using Paas.Pioneer.IOTNetworkCard.RequestFactory.Context.Enum;
using Paas.Pioneer.IOTNetworkCard.RequestFactory.Dto.Input;
using Paas.Pioneer.IOTNetworkCard.RequestFactory.Dto.Output;

namespace Paas.Pioneer.IOTNetworkCard.RequestFactory.Context
{
    /// <summary>
    /// 基类接口
    /// </summary>
    public interface IRequestFactory
    {
        public EContextRequestType Type { get; }

        /// <summary>
        /// 停机保号/复机/测试期去激活
        /// </summary>
        /// <returns></returns>
        Task<DisabledNumberOutDto?> DisabledNumberAsync(DisabledNumberDto disabledNumberDto);

        /// <summary>
        /// 机卡重绑
        /// </summary>
        /// <returns></returns>
        Task<IMEIReRecOrdDtoOutDto> IMEIReRecOrdAsync(IMEIReRecOrdDto iMEIReRecOrdDto);

        /// <summary>
        /// 流量总使用量查询(时间段)
        /// </summary>
        /// <returns></returns>
        Task<QueryTrafficByDateOutDto> QueryTrafficByDateAsync(QueryTrafficByDateDto queryTrafficByDateDto);

        /// <summary>
        /// 获取用户实名信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<SvcContQueryIotRealNameDto?> QueryIotRealNameAsync(QueryIotRealNameInput input);

        /// <summary>
        /// 接入号码查询接口
        /// </summary>
        /// <returns></returns>
        Task<GetTelephoneOutDto> GetTelephoneAsync(GetTelephoneDto getTelephoneDto);

        /// <summary>
        /// 产品资料查询接口
        /// </summary>
        /// <returns></returns>
        Task<ProdInstQueryOutDto> ProdInstQueryAsync(ProdInstQueryDto prodInstQueryDto);

        /// <summary>
        /// 欠费查询接口
        /// </summary>
        /// <returns></returns>
        Task<QueryOweOutDto> QueryOweAsync(QueryOweDto queryOweDto);

        /// <summary>
        /// SIM卡列表查询接口
        /// </summary>
        /// <returns></returns>
        Task<GetSIMListOutDto> GetSIMListAsync(GetSIMListDto getSIMListDto);

        /// <summary>
        /// 流量总使用量查询(当月)
        /// </summary>
        /// <returns></returns>
        Task<QueryTrafficOutDto> QueryTrafficAsync(QueryTrafficDto queryTrafficInput);
    }
}