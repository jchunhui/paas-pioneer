﻿namespace Paas.Pioneer.IOTNetworkCard.RequestFactory.Dto.Input;

public class BaseRequestInput
{
    /// <summary>
    /// 运营商账号
    /// </summary>
    public string Account { get; set; }

    /// <summary>
    /// 运营商账号密码
    /// </summary>
    public string Password { protected get; set; }

    /// <summary>
    /// 密钥
    /// </summary>
    public string SecretKey { protected get; set; }

    /// <summary>
    /// user_id对应的密码经过DES算法加密后的密文
    /// </summary>
    public string DesPassword
    {
        get
        {
            string key1 = SecretKey.Substring(0, 3);
            string key2 = SecretKey.Substring(3, 3);
            string key3 = SecretKey.Substring(6, 3);
            return DesUtils.strEnc(this.Password, key1, key2, key3); //密码加密
        }
    }
}