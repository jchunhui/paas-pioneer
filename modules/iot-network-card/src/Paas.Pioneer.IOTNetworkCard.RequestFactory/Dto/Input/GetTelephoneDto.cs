﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paas.Pioneer.IOTNetworkCard.RequestFactory.Dto.Input
{
    public class GetTelephoneDto : BaseRequestInput
    {
        /// <summary>
        /// 请求方法
        /// </summary>
        public string Method => "getTelephone";

        /// <summary>
        /// 接入号码
        /// 物联网接入号(149或10649号段
        /// </summary>
        public string AccessNumber { get; set; }

        /// <summary>
        /// 身份证号码
        /// </summary>
        public string IDCard { get; set; }

        /// <summary>
        /// ICCID
        /// </summary>
        public string ICCID { get; set; }

        /// <summary>
        /// sign参数为接入号码、身份证号码、用户名、密码、method经过自然排序后拼接成的以逗号分隔的字符串，再通过DES加密算法加密之后所得结果。
        /// </summary>
        public string Sign
        {
            get
            {
                string[] arr = { AccessNumber, IDCard, Account, Password, Method };
                string key1 = SecretKey.Substring(0, 3);
                string key2 = SecretKey.Substring(3, 3);
                string key3 = SecretKey.Substring(6, 3);
                return DesUtils.strEnc(DesUtils.naturalOrdering(arr), key1, key2, key3);
            }
        }
    }
}
