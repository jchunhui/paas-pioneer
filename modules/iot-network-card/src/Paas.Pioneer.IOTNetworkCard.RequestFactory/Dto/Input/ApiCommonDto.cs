﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paas.Pioneer.IOTNetworkCard.RequestFactory.Dto.Input
{
    public class ApiCommonDto
    {
        /// <summary>
        /// 接口标识
        /// </summary>
        public string method { get; set; }

        /// <summary>
        /// 账号
        /// </summary>
        public string user_id { get; set; }

        /// <summary>
        /// 接入号码
        /// </summary>
        public string access_number { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string passWord { get; set; }

        /// <summary>
        /// 签名
        /// </summary>
        public string sign { get; set; }
    }
}
