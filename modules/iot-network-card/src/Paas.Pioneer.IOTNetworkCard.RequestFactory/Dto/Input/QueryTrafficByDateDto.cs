﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paas.Pioneer.IOTNetworkCard.RequestFactory.Dto.Input
{
    public class QueryTrafficByDateDto : BaseRequestInput
    {
        /// <summary>
        /// 请求方法
        /// </summary>
        public string Method => "queryTrafficByDate";

        /// <summary>
        /// 接入号码
        /// 物联网接入号(149或10649号段
        /// </summary>
        public string AccessNumber { get; set; }

        /// <summary>
        /// 开始时间(例如：20161001)
        /// </summary>
        public string StartDate { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        public string EndDate { get; set; }

        /// <summary>
        /// 是否需要明细0：只返回总使用量，1：返回明细。建议使用0，1后续会被限制调用
        /// </summary>
        public string NeedDtl { get; set; }

        /// <summary>
        /// sign参数为接入号码、用户名、密码、method经过自然排序后拼接成的以逗号分隔的字符串，再通过DES加密算法加密之后所得结果。
        /// </summary>
        public string Sign
        {
            get
            {
                string[] arr = { AccessNumber, Account, Password, Method };
                string key1 = SecretKey.Substring(0, 3);
                string key2 = SecretKey.Substring(3, 3);
                string key3 = SecretKey.Substring(6, 3);
                return DesUtils.strEnc(DesUtils.naturalOrdering(arr), key1, key2, key3);
            }
        }
    }
}
