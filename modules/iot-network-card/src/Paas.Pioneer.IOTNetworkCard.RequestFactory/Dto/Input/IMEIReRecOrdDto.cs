﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paas.Pioneer.IOTNetworkCard.RequestFactory.Dto.Input
{
    public class IMEIReRecOrdDto : BaseRequestInput
    {
        /// <summary>
        /// 请求方法
        /// </summary>
        public string Method => "IMEIReRecord";

        /// <summary>
        /// 接入号码
        /// 物联网接入号(149或10649号段
        /// </summary>
        public string AccessNumber { get; set; }

        /// <summary>
        /// 动作类型
        /// </summary>
        public string Action { get; set; } = "ADD";

        /// <summary>
        /// 重绑类型
        /// </summary>
        public string Bind_Type { get; set; }

        /// <summary>
        /// 设备号
        /// </summary>
        public string Imei { get; set; }

        /// <summary>
        /// sign参数为接入号码或iccid、用户名、action、密码、method经过自然排序后拼接成的以逗号分隔的字符串，再通过DES加密算法加密之后所得结果。
        /// </summary>
        public string Sign
        {
            get
            {
                string[] arr = { AccessNumber, Account, Action, Password, Method };
                string key1 = SecretKey.Substring(0, 3);
                string key2 = SecretKey.Substring(3, 3);
                string key3 = SecretKey.Substring(6, 3);
                return DesUtils.strEnc(DesUtils.naturalOrdering(arr), key1, key2, key3);
            }
        }
    }
}
