﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paas.Pioneer.IOTNetworkCard.RequestFactory.Dto.Output
{
    public class GetTelephoneOutDto
    {
        /// <summary>
        /// 状态响应码--返回0，表示查询成功；返回-1，表示没有权限（用户名密码错误或iccid/imsi不属于该用户）；返回-2，表示参数错误（iccid或imsi号码错误）；返回 -3，表示sign值错误（加密方式错误）
        /// </summary>
        public string RESULT { get; set; }

        /// <summary>
        /// 查询结果
        /// </summary>
        public string SMSG { get; set; }
    }
}
