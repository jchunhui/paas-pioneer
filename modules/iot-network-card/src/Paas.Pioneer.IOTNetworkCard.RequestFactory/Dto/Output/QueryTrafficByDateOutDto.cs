﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paas.Pioneer.IOTNetworkCard.RequestFactory.Dto.Output
{
    public class QueryTrafficByDateResonOutDto
    {
        /// <summary>
        /// 根节点
        /// </summary>
        public QueryTrafficByDateOutDto root { get; set; }


    }

    public class QueryTrafficByDateOutDto
    {
        /// <summary>
        /// 移动用户上网及数据详单查询
        /// </summary>
        public string NEW_DATA_TICKET_QRsp { get; set; }

        /// <summary>
        /// 查询号码
        /// </summary>
        public string number { get; set; }

        /// <summary>
        /// 流量合计
        /// </summary>
        public string TOTAL_BYTES_CNT { get; set; }

        /// <summary>
        /// 流量使用记录
        /// </summary>
        public string TOTALCOUNT { get; set; }

        /// <summary>
        /// 流量分页
        /// </summary>
        public string TOTALPAGE { get; set; }

        /// <summary>
        /// 流量使用节点
        /// </summary>
        public string NEW_DATA_TICKET_Qrlist { get; set; }

        /// <summary>
        /// 使用时长合计
        /// </summary>
        public string DURATION_CNT_CH { get; set; }

        /// <summary>
        /// 话费合计
        /// </summary>
        public string CHARGE_CNT_CH { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public string TICKET_NUMBER { get; set; }

        /// <summary>
        /// 通信地点
        /// </summary>
        public string TICKET_TYPE { get; set; }

        /// <summary>
        /// 承载网络
        /// </summary>
        public string SERVICE_TYPE { get; set; }

        /// <summary>
        /// 上线时间
        /// </summary>
        public string START_TIME { get; set; }

        /// <summary>
        /// 使用时长
        /// </summary>
        public string DURATION_CH { get; set; }

        /// <summary>
        /// 话费
        /// </summary>
        public string TICKET_CHARGE_CH { get; set; }

        /// <summary>
        /// 流量
        /// </summary>
        public string BYTES_CNT { get; set; }

        /// <summary>
        /// APN名称
        /// </summary>
        public string APNNAME { get; set; }

        /// <summary>
        /// RG标识
        /// </summary>
        public string PID { get; set; }

        /// <summary>
        /// 处理结果代码
        /// </summary>
        public string IRESULT { get; set; }

        /// <summary>
        /// 流水号
        /// </summary>
        public string GROUP_TRANSACTIONID { get; set; }
    }
}
