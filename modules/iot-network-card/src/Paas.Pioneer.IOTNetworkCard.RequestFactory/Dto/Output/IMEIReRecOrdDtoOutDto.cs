﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paas.Pioneer.IOTNetworkCard.RequestFactory.Dto.Output
{
    public class IMEIReRecOrdDtoOutDto
    {
        /// <summary>
        /// 返回0，表示查询成功
        /// </summary>
        public string result { get; set; }

        /// <summary>
        /// 例如：成功接收消息
        /// </summary>
        public string resultMsg { get; set; }

        /// <summary>
        /// 流水号
        /// </summary>
        public string GROUP_TRANSACTIONID { get; set; }
    }


}
