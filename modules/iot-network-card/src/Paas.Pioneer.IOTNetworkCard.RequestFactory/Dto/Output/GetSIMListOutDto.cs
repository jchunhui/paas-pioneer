﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paas.Pioneer.IOTNetworkCard.RequestFactory.Dto.Output
{
    public class GetSIMListOutDto
    {
        public string groupTransactionId { get; set; }

        public string resultCode { get; set; }

        public string resultMsg { get; set; }

        public SUMLDescription description { get; set; }

    }

    public class SUMLDescription
    {
        public string pageIndex { get; set; }

        public List<SimListModel> simList { get; set; }
    }

    public class SimListModel
    {
        public string accNumber { get; set; }
        public string iccid { get; set; }
        public string activationTime { get; set; }
        public List<string> simStatus { get; set; }

    }
}
