﻿namespace Paas.Pioneer.IOTNetworkCard.RequestFactory.Dto.Output;

public class SvcContQueryIotRealNameDto
{
    /// <summary>
    /// SvcCont
    /// </summary>
    public ResultQueryIotRealNameDto SvcCont { get; set; }
}

public class ResultQueryIotRealNameDto
{
    /// <summary>
    /// RESULT
    /// </summary>
    public QueryIotRealNameDto RESULT { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public string ResultCode { get; set; }

    /// <summary>
    /// 处理成功！
    /// </summary>
    public string ResultMsg { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public string GROUP_TRANSACTIONID { get; set; }
}

public class QueryIotRealNameDto
{
    /// <summary>
    /// 
    /// </summary>
    public string ActiveTime { get; set; }

    /// <summary>
    /// 在用
    /// </summary>
    public string ProdStatusName { get; set; }

    /// <summary>
    /// 在用
    /// </summary>
    public string ProdMainStatusName { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public string CertNumber { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public string Number { get; set; }
}