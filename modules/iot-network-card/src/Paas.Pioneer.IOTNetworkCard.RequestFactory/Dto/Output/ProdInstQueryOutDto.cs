﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paas.Pioneer.IOTNetworkCard.RequestFactory.Dto.Output
{
    public class ProdInstQueryOutDto
    {
        /// <summary>
        /// 业务内容 
        /// </summary>
        public SvcContModel SvcCont { get; set; }

    }
    public class SvcContModel
    {
        /// <summary>
        /// 结果字段 
        /// </summary>
        public ResultModel result { get; set; }
        /// <summary>
        ///  返回状态标识
        /// </summary>
        public string resultCode { get; set; }
        /// <summary>
        ///  返回标识说明
        /// </summary>
        public string resultMsg { get; set; }

        /// <summary>
        /// 流水号 
        /// </summary>
        public string GROUP_TRANSACTIONID { get; set; }
    }

    public class ResultModel
    {
        /// <summary>
        /// 业务内容 
        /// </summary>
        public ProdInfosModel prodInfos { get; set; }

        public string endDt { get; set; }

        public string prodOfferNbr { get; set; }

        public string prodOfferName { get; set; }

        public string startDt { get; set; }

        public string statusName { get; set; }

        public string prodStatusName { get; set; }

        public string productName { get; set; }

        public string stopflag { get; set; }
    }

    public class ProdInfosModel
    {
        public List<AttrInfosModel> attrInfos { get; set; }

        public string commonRegionName { get; set; }

        public string custName { get; set; }

        public List<FunProdInfosModel> funProdInfos { get; set; }

        public string phoneNum { get; set; }
    }

    public class AttrInfosModel
    {
        public string attrName { get; set; }

        public string attrValue { get; set; }
    }

    public class FunProdInfosModel
    {
        public string productName { get; set; }
    }
}
