﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paas.Pioneer.IOTNetworkCard.RequestFactory.Dto.Output
{
    public class DisabledNumberOutResponseDto
    {
        /// <summary>
        /// 业务根节点
        /// </summary>
        public DisabledNumberOutDto BusinessServiceResponse { get; set; }
    }

    public class DisabledNumberOutDto
    {
        /// <summary>
        /// 请求状态-返回0，表示请求成功
        /// </summary>
        public string RspType { get; set; }

        /// <summary>
        /// 返回0，表示成功接收消息；
        /// </summary>
        public string Result { get; set; }

        /// <summary>
        /// 返回消息信息
        /// </summary>
        public string ResultMsg { get; set; }

        /// <summary>
        /// 流水号
        /// </summary>
        public string GROUP_TRANSACTIONID { get; set; }
    }
}