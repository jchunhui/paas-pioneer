﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paas.Pioneer.IOTNetworkCard.RequestFactory.Dto.Output
{
    public class QueryOweOutDto
    {
        /// <summary>
        /// 业务内容 
        /// </summary>
        public QueryOweSvcContModel SvcCont { get; set; }
    }
    public class QueryOweSvcContModel
    {
        /// <summary>
        ///  
        /// </summary>
        public string Acc_Nbr { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public List<BillQueryInformationModel> BillQueryInformation { get; set; }

        /// <summary>
        ///   
        /// </summary>
        public string Service_Result_Code { get; set; }

        /// <summary>
        /// 流水号 
        /// </summary>
        public string GROUP_TRANSACTIONID { get; set; }

        /// <summary>
        ///   
        /// </summary>
        public string number { get; set; }
    }

    public class BillQueryInformationModel
    {
        public string Acc_Nbr { get; set; }

        public string Acct_Item_Charge { get; set; }

        public string Acct_Item_Type_Name { get; set; }

        public string Acct_Name { get; set; }

        public string Billing_Cycle_ID { get; set; }

        public string Query_Flag { get; set; }

    }
}
