﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Paas.Pioneer.Domain;

namespace Paas.Pioneer.IOTNetworkCard.Domain.RealNameVerify
{
    /// <summary>
    /// 运营商实名验证管理
    /// </summary>
    [Comment("运营商实名验证管理")]
    [Table("IOT_RealNameVerify")]
    [Index(nameof(CarrierOperatorId), Name = "IDX_CarrierOperatorId")]
    public class RealNameVerifyEntity : BaseEntityNotTenantCore
    {
        /// <summary>
        /// 运营商Id
        /// </summary>
        [Comment("运营商Id")]
        [Column("CarrierOperatorId", TypeName = "char(36)")]
        public Guid CarrierOperatorId { get; set; }


        /// <summary>
        /// 验证方式字典Id
        /// </summary>
        [Comment("验证方式字典Id")]
        [Column("VerifyDictionaryId", TypeName = "char(36)")]
        public Guid VerifyDictionaryId { get; set; }


        /// <summary>
        /// 官方实名跳转复制号码字典Id
        /// </summary>
        [Comment("官方实名跳转复制号码字典Id")]
        [Column("VerifyPhoneDictionaryId", TypeName = "char(36)")]
        public Guid VerifyPhoneDictionaryId { get; set; }


        /// <summary>
        /// 验证地址
        /// </summary>
        [Comment("验证地址")]
        [Column("VerifyUrl", TypeName = "varchar(225)")]
        public string VerifyUrl { get; set; }


        /// <summary>
        /// 验证提示
        /// </summary>
        [Comment("验证提示")]
        [Column("VerifyTip", TypeName = "varchar(500)")]
        public string VerifyTip { get; set; }
    }
}