﻿using System;
using Paas.Pioneer.IOTNetworkCard.Domain.BaseExtensions;
using Volo.Abp.Domain.Repositories;

namespace Paas.Pioneer.IOTNetworkCard.Domain.RealNameVerify
{
    public interface IRealNameVerifyRepository : IRepository<RealNameVerifyEntity, Guid>, IBaseExtensionRepository<RealNameVerifyEntity>
    {

    }
}
