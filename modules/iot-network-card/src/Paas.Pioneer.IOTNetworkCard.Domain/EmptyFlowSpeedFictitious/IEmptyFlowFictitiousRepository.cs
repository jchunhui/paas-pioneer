﻿using Paas.Pioneer.IOTNetworkCard.Domain.BaseExtensions;
using System;
using Volo.Abp.Domain.Repositories;

namespace Paas.Pioneer.IOTNetworkCard.Domain.EmptyFlowSpeedFictitious
{
    public interface IEmptyFlowSpeedFictitiousRepository : IRepository<EmptyFlowSpeedFictitiousEntity, Guid>, IBaseExtensionRepository<EmptyFlowSpeedFictitiousEntity>
    {

    }
}
