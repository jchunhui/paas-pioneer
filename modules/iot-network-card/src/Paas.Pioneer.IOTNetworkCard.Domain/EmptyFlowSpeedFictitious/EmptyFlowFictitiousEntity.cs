﻿using Microsoft.EntityFrameworkCore;
using Paas.Pioneer.Domain;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Paas.Pioneer.IOTNetworkCard.Domain.EmptyFlowSpeedFictitious
{
    /// <summary>
    /// 虚量限速配置表
    /// </summary>
    [Comment("虚量限速配置表")]
    [Table("IOT_EmptyFlowSpeedFictitious")]
    [Index(nameof(PackageId), Name = "IDX_PackageId")]
    public class EmptyFlowSpeedFictitiousEntity : BaseNotTenantEntity
    {
        /// <summary>
        /// 套餐Id
        /// </summary>
        [Comment("套餐Id")]
        [Column("PackageId", TypeName = "char(36)")]
        public Guid PackageId { get; set; }

        /// <summary>
        /// 虚拟Json
        /// </summary>
        [Comment("虚拟Json")]
        [Column("EmptyFlowFictitiousJson", TypeName = "varchar(4000)")]
        public string EmptyFlowFictitiousJson { get; set; }

        /// <summary>
        /// 限速字典Id
        /// </summary>
        [Comment("限速字典Id")]
        [Column("LimitDictionaryId", TypeName = "char(36)")]
        public Guid? LimitDictionaryId { get; set; }

        /// <summary>
        /// 限速Json
        /// </summary>
        [Comment("限速Json")]
        [Column("SpeedFictitiousJson", TypeName = "varchar(4000)")]
        public string SpeedFictitiousJson { get; set; }
    }
}