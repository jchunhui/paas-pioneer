﻿using System;
using Paas.Pioneer.IOTNetworkCard.Domain.BaseExtensions;
using Volo.Abp.Domain.Repositories;

namespace Paas.Pioneer.IOTNetworkCard.Domain.NetworkCardMonitor
{
    public interface INetworkCardMonitorRepository : IRepository<NetworkCardMonitorEntity, Guid>, IBaseExtensionRepository<NetworkCardMonitorEntity>
    {

    }
}
