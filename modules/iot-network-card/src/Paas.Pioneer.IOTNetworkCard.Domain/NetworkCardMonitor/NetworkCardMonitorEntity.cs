﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Paas.Pioneer.Domain;

namespace Paas.Pioneer.IOTNetworkCard.Domain.NetworkCardMonitor
{
    /// <summary>
    /// 网卡监控
    /// </summary>
    [Comment("网卡监控")]
    [Table("IOT_NetworkCardMonitor")]
    [Index(nameof(NetworkCardId), Name = "IDX_NetworkCardId")]
    public class NetworkCardMonitorEntity : BaseNotTenantEntity
    {
        /// <summary>
        /// 网卡Id
        /// </summary>
        [Comment("网卡Id")]
        [Column("NetworkCardId", TypeName = "char(36)")]
        public Guid NetworkCardId { get; set; }

        /// <summary>
        /// 使用流量
        /// </summary>
        [Comment("使用流量")]
        [Column("UseFlow", TypeName = "int")]
        public int UseFlow { get; set; }
    }
}