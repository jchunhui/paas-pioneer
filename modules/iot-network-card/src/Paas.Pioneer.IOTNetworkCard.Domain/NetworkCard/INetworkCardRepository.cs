﻿using System;
using Paas.Pioneer.IOTNetworkCard.Domain.BaseExtensions;
using Volo.Abp.Domain.Repositories;

namespace Paas.Pioneer.IOTNetworkCard.Domain.NetworkCard
{
    public interface INetworkCardRepository : IRepository<NetworkCardEntity, Guid>, IBaseExtensionRepository<NetworkCardEntity>
    {

    }
}
