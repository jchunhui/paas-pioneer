﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Paas.Pioneer.Domain;

namespace Paas.Pioneer.IOTNetworkCard.Domain.NetworkCard
{
    /// <summary>
    /// 网卡管理
    /// </summary>
    [Comment("网卡管理")]
    [Table("IOT_NetworkCard")]
    [Index(nameof(CarrierOperatorId), Name = "IDX_CarrierOperatorId")]
    [Index(nameof(ICCID), Name = "IDX_ICCIDId")]
    public class NetworkCardEntity : BaseNotTenantEntity
    {
        /// <summary>
        /// ICCID
        /// </summary>
        [Comment("ICCID")]
        [Column("ICCID", TypeName = "varchar(150)")]
        public string ICCID { get; set; }

        /// <summary>
        /// 用户Id
        /// </summary>
        [Comment("用户Id")]
        [Column("UserId", TypeName = "char(36)")]
        public Guid UserId { get; set; }

        /// <summary>
        /// 接入号码
        /// </summary>
        [Comment("接入号码")]
        [Column("AccessPhone", TypeName = "varchar(150)")]
        public string AccessPhone { get; set; }

        /// <summary>
        /// 虚拟号
        /// </summary>
        [Comment("虚拟号")]
        [Column("EmptyPhone", TypeName = "varchar(100)")]
        public string EmptyPhone { get; set; }

        /// <summary>
        /// IMEI
        /// </summary>
        [Comment("IMEI")]
        [Column("IMEI", TypeName = "varchar(100)")]
        public string IMEI { get; set; }

        /// <summary>
        /// 钱包余额
        /// </summary>
        [Comment("钱包余额")]
        [Column("WalletBalance", TypeName = "decimal(5, 2)")]
        public decimal WalletBalance { get; set; }

        /// <summary>
        /// 赠送余额
        /// </summary>
        [Comment("赠送余额")]
        [Column("GiftBalance", TypeName = "decimal(5, 2)")]
        public decimal GiftBalance { get; set; }

        /// <summary>
        /// 运营商Id
        /// </summary>
        [Comment("运营商Id")]
        [Column("CarrierOperatorId", TypeName = "char(36)")]
        public Guid CarrierOperatorId { get; set; }

        /// <summary>
        /// 运营商套餐系列
        /// </summary>
        [Comment("运营商套餐系列")]
        [Column("CarrierOperatorPackageDictionaryId", TypeName = "char(36)")]
        public Guid CarrierOperatorPackageDictionaryId { get; set; }

        /// <summary>
        /// 仅接口充值
        /// </summary>
        public bool IsApiRecharge { get; set; }

        /// <summary>
        /// 轮询网卡
        /// </summary>
        public bool IsPollingCard { get; set; }

        /// <summary>
        /// 导入网卡记录id
        /// </summary>
        [Comment("导入网卡记录id")]
        [Column("ImportCardId", TypeName = "char(36)")]
        public Guid ImportCardId { get; set; }

        /// <summary>
        /// 卡状态
        /// </summary>
        [Comment("卡状态")]
        [Column("Status", TypeName = "int")]
        public int Status { get; set; }

        /// <summary>
        /// 是否实名
        /// </summary>
        public bool IsRealName { get; set; }

        /// <summary>
        /// 是否窜卡
        /// </summary>
        public bool IsChanneling { get; set; }

        /// <summary>
        /// 网卡类型
        /// </summary>
        [Comment("网卡类型")]
        [Column("NetworkCardDictionaryId", TypeName = "char(36)")]
        public Guid NetworkCardDictionaryId { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [Comment("备注")]
        [Column("Remark", TypeName = "varchar(225)")]
        public string Remark { get; set; }
    }
}