﻿using System;
using Paas.Pioneer.IOTNetworkCard.Domain.BaseExtensions;
using Volo.Abp.Domain.Repositories;

namespace Paas.Pioneer.IOTNetworkCard.Domain.Package
{
    public interface IPackageRepository : IRepository<PackageEntity, Guid>, IBaseExtensionRepository<PackageEntity>
    {

    }
}
