﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Paas.Pioneer.Domain;

namespace Paas.Pioneer.IOTNetworkCard.Domain.Package
{
    /// <summary>
    /// 套餐管理
    /// </summary>
    [Comment("套餐管理")]
    [Table("IOT_Package")]
    [Index(nameof(CarrierOperatorId), Name = "IDX_CarrierOperatorId")]
    public class PackageEntity : BaseNotTenantEntity
    {
        /// <summary>
        /// 运营商Id
        /// </summary>
        [Comment("运营商Id")]
        [Column("CarrierOperatorId", TypeName = "char(36)")]
        public Guid CarrierOperatorId { get; set; }

        /// <summary>
        /// 运营商套餐系列
        /// </summary>
        [Comment("运营商套餐系列")]
        [Column("CarrierOperatorPackageDictionaryId", TypeName = "char(36)")]
        public Guid CarrierOperatorPackageDictionaryId { get; set; }

        /// <summary>
        /// 充值类型
        /// </summary>
        [Comment("充值类型")]
        [Column("RechargeDictionaryId", TypeName = "char(36)")]
        public Guid RechargeDictionaryId { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Comment("名称")]
        [Column("Name", TypeName = "varchar(50)")]
        public string Name { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        [Comment("排序")]
        [Column("Sort", TypeName = "int")]
        public int Sort { get; set; }

        /// <summary>
        /// 套餐类型
        /// </summary>
        [Comment("套餐类型")]
        [Column("DictionaryId", TypeName = "char(36)")]
        public Guid DictionaryId { get; set; }

        /// <summary>
        /// 显示
        /// </summary>
        public bool IsShow { get; set; }

        /// <summary>
        /// 每月最大充值数
        /// </summary>
        [Comment("每月最大充值数")]
        [Column("MaxRechargeCount", TypeName = "int")]
        public int MaxRechargeCount { get; set; }

        /// <summary>
        /// 套餐数量
        /// </summary>
        [Comment("套餐数量")]
        [Column("Count", TypeName = "int")]
        public int Count { get; set; }

        /// <summary>
        /// 成本价
        /// </summary>
        [Comment("成本价")]
        [Column("CostPrice", TypeName = "decimal(5, 2)")]
        public decimal CostPrice { get; set; }

        /// <summary>
        /// 套餐金额
        /// </summary>
        [Comment("套餐金额")]
        [Column("PackageAmount", TypeName = "decimal(5, 2)")]
        public decimal PackageAmount { get; set; }

        /// <summary>
        /// 虚量（MB）
        /// </summary>
        [Comment("虚量（MB）")]
        [Column("EmptyFlow", TypeName = "int")]
        public int EmptyFlow { get; set; }

        /// <summary>
        /// 真实流量（MB）
        /// </summary>
        [Comment("真实流量（MB）")]
        [Column("RealFlow", TypeName = "int")]
        public int RealFlow { get; set; }

        /// <summary>
        /// 禁用
        /// </summary>
        public bool IsDisable { get; set; }

        /// <summary>
        /// 热卖
        /// </summary>
        public bool IsHeat { get; set; }

        /// <summary>
        /// 是否开户套餐
        /// </summary>
        public bool IsOpenAccount { get; set; }

        /// <summary>
        /// 开启时间
        /// </summary>
        public DateTime? OpenDateTime { get; set; }

        /// <summary>
        /// 关闭时间
        /// </summary>
        public DateTime? CloseDateTime { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [Comment("备注")]
        [Column("Remark", TypeName = "varchar(225)")]
        public string Remark { get; set; }
    }
}