﻿using Volo.Abp.Settings;

namespace Paas.Pioneer.IOTNetworkCard.Domain.Settings
{
    public class IOTNetworkCardsSettingDefinitionProvider : SettingDefinitionProvider
    {
        public override void Define(ISettingDefinitionContext context)
        {
            //Define your own settings here. Example:
            //context.Add(new SettingDefinition(IOTNetworkCardsSettings.MySetting1));
        }
    }
}
