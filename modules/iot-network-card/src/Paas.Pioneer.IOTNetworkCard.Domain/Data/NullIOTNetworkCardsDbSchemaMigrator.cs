﻿using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;

namespace Paas.Pioneer.IOTNetworkCard.Domain.Data
{
    /* This is used if database provider does't define
     * IIOTNetworkCardsDbSchemaMigrator implementation.
     */
    public class NullIOTNetworkCardsDbSchemaMigrator : IIOTNetworkCardsDbSchemaMigrator, ITransientDependency
    {
        public Task MigrateAsync()
        {
            return Task.CompletedTask;
        }
    }
}