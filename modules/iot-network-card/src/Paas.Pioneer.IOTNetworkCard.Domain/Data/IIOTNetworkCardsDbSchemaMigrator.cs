﻿using System.Threading.Tasks;

namespace Paas.Pioneer.IOTNetworkCard.Domain.Data
{
    public interface IIOTNetworkCardsDbSchemaMigrator
    {
        Task MigrateAsync();
    }
}
