﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Paas.Pioneer.Domain;

namespace Paas.Pioneer.IOTNetworkCard.Domain.ImportNetworkCard
{
    /// <summary>
    /// 导入网卡记录
    /// </summary>
    [Comment("导入网卡记录")]
    [Table("IOT_ImportNetworkCard")]
    [Index(nameof(CarrierOperatorId), Name = "IDX_CarrierOperatorId")]
    public class ImportNetworkCardEntity : BaseEntityNotTenantCore
    {
        /// <summary>
        /// 导入总卡数
        /// </summary>
        [Comment("导入总卡数")]
        [Column("ImportSumCount", TypeName = "int")]
        public int ImportSumCount { get; set; }

        /// <summary>
        /// 导入成功卡数
        /// </summary>
        [Comment("导入成功卡数")]
        [Column("ImportSuccessCount", TypeName = "int")]
        public int ImportSuccessCount { get; set; }

        /// <summary>
        /// 运营商Id
        /// </summary>
        [Comment("运营商Id")]
        [Column("CarrierOperatorId", TypeName = "char(36)")]
        public Guid CarrierOperatorId { get; set; }

        /// <summary>
        /// 运营商套餐系列
        /// </summary>
        [Comment("运营商套餐系列")]
        [Column("CarrierOperatorPackageDictionaryId", TypeName = "char(36)")]
        public Guid CarrierOperatorPackageDictionaryId { get; set; }

        /// <summary>
        /// 网卡类型
        /// </summary>
        [Comment("网卡类型")]
        [Column("NetworkCardDictionaryId", TypeName = "char(36)")]
        public Guid NetworkCardDictionaryId { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [Comment("备注")]
        [Column("Remark", TypeName = "varchar(225)")]
        public string Remark { get; set; }
    }
}