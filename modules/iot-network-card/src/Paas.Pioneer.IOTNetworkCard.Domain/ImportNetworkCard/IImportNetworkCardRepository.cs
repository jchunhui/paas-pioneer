﻿using System;
using System.Threading.Tasks;
using Paas.Pioneer.Domain.Shared.Dto.Input;
using Paas.Pioneer.Domain.Shared.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.ImportNetworkCard.Dto.Input;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.ImportNetworkCard.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Domain.BaseExtensions;
using Volo.Abp.Domain.Repositories;

namespace Paas.Pioneer.IOTNetworkCard.Domain.ImportNetworkCard
{
    public interface IImportNetworkCardRepository : IRepository<ImportNetworkCardEntity, Guid>,
        IBaseExtensionRepository<ImportNetworkCardEntity>
    {
        Task<Page<GetImportNetworkCardPageListOutput>> GetPageListAsync(
            PageInput<GetImportNetworkCardPageListInput> input);
    }
}