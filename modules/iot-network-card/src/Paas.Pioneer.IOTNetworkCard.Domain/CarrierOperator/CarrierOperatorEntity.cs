﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Paas.Pioneer.Domain;

namespace Paas.Pioneer.IOTNetworkCard.Domain.CarrierOperator
{
    /// <summary>
    /// 运营商管理
    /// </summary>
    [Comment("运营商管理")]
    [Table("IOT_CarrierOperator")]
    [Index(nameof(Account), Name = "IDX_Account")]
    public class CarrierOperatorEntity : BaseNotTenantEntity
    {
        /// <summary>
        /// 名称
        /// </summary>
        [Comment("名称")]
        [Column("Name", TypeName = "varchar(50)")]
        public string Name { get; set; }

        /// <summary>
        /// 账户
        /// </summary>
        [Comment("账户")]
        [Column("Account", TypeName = "varchar(50)")]
        public string Account { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [Comment("密码")]
        [Column("Password", TypeName = "varchar(50)")]
        public string Password { get; set; }

        /// <summary>
        /// 密钥
        /// </summary>
        [Comment("密钥")]
        [Column("SecretKey", TypeName = "varchar(225)")]
        public string SecretKey { get; set; }

        /// <summary>
        /// 所属类别字典Id
        /// </summary>
        [Comment("所属类别字典Id")]
        [Column("DictionaryId", TypeName = "char(36)")]
        public Guid DictionaryId { get; set; }

        /// <summary>
        /// 绑定手机字典Id
        /// </summary>
        [Comment("绑定手机字典Id")]
        [Column("BindPhoneDictionaryId", TypeName = "char(36)")]
        public Guid BindPhoneDictionaryId { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        [Comment("排序")]
        [Column("Sort", TypeName = "int")]
        public int Sort { get; set; }

        /// <summary>
        /// 停机阈值(MB)
        /// </summary>
        [Comment("停机阈值")]
        [Column("ShutdownThreshold", TypeName = "int")]
        public int ShutdownThreshold { get; set; }

        /// <summary>
        /// 禁用
        /// </summary>
        public bool IsDisable { get; set; }
        
        /// <summary>
        /// 是否绑定手机号
        /// </summary>
        public bool IsBindPhone { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [Comment("备注")]
        [Column("Remark", TypeName = "varchar(225)")]
        public string Remark { get; set; }
    }
}