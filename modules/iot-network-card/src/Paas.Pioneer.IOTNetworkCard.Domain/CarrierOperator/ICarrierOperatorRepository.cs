﻿using System;
using Paas.Pioneer.IOTNetworkCard.Domain.BaseExtensions;
using Volo.Abp.Domain.Repositories;

namespace Paas.Pioneer.IOTNetworkCard.Domain.CarrierOperator
{
    public interface ICarrierOperatorRepository : IRepository<CarrierOperatorEntity, Guid>, IBaseExtensionRepository<CarrierOperatorEntity>
    {

    }
}
