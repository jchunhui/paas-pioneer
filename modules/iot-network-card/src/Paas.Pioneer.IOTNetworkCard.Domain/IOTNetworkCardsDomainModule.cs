﻿using Paas.Pioneer.IOTNetworkCard.Domain.Shared;
using Paas.Pioneer.IOTNetworkCard.Domain.Shared.MultiTenancy;
using Volo.Abp.AuditLogging;
using Volo.Abp.Modularity;
using Volo.Abp.MultiTenancy;
using Volo.Abp.TenantManagement;

namespace Paas.Pioneer.IOTNetworkCard.Domain
{
    [DependsOn(
        typeof(IOTNetworkCardsDomainSharedModule),
        typeof(AbpAuditLoggingDomainModule),
        typeof(AbpTenantManagementDomainModule)
    )]
    public class IOTNetworkCardsDomainModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpMultiTenancyOptions>(options =>
            {
                options.IsEnabled = MultiTenancyConsts.IsEnabled;
            });
        }
    }
}
