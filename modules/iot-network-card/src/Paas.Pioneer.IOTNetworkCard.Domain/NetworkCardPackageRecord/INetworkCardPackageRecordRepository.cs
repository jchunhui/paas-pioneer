﻿using System;
using Paas.Pioneer.IOTNetworkCard.Domain.BaseExtensions;
using Volo.Abp.Domain.Repositories;

namespace Paas.Pioneer.IOTNetworkCard.Domain.NetworkCardPackageRecord
{
    public interface INetworkCardPackageRecordRepository : IRepository<NetworkCardPackageRecordEntity, Guid>, IBaseExtensionRepository<NetworkCardPackageRecordEntity>
    {

    }
}
