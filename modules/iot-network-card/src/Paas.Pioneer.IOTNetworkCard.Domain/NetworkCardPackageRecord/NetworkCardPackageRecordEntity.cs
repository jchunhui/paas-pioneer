﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Paas.Pioneer.Domain;

namespace Paas.Pioneer.IOTNetworkCard.Domain.NetworkCardPackageRecord
{
    /// <summary>
    /// 网卡套餐记录
    /// </summary>
    [Comment("网卡套餐记录")]
    [Table("IOT_NetworkCardPackageRecord")]
    [Index(nameof(NetworkCardId), Name = "IDX_NetworkCardId")]
    public class NetworkCardPackageRecordEntity : BaseEntityNotTenantCore
    {
        /// <summary>
        /// 网卡Id
        /// </summary>
        [Comment("网卡Id")]
        [Column("NetworkCardId", TypeName = "char(36)")]
        public Guid NetworkCardId { get; set; }

        /// <summary>
        /// 套餐id
        /// </summary>
        [Comment("套餐id")]
        [Column("PackageId", TypeName = "char(36)")]
        public Guid PackageId { get; set; }
    }
}