﻿using Microsoft.AspNetCore.Mvc;
using Paas.Pioneer.Domain.Shared.Dto.Input;
using System;
using System.Threading.Tasks;
using Volo.Abp.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Paas.Pioneer.Domain.Shared.Configs;
using Paas.Pioneer.Domain.Shared.Dto.Output;
using Paas.Pioneer.Domain.Shared.Helpers;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.ImportNetworkCard.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.ImportNetworkCard.Dto.Input;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.ImportNetworkCard;
using Volo.Abp;

namespace Paas.Pioneer.IOTNetworkCard.HttpApi.Controllers.ImportNetworkCard
{
    /// <summary>
    /// 导入网卡记录
    /// </summary>
    [Route("api/iot/[controller]")]
    [Authorize]
    public class ImportNetworkCardController : AbpController
    {
        private readonly IImportNetworkCardService _importNetworkCardService;
        private readonly UploadConfig _uploadConfig;
        private readonly UploadHelper _uploadHelper;

        /// <summary>
        /// 构造函数
        /// </summary>
        public ImportNetworkCardController(IImportNetworkCardService importNetworkCardService,
            IOptionsMonitor<UploadConfig> uploadConfig,
            IOptionsMonitor<UploadHelper> uploadHelper)
        {
            _importNetworkCardService = importNetworkCardService;
            _uploadConfig = uploadConfig.CurrentValue;
            _uploadHelper = uploadHelper.CurrentValue;
        }

        /// <summary>
        /// 查询导入网卡记录
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<GetImportNetworkCardOutput> Get(Guid id)
        {
            return await _importNetworkCardService.GetAsync(id);
        }

        /// <summary>
        /// 查询分页导入网卡记录
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        [HttpPost("page-list")]
        public async Task<Page<GetImportNetworkCardPageListOutput>> GetPageList(
            [FromBody] PageInput<GetImportNetworkCardPageListInput> input)
        {
            return await _importNetworkCardService.GetPageListAsync(input);
        }

        /// <summary>
        /// 新增导入网卡记录
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        [HttpPost]
        public async Task Add([FromBody] AddImportNetworkCardInput input)
        {
            await _importNetworkCardService.AddAsync(input);
        }

        /// <summary>
        /// 新增单卡网卡记录
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        [HttpPost("single")]
        public async Task AddSingleCard([FromBody] AddSingleNetworkCardInput input)
        {
            await _importNetworkCardService.AddSingleCard(input);
        }

        /// <summary>
        /// 修改导入网卡记录
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        [HttpPut]
        public async Task Update([FromBody] UpdateImportNetworkCardInput input)
        {
            await _importNetworkCardService.UpdateAsync(input);
        }

        /// <summary>
        /// 删除导入网卡记录
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task SoftDelete(Guid id)
        {
            await _importNetworkCardService.DeleteAsync(id);
        }

        /// <summary>
        /// 批量删除导入网卡记录
        /// </summary>
        /// <param name="ids">主键集合</param>
        /// <returns></returns>
        [HttpDelete("ids")]
        public async Task BatchSoftDelete([FromBody] Guid[] ids)
        {
            await _importNetworkCardService.BatchSoftDeleteAsync(ids);
        }

        /// <summary>
        /// 上传excel文件
        /// </summary>
        /// <param name="file">文件流</param>
        /// <returns></returns>
        [HttpPost("upload-excel")]
        public async Task<string> AvatarUpload([FromForm] IFormFile file)
        {
            var config = _uploadConfig.Document;
            var res = await _uploadHelper.UploadAsync(file, config, new { CurrentUser.Id });
            if (res.Success)
            {
                return res.Data.FilePath;
            }

            throw new BusinessException(res.Msg ?? "上传失败！");
        }
    }
}