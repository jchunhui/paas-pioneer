﻿using Microsoft.AspNetCore.Mvc;
using Paas.Pioneer.Domain.Shared.Dto.Input;
using System;
using System.Threading.Tasks;
using Volo.Abp.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Paas.Pioneer.Domain.Shared.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.RealNameVerify.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.RealNameVerify.Dto.Input;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.RealNameVerify;

namespace Paas.Pioneer.IOTNetworkCard.HttpApi.Controllers.RealNameVerify
{
    /// <summary>
    /// 运营商实名验证管理
    /// </summary>
    [Route("api/iot/[controller]")]
    [Authorize]
    public class RealNameVerifyController : AbpController
    {
        private readonly IRealNameVerifyService _realNameVerifyService;

        /// <summary>
        /// 构造函数
        /// </summary>
        public RealNameVerifyController(IRealNameVerifyService realNameVerifyService)
        {
            _realNameVerifyService = realNameVerifyService;
        }

        /// <summary>
        /// 查询运营商实名验证管理
        /// </summary>
        /// <param name="carrierOperatorId">运营商主键</param>
        /// <returns></returns>
        [HttpGet("carrier-operator/{carrierOperatorId}")]
        public async Task<GetRealNameVerifyOutput> GetCarrierOperatorRealNameVerifyAsync(Guid carrierOperatorId)
        {
            return await _realNameVerifyService.GetCarrierOperatorRealNameVerifyAsync(carrierOperatorId);
        }

        /// <summary>
        /// 查询分页运营商实名验证管理
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        [HttpPost("page-list")]
        public async Task<Page<GetRealNameVerifyPageListOutput>> GetPageList(
            [FromBody] PageInput<GetRealNameVerifyPageListInput> input)
        {
            return await _realNameVerifyService.GetPageListAsync(input);
        }

        /// <summary>
        /// 修改运营商实名验证管理
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        [HttpPut("add-or-update")]
        public async Task AddOrUpdate([FromBody] UpdateRealNameVerifyInput input)
        {
            if (input.Id == null || input.Id == Guid.Empty)
            {
                await _realNameVerifyService.AddAsync(new AddRealNameVerifyInput()
                {
                    CarrierOperatorId = input.CarrierOperatorId,
                    VerifyDictionaryId = input.VerifyDictionaryId,
                    VerifyTip = input.VerifyTip,
                    VerifyPhoneDictionaryId = input.VerifyPhoneDictionaryId,
                    VerifyUrl = input.VerifyUrl,
                });
                return;
            }

            await _realNameVerifyService.UpdateAsync(input);
        }

        /// <summary>
        /// 删除运营商实名验证管理
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task SoftDelete(Guid id)
        {
            await _realNameVerifyService.DeleteAsync(id);
        }

        /// <summary>
        /// 批量删除运营商实名验证管理
        /// </summary>
        /// <param name="ids">主键集合</param>
        /// <returns></returns>
        [HttpDelete("ids")]
        public async Task BatchSoftDelete([FromBody] Guid[] ids)
        {
            await _realNameVerifyService.BatchSoftDeleteAsync(ids);
        }
    }
}