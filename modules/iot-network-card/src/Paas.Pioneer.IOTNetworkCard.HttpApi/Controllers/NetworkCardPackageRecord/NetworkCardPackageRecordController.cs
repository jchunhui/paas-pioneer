﻿using Microsoft.AspNetCore.Mvc;
using Paas.Pioneer.Domain.Shared.Dto.Input;
using System;
using System.Threading.Tasks;
using Volo.Abp.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Paas.Pioneer.Domain.Shared.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCardPackageRecord.Dto.Input;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCardPackageRecord;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCardPackageRecord.Dto.Output;

namespace Paas.Pioneer.IOTNetworkCard.HttpApi.Controllers.NetworkCardPackageRecord
{
    /// <summary>
    /// 网卡套餐记录
    /// </summary>
    [Route("api/iot/[controller]")]
    [Authorize]
    public class NetworkCardPackageRecordController : AbpController
    {
        private readonly INetworkCardPackageRecordService _networkCardPackageRecordService;

        /// <summary>
        /// 构造函数
        /// </summary>
        public NetworkCardPackageRecordController(INetworkCardPackageRecordService networkCardPackageRecordService)
        {
            _networkCardPackageRecordService = networkCardPackageRecordService;
        }

        /// <summary>
        /// 查询网卡套餐记录
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<GetNetworkCardPackageRecordOutput> Get(Guid id)
        {
            return await _networkCardPackageRecordService.GetAsync(id);
        }

        /// <summary>
        /// 查询分页网卡套餐记录
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        [HttpPost("page-list")]
        public async Task<Page<GetNetworkCardPackageRecordPageListOutput>> GetPageList(
            [FromBody] PageInput<GetNetworkCardPackageRecordPageListInput> input)
        {
            return await _networkCardPackageRecordService.GetPageListAsync(input);
        }

        /// <summary>
        /// 新增网卡套餐记录
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        [HttpPost]
        public async Task Add([FromBody] AddNetworkCardPackageRecordInput input)
        {
            await _networkCardPackageRecordService.AddAsync(input);
        }

        /// <summary>
        /// 修改网卡套餐记录
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        [HttpPut]
        public async Task Update([FromBody] UpdateNetworkCardPackageRecordInput input)
        {
            await _networkCardPackageRecordService.UpdateAsync(input);
        }

        /// <summary>
        /// 删除网卡套餐记录
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task SoftDelete(Guid id)
        {
            await _networkCardPackageRecordService.DeleteAsync(id);
        }

        /// <summary>
        /// 批量删除网卡套餐记录
        /// </summary>
        /// <param name="ids">主键集合</param>
        /// <returns></returns>
        [HttpDelete("ids")]
        public async Task BatchSoftDelete([FromBody] Guid[] ids)
        {
            await _networkCardPackageRecordService.BatchSoftDeleteAsync(ids);
        }
    }
}