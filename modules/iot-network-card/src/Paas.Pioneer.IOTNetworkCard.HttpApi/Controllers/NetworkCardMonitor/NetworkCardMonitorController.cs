﻿using Microsoft.AspNetCore.Mvc;
using Paas.Pioneer.Domain.Shared.Dto.Input;
using System;
using System.Threading.Tasks;
using Volo.Abp.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Paas.Pioneer.Domain.Shared.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCardMonitor.Dto.Input;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCardMonitor;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCardMonitor.Dto.Output;

namespace Paas.Pioneer.IOTNetworkCard.HttpApi.Controllers.NetworkCardMonitor
{
    /// <summary>
    /// 网卡监控
    /// </summary>
    [Route("api/iot/[controller]")]
    [Authorize]
    public class NetworkCardMonitorController : AbpController
    {
        private readonly INetworkCardMonitorService _networkCardMonitorService;

        /// <summary>
        /// 构造函数
        /// </summary>
        public NetworkCardMonitorController(INetworkCardMonitorService networkCardMonitorService)
        {
            _networkCardMonitorService = networkCardMonitorService;
        }

        /// <summary>
        /// 查询网卡监控
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<GetNetworkCardMonitorOutput> Get(Guid id)
        {
            return await _networkCardMonitorService.GetAsync(id);
        }

        /// <summary>
        /// 查询分页网卡监控
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        [HttpPost("page-list")]
        public async Task<Page<GetNetworkCardMonitorPageListOutput>> GetPageList(
            [FromBody] PageInput<GetNetworkCardMonitorPageListInput> input)
        {
            return await _networkCardMonitorService.GetPageListAsync(input);
        }

        /// <summary>
        /// 新增网卡监控
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        [HttpPost]
        public async Task Add([FromBody] AddNetworkCardMonitorInput input)
        {
            await _networkCardMonitorService.AddAsync(input);
        }

        /// <summary>
        /// 修改网卡监控
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        [HttpPut]
        public async Task Update([FromBody] UpdateNetworkCardMonitorInput input)
        {
            await _networkCardMonitorService.UpdateAsync(input);
        }

        /// <summary>
        /// 删除网卡监控
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task SoftDelete(Guid id)
        {
            await _networkCardMonitorService.DeleteAsync(id);
        }

        /// <summary>
        /// 批量删除网卡监控
        /// </summary>
        /// <param name="ids">主键集合</param>
        /// <returns></returns>
        [HttpDelete("ids")]
        public async Task BatchSoftDelete([FromBody] Guid[] ids)
        {
            await _networkCardMonitorService.BatchSoftDeleteAsync(ids);
        }
    }
}