﻿using Microsoft.AspNetCore.Mvc;
using Paas.Pioneer.Domain.Shared.Dto.Input;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Paas.Pioneer.Domain.Shared.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.CarrierOperator.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.CarrierOperator.Dto.Input;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.CarrierOperator;
using Paas.Pioneer.IOTNetworkCard.RequestFactory.Context;
using Paas.Pioneer.IOTNetworkCard.RequestFactory.Context.Enum;
using Paas.Pioneer.IOTNetworkCard.RequestFactory.Dto.Input;

namespace Paas.Pioneer.IOTNetworkCard.HttpApi.Controllers.CarrierOperator
{
    /// <summary>
    /// 运营商管理
    /// </summary>
    [Route("api/iot/[controller]")]
    [Authorize]
    public class CarrierOperatorController : AbpController
    {
        private readonly ICarrierOperatorService _carrierOperatorService;
        private readonly ContextRequestFactory _contextRequestFactory;

        /// <summary>
        /// 构造函数
        /// </summary>
        public CarrierOperatorController(ICarrierOperatorService carrierOperatorService,
            ContextRequestFactory contextRequestFactory)
        {
            _carrierOperatorService = carrierOperatorService;
            _contextRequestFactory = contextRequestFactory;
        }

        /// <summary>
        /// 测试方法
        /// </summary>
        /// <returns></returns>
        [HttpGet("hhh")]
        [AllowAnonymous]
        public async Task<GetCarrierOperatorOutput> Get()
        {
            await _contextRequestFactory.GetRequestFactory(EContextRequestType.Telecom).QueryIotRealNameAsync(
                new QueryIotRealNameInput
                {
                    AccessNumber = "1064949262207",
                    Account = "5U2J4m6Om6ih0w6Q2enpK8Yj8FzVc8HY",
                    Password = "4986of7JG2C8jNo5",
                    SecretKey = "Cb0X4O8L7"
                });
            return null;
        }


        /// <summary>
        /// 查询运营商管理
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<GetCarrierOperatorOutput> Get(Guid id)
        {
            return await _carrierOperatorService.GetAsync(id);
        }

        /// <summary>
        /// 查询分页运营商管理
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        [HttpPost("page-list")]
        public async Task<Page<GetCarrierOperatorPageListOutput>> GetPageList(
            [FromBody] PageInput<GetCarrierOperatorPageListInput> input)
        {
            return await _carrierOperatorService.GetPageListAsync(input);
        }

        /// <summary>
        /// 新增运营商管理
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        [HttpPost]
        public async Task Add([FromBody] AddCarrierOperatorInput input)
        {
            await _carrierOperatorService.AddAsync(input);
        }

        /// <summary>
        /// 修改运营商管理
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        [HttpPut]
        public async Task Update([FromBody] UpdateCarrierOperatorInput input)
        {
            await _carrierOperatorService.UpdateAsync(input);
        }

        /// <summary>
        /// 删除运营商管理
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task SoftDelete(Guid id)
        {
            await _carrierOperatorService.DeleteAsync(id);
        }

        /// <summary>
        /// 批量删除运营商管理
        /// </summary>
        /// <param name="ids">主键集合</param>
        /// <returns></returns>
        [HttpDelete("ids")]
        public async Task BatchSoftDelete([FromBody] Guid[] ids)
        {
            await _carrierOperatorService.BatchSoftDeleteAsync(ids);
        }

        /// <summary>
        /// 获取运营商列表
        /// </summary>
        /// <returns></returns>
        [HttpGet("list")]
        public async Task<IEnumerable<GetCarrierOperatorListOutput>> GetCarrierOperatorListAsync()
        {
            return await _carrierOperatorService.GetCarrierOperatorListAsync();
        }
    }
}