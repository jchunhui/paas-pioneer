﻿using Microsoft.AspNetCore.Mvc;
using Paas.Pioneer.Domain.Shared.Dto.Input;
using System;
using System.Threading.Tasks;
using Volo.Abp.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Paas.Pioneer.Domain.Shared.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.Package.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.Package;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.Package.Dto.Input;

namespace Paas.Pioneer.IOTNetworkCard.HttpApi.Controllers.Package
{
    /// <summary>
    /// 套餐管理
    /// </summary>
    [Route("api/iot/[controller]")]
    [Authorize]
    public class PackageController : AbpController
    {
        private readonly IPackageService _packageService;

        /// <summary>
        /// 构造函数
        /// </summary>
        public PackageController(IPackageService packageService)
        {
            _packageService = packageService;
        }

        /// <summary>
        /// 查询套餐管理
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<GetPackageOutput> Get(Guid id)
        {
            return await _packageService.GetAsync(id);
        }

        /// <summary>
        /// 查询分页套餐管理
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        [HttpPost("page-list")]
        public async Task<Page<GetPackagePageListOutput>> GetPageList(
            [FromBody] PageInput<GetPackagePageListInput> input)
        {
            return await _packageService.GetPageListAsync(input);
        }

        /// <summary>
        /// 新增套餐管理
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        [HttpPost]
        public async Task Add([FromBody] AddPackageInput input)
        {
            await _packageService.AddAsync(input);
        }

        /// <summary>
        /// 修改套餐管理
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        [HttpPut]
        public async Task Update([FromBody] UpdatePackageInput input)
        {
            await _packageService.UpdateAsync(input);
        }

        /// <summary>
        /// 删除套餐管理
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task SoftDelete(Guid id)
        {
            await _packageService.DeleteAsync(id);
        }

        /// <summary>
        /// 批量删除套餐管理
        /// </summary>
        /// <param name="ids">主键集合</param>
        /// <returns></returns>
        [HttpDelete("ids")]
        public async Task BatchSoftDelete([FromBody] Guid[] ids)
        {
            await _packageService.BatchSoftDeleteAsync(ids);
        }
    }
}