﻿using Microsoft.AspNetCore.Mvc;
using Paas.Pioneer.Domain.Shared.Dto.Input;
using System;
using System.Threading.Tasks;
using Volo.Abp.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Paas.Pioneer.Domain.Shared.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.CarrierOperator.Dto.Input;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCard;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCard.Dto.Input;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCard.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.RequestFactory.Dto.Output;

namespace Paas.Pioneer.IOTNetworkCard.HttpApi.Controllers.NetworkCard
{
    /// <summary>
    /// 网卡管理
    /// </summary>
    [Route("api/iot/[controller]")]
    [Authorize]
    public class NetworkCardController : AbpController
    {
        private readonly INetworkCardService _networkCardService;

        /// <summary>
        /// 构造函数
        /// </summary>
        public NetworkCardController(INetworkCardService networkCardService)
        {
            _networkCardService = networkCardService;
        }

        /// <summary>
        /// 查询网卡管理
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<GetNetworkCardOutput> Get(Guid id)
        {
            return await _networkCardService.GetAsync(id);
        }

        /// <summary>
        /// 查询分页网卡管理
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        [HttpPost("page-list")]
        public async Task<Page<GetNetworkCardPageListOutput>> GetPageList(
            [FromBody] PageInput<GetNetworkCardPageListInput> input)
        {
            return await _networkCardService.GetPageListAsync(input);
        }

        /// <summary>
        /// 绑定网卡
        /// </summary>
        /// <param name="iccid">卡号</param>
        /// <returns></returns>
        [HttpPost("bind-iccid/{iccid}")]
        public async Task<BindIccidOutput> BindIccidAsync(string iccid)
        {
            return await _networkCardService.BindIccidAsync(iccid);
        }

        /// <summary>
        /// 新增网卡管理
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        [HttpPost]
        public async Task Add([FromBody] AddNetworkCardInput input)
        {
            await _networkCardService.AddAsync(input);
        }

        /// <summary>
        /// 修改网卡管理
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        [HttpPut]
        public async Task Update([FromBody] UpdateNetworkCardInput input)
        {
            await _networkCardService.UpdateAsync(input);
        }

        /// <summary>
        /// 删除网卡管理
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task SoftDelete(Guid id)
        {
            await _networkCardService.DeleteAsync(id);
        }

        /// <summary>
        /// 批量删除网卡管理
        /// </summary>
        /// <param name="ids">主键集合</param>
        /// <returns></returns>
        [HttpDelete("ids")]
        public async Task BatchSoftDelete([FromBody] Guid[] ids)
        {
            await _networkCardService.BatchSoftDeleteAsync(ids);
        }

        #region 停机保号/复机/测试期去激活

        /// <summary>
        /// 停机保号/复机/测试期去激活
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        [HttpPost("sd-r-ta")]
        public async Task<DisabledNumberOutDto> ShutDownOrResumeOrTestActivation(
            [FromBody] UpShutDownOrResumeOrTestActivationInput input)
        {
            return await _networkCardService.ShutDownOrResumeOrTestActivationAsync(input);
        }

        #endregion

        #region 机卡重绑

        /// <summary>
        /// 机卡重绑
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        [HttpPost("imeirerecord")]
        public async Task<IMEIReRecOrdDtoOutDto> IMEIReRecOrd([FromBody] UpIMEIReRecOrdInput input)
        {
            return await _networkCardService.IMEIReRecOrd(input);
        }

        #endregion

        #region 流量总使用量查询(时间段)

        /// <summary>
        /// 流量总使用量查询(时间段)
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        [HttpPost("querytrafficbydate")]
        public async Task<QueryTrafficByDateOutDto> QueryTrafficByDate([FromBody] UpQueryTrafficByDateInput input)
        {
            return await _networkCardService.QueryTrafficByDate(input);
        }

        #endregion

        #region 接入号码查询接口

        /// <summary>
        /// 接入号码查询接口
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        [HttpPost("getTelephone")]
        public async Task<GetTelephoneOutDto> GetTelephone([FromBody] GetTelephoneInput input)
        {
            return await _networkCardService.GetTelephone(input);
        }

        #endregion

        #region 产品资料查询接口

        /// <summary>
        /// 产品资料查询接口
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        [HttpPost("prodinstquery")]
        public async Task<ProdInstQueryOutDto> ProdInstQuery([FromBody] ProdInstQueryInput input)
        {
            return await _networkCardService.ProdInstQuery(input);
        }

        #endregion

        #region 欠费查询接口

        /// <summary>
        /// 欠费查询接口
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        [HttpPost("queryowe")]
        public async Task<QueryOweOutDto> QueryOwe([FromBody] QueryOweInput input)
        {
            return await _networkCardService.QueryOwe(input);
        }

        #endregion

        #region SIM卡列表查询接口

        /// <summary>
        /// SIM卡列表查询接口
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        [HttpPost("getsimlist")]
        public async Task<GetSIMListOutDto> GetSIMList([FromBody] GetSIMListInput input)
        {
            return await _networkCardService.GetSIMList(input);
        }

        #endregion

        #region 流量总使用量查询(当月)

        /// <summary>
        /// 流量总使用量查询(当月)
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        [HttpPost("queryTraffic")]
        public async Task<QueryTrafficOutDto> QueryTraffic([FromBody] QueryTrafficInput input)
        {
            return await _networkCardService.QueryTraffic(input);
        }

        #endregion
    }
}