﻿using Microsoft.AspNetCore.Mvc;
using Paas.Pioneer.Domain.Shared.Dto.Input;
using System;
using System.Threading.Tasks;
using Volo.Abp.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Paas.Pioneer.Domain.Shared.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.EmptyFlowSpeedFictitious.Dto.Input;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.EmptyFlowSpeedFictitious.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.EmptyFlowSpeedFictitious;

namespace Paas.Pioneer.IOTNetworkCard.HttpApi.Controllers.EmptyFlowSpeedFictitious
{
    /// <summary>
    /// 虚量配置表
    /// </summary>
    [Route("api/iot/[controller]")]
    [Authorize]
    public class EmptyFlowSpeedFictitiousController : AbpController
    {
        private readonly IEmptyFlowSpeedFictitiousService _emptyFlowSpeedFictitiousService;

        /// <summary>
        /// 构造函数
        /// </summary>
        public EmptyFlowSpeedFictitiousController(IEmptyFlowSpeedFictitiousService emptyFlowSpeedFictitiousService)
        {
            _emptyFlowSpeedFictitiousService = emptyFlowSpeedFictitiousService;
        }

        /// <summary>
        /// 查询虚量配置表
        /// </summary>
        /// <param name="packageId">套餐id</param>
        /// <returns></returns>
        [HttpGet("{packageId}")]
        public async Task<GetEmptyFlowSpeedFictitiousOutput> Get(Guid packageId)
        {
            return await _emptyFlowSpeedFictitiousService.GetAsync(packageId);
        }

        /// <summary>
        /// 查询分页虚量配置表
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        [HttpPost("page-list")]
        public async Task<Page<GetEmptyFlowSpeedFictitiousPageListOutput>> GetPageList(
            [FromBody] PageInput<GetEmptyFlowSpeedFictitiousPageListInput> input)
        {
            return await _emptyFlowSpeedFictitiousService.GetPageListAsync(input);
        }

        /// <summary>
        /// 新增/编辑虚量配置表
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        [HttpPost("empty-quantity/add-update")]
        public async Task AddOrUpdateEmptyQuantityAsync([FromBody] AddEmptyFlowFictitiousInput input)
        {
            await _emptyFlowSpeedFictitiousService.AddOrUpdateEmptyQuantityAsync(input);
        }

        /// <summary>
        /// 新增/编辑限速配置表
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        [HttpPost("limit-velocity/add-update")]
        public async Task AddOrUpdateLimitVelocity([FromBody] AddSpeedFictitiousInput input)
        {
            await _emptyFlowSpeedFictitiousService.AddOrUpdateSpeedAsync(input);
        }

        /// <summary>
        /// 删除虚量配置表
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task SoftDelete(Guid id)
        {
            await _emptyFlowSpeedFictitiousService.DeleteAsync(id);
        }

        /// <summary>
        /// 批量删除虚量配置表
        /// </summary>
        /// <param name="ids">主键集合</param>
        /// <returns></returns>
        [HttpDelete("ids")]
        public async Task BatchSoftDelete([FromBody] Guid[] ids)
        {
            await _emptyFlowSpeedFictitiousService.BatchSoftDeleteAsync(ids);
        }
    }
}