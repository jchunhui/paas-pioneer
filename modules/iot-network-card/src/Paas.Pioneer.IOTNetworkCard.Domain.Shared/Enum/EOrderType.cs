﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paas.Pioneer.IOTNetworkCard.Domain.Shared.Enum
{
    /// <summary>
    /// 停机保号类型19表示停机保号，20表示停机保号后复机,21表示测试期去激活，22表示测试期去激活后回到测试激活
    /// </summary>
    public enum EOrderType
    {
        /// <summary>
        /// 停机保号
        /// </summary>
        [Description("表示停机保号")]
        ShutDown = 19,

        /// <summary>
        /// 停机保号后复机
        /// </summary>
        [Description("停机保号后复机")]
        Resume = 20,

        /// <summary>
        /// 测试期去激活
        /// </summary>
        [Description("测试期去激活")]
        TestActivation = 21,

        /// <summary>
        /// 测试期去激活后回到测试激活
        /// </summary>
        [Description("测试期去激活后回到测试激活")]
        TestActivationReturnActivation = 22,
    }
}
