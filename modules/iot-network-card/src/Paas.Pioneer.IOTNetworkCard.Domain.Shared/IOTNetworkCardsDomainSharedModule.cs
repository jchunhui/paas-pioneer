﻿using Paas.Pioneer.Redis;
using Volo.Abp.AuditLogging;
using Volo.Abp.Modularity;
using Volo.Abp.TenantManagement;
using Volo.Abp.VirtualFileSystem;

namespace Paas.Pioneer.IOTNetworkCard.Domain.Shared
{
    [DependsOn(
        typeof(AbpTenantManagementDomainSharedModule),
        typeof(AbpAuditLoggingDomainSharedModule),
        typeof(RedisModule)
        )]
    public class IOTNetworkCardsDomainSharedModule : AbpModule
    {
        public override void PreConfigureServices(ServiceConfigurationContext context)
        {
            IOTNetworkCardsGlobalFeatureConfigurator.Configure();
            IOTNetworkCardsModuleExtensionConfigurator.Configure();
        }

        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpVirtualFileSystemOptions>(options =>
            {
                options.FileSets.AddEmbedded<IOTNetworkCardsDomainSharedModule>();
            });
        }
    }
}
