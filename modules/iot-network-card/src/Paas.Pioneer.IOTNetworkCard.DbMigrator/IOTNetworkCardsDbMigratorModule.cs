﻿using Paas.Pioneer.IOTNetworkCard.Application.Contracts;
using Paas.Pioneer.IOTNetworkCard.EntityFrameworkCore.EntityFrameworkCore;
using Volo.Abp.Autofac;
using Volo.Abp.Modularity;

namespace Paas.Pioneer.IOTNetworkCard.DbMigrator
{
    [DependsOn(
        typeof(AbpAutofacModule),
        typeof(IOTNetworkCardsEntityFrameworkCoreModule),
        typeof(IOTNetworkCardsApplicationContractsModule)
        )]
    public class IOTNetworkCardsDbMigratorModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
           
        }
    }
}
