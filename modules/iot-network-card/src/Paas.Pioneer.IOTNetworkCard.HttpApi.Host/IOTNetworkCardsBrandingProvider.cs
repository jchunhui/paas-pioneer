﻿using Volo.Abp.DependencyInjection;
using Volo.Abp.Ui.Branding;

namespace Paas.Pioneer.IOTNetworkCard.HttpApi.Host
{
    [Dependency(ReplaceServices = true)]
    public class IOTNetworkCardsBrandingProvider : DefaultBrandingProvider
    {
        public override string AppName => "IOTNetworkCards";
    }
}
