﻿using System;
using Paas.Pioneer.Domain.Shared.ModelValidation;

namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCard.Dto.Output
{
    /// <summary>
    /// 网卡管理获取
    /// </summary>
    public class GetNetworkCardOutput
    {

        /// <summary>
        /// ICCID
        /// </summary>
        public string ICCID { get; set; }


        /// <summary>
        /// 接入号码
        /// </summary>
        public string AccessPhone { get; set; }


        /// <summary>
        /// 虚拟号
        /// </summary>
        public string EmptyPhone { get; set; }


        /// <summary>
        /// IMEI
        /// </summary>
        public string IMEI { get; set; }


        /// <summary>
        /// 钱包余额
        /// </summary>
        public decimal WalletBalance { get; set; }


        /// <summary>
        /// 赠送余额
        /// </summary>
        public decimal GiftBalance { get; set; }


        /// <summary>
        /// 运营商Id
        /// </summary>
        public Guid CarrierOperatorId { get; set; }


        /// <summary>
        /// 运营商套餐系列
        /// </summary>
        public Guid CarrierOperatorPackageDictionaryId { get; set; }


        /// <summary>
        /// 仅接口充值
        /// </summary>
        public bool IsApiRecharge { get; set; }


        /// <summary>
        /// 轮询网卡
        /// </summary>
        public bool IsPollingCard { get; set; }


        /// <summary>
        /// 导入网卡记录id
        /// </summary>
        public Guid ImportCardId { get; set; }


        /// <summary>
        /// 卡状态
        /// </summary>
        public int Status { get; set; }


        /// <summary>
        /// 是否实名
        /// </summary>
        public bool IsRealName { get; set; }


        /// <summary>
        /// 是否窜卡
        /// </summary>
        public bool IsChanneling { get; set; }


        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }


        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? LastModificationTime { get; set; }


        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreationTime { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public Guid Id { get; set; }

    }
}