﻿using Paas.Pioneer.IOTNetworkCard.Domain.Shared.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCard.Dto.Input
{
    public class UpShutDownOrResumeOrTestActivationInput
    {
        /// <summary>
        /// 网卡ID
        /// </summary>
        [Required(ErrorMessage = "请输入网卡ID")]
        public Guid NetworkCardId { get; set; }

        /// <summary>
        /// 停机保号类型
        /// </summary>
        [Required(ErrorMessage = "请输入停机保号类型")]
        public EOrderType orderTypeId { get; set; }
    }
}
