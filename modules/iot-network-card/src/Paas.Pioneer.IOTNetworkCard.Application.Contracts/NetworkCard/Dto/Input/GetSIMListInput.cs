﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCard.Dto.Input
{
    public class GetSIMListInput
    {
        /// <summary>
        /// 网卡ID
        /// </summary>
        [Required(ErrorMessage = "请输入网卡ID")]
        public Guid NetworkCardId { get; set; }

        /// <summary>
        /// 分页索引
        /// </summary>
        [Required(ErrorMessage = "请输入分页索引")]
        public String pageIndex { get; set; }

        /// <summary>
        /// 激活时间段：起始
        /// </summary>
        [Required(ErrorMessage = "请输入激活时间段：起始")]
        public String activationTimeBegin { get; set; }

        /// <summary>
        /// 激活时间段：截止 
        /// </summary>
        [Required(ErrorMessage = "请输入激活时间段：截止 ")]
        public String activationTimeEnd { get; set; }

        /// <summary>
        /// SIM卡状态
        /// </summary>
        [Required(ErrorMessage = "请输入SIM卡状态 ")]
        public String simStatus { get; set; }

        /// <summary>
        /// 群组ID
        /// </summary>
        [Required(ErrorMessage = "请输入群组ID ")]
        public String groupId { get; set; }
    }
}
