﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCard.Dto.Input
{
    public class UpIMEIReRecOrdInput
    {
        /// <summary>
        /// 网卡ID
        /// </summary>
        [Required(ErrorMessage = "请输入网卡ID")]
        public Guid NetworkCardId { get; set; }

        /// <summary>
        /// 重绑类型
        /// </summary>
        [Required(ErrorMessage = "请输入重绑类型")]
        public int bind_type { get; set; }

        /// <summary>
        /// 设备号
        /// </summary>
        public string imei { get; set; }
    }
}
