﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCard.Dto.Input
{
    public class QueryTrafficInput
    {
        /// <summary>
        /// 网卡ID
        /// </summary>
        [Required(ErrorMessage = "请输入网卡ID")]
        public Guid NetworkCardId { get; set; }

        /// <summary>
        /// 是否需要明细0：只返回总使用量，1：返回明细。建议使用0，1后续会被限制调用
        /// </summary>
        [Required(ErrorMessage = "请输入是否需要明细")]
        public int needDtl { get; set; }
    }
}
