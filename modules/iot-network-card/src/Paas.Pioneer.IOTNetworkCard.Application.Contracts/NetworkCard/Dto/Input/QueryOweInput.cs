﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCard.Dto.Input
{
    public class QueryOweInput
    {
        /// <summary>
        /// 网卡ID
        /// </summary>
        [Required(ErrorMessage = "请输入网卡ID")]
        public Guid NetworkCardId { get; set; }
    }
}
