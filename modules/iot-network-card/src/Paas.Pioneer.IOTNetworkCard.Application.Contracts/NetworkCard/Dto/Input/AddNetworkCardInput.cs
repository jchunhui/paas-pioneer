﻿using System;
using Paas.Pioneer.Domain.Shared.ModelValidation;
using System.ComponentModel.DataAnnotations;

namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCard.Dto.Input
{
    /// <summary>
    /// 网卡管理添加
    /// </summary>
    public class AddNetworkCardInput
    {

        /// <summary>
        /// ICCID
        /// </summary>
        [Required(ErrorMessage = "请输入ICCID")]
        public string ICCID { get; set; }


        /// <summary>
        /// 接入号码
        /// </summary>
        [Required(ErrorMessage = "请输入接入号码")]
        public string AccessPhone { get; set; }


        /// <summary>
        /// 虚拟号
        /// </summary>
        [Required(ErrorMessage = "请输入虚拟号")]
        public string EmptyPhone { get; set; }


        /// <summary>
        /// IMEI
        /// </summary>
        [Required(ErrorMessage = "请输入IMEI")]
        public string IMEI { get; set; }


        /// <summary>
        /// 钱包余额
        /// </summary>
        [Required(ErrorMessage = "请输入钱包余额")]
        public decimal WalletBalance { get; set; }


        /// <summary>
        /// 赠送余额
        /// </summary>
        [Required(ErrorMessage = "请输入赠送余额")]
        public decimal GiftBalance { get; set; }


        /// <summary>
        /// 运营商Id
        /// </summary>
        [Required(ErrorMessage = "请输入运营商Id")]
        public Guid CarrierOperatorId { get; set; }


        /// <summary>
        /// 运营商套餐系列
        /// </summary>
        [Required(ErrorMessage = "请输入运营商套餐系列")]
        public Guid CarrierOperatorPackageDictionaryId { get; set; }


        /// <summary>
        /// 仅接口充值
        /// </summary>
        [Required(ErrorMessage = "请输入仅接口充值")]
        public bool IsApiRecharge { get; set; }


        /// <summary>
        /// 轮询网卡
        /// </summary>
        [Required(ErrorMessage = "请输入轮询网卡")]
        public bool IsPollingCard { get; set; }


        /// <summary>
        /// 导入网卡记录id
        /// </summary>
        [Required(ErrorMessage = "请输入导入网卡记录id")]
        public Guid ImportCardId { get; set; }


        /// <summary>
        /// 卡状态
        /// </summary>
        [Required(ErrorMessage = "请输入卡状态")]
        public int Status { get; set; }


        /// <summary>
        /// 是否实名
        /// </summary>
        [Required(ErrorMessage = "请输入是否实名")]
        public bool IsRealName { get; set; }


        /// <summary>
        /// 是否窜卡
        /// </summary>
        [Required(ErrorMessage = "请输入是否窜卡")]
        public bool IsChanneling { get; set; }


        /// <summary>
        /// 备注
        /// </summary>
        [Required(ErrorMessage = "请输入备注")]
        public string Remark { get; set; }


        /// <summary>
        /// 修改时间
        /// </summary>
        [Required(ErrorMessage = "请输入修改时间")]
        public DateTime? LastModificationTime { get; set; }


        /// <summary>
        /// 创建时间
        /// </summary>
        [Required(ErrorMessage = "请输入创建时间")]
        public DateTime CreationTime { get; set; }


        /// <summary>
        /// 
        /// </summary>
        [Required(ErrorMessage = "请输入")]
        public Guid Id { get; set; }

    }
}