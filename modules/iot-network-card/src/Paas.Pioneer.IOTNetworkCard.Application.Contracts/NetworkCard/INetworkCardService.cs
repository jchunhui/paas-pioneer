﻿using Paas.Pioneer.Domain.Shared.Dto.Input;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;
using Paas.Pioneer.Domain.Shared.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.CarrierOperator.Dto.Input;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCard.Dto.Input;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCard.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.RequestFactory.Dto.Output;

namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCard
{
    /// <summary>
    /// 网卡管理接口
    /// </summary>
    public interface INetworkCardService : IApplicationService
    {
        /// <summary>
        /// 查询网卡管理
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        Task<GetNetworkCardOutput> GetAsync(Guid id);

        /// <summary>
        /// 查询分页网卡管理
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        Task<Page<GetNetworkCardPageListOutput>> GetPageListAsync(PageInput<GetNetworkCardPageListInput> model);

        /// <summary>
        /// 新增网卡管理
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        Task AddAsync(AddNetworkCardInput input);

        /// <summary>
        /// 修改网卡管理
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        Task UpdateAsync(UpdateNetworkCardInput input);

        /// <summary>
        /// 删除网卡管理
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        Task DeleteAsync(Guid id);

        /// <summary>
        /// 批量删除网卡管理
        /// </summary>
        /// <param name="ids">主键集合</param>
        /// <returns></returns>
        Task BatchSoftDeleteAsync(IEnumerable<Guid> ids);

        /// <summary>
        /// 停机保号/复机/测试期去激活
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        Task<DisabledNumberOutDto> ShutDownOrResumeOrTestActivationAsync(UpShutDownOrResumeOrTestActivationInput input);

        /// <summary>
        /// 机卡重绑
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        Task<IMEIReRecOrdDtoOutDto> IMEIReRecOrd(UpIMEIReRecOrdInput input);

        /// <summary>
        /// 流量总使用量查询(时间段)
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        Task<QueryTrafficByDateOutDto> QueryTrafficByDate(UpQueryTrafficByDateInput input);

        Task<GetTelephoneOutDto> GetTelephone(GetTelephoneInput input);

        Task<ProdInstQueryOutDto> ProdInstQuery(ProdInstQueryInput input);

        Task<QueryOweOutDto> QueryOwe(QueryOweInput input);

        Task<GetSIMListOutDto> GetSIMList(GetSIMListInput input);

        Task<QueryTrafficOutDto> QueryTraffic(QueryTrafficInput input);
        Task<BindIccidOutput> BindIccidAsync(string iccid);
    }
}