﻿using System;
using Paas.Pioneer.Domain.Shared.ModelValidation;
using System.ComponentModel.DataAnnotations;

namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.ImportNetworkCard.Dto.Input
{
    /// <summary>
    /// 导入网卡记录添加
    /// </summary>
    public class AddImportNetworkCardInput
    {
        /// <summary>
        /// 文件路径
        /// </summary>
        [Required(ErrorMessage = "请上传文件")]
        public string file { get; set; }

        /// <summary>
        /// 运营商Id
        /// </summary>
        [Required(ErrorMessage = "请输入运营商Id")]
        public Guid CarrierOperatorId { get; set; }

        /// <summary>
        /// 运营商套餐系列
        /// </summary>
        [Required(ErrorMessage = "请输入运营商套餐系列")]
        public Guid CarrierOperatorPackageDictionaryId { get; set; }

        /// <summary>
        /// 网卡类型
        /// </summary>
        [Required(ErrorMessage = "请输入网卡类型")]
        public Guid NetworkCardDictionaryId { get; set; }

        /// <summary>
        /// 不轮询网卡
        /// </summary>
        public bool isPollingCard { get; set; }
    }
}