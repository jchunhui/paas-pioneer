﻿using System;
using Paas.Pioneer.Domain.Shared.ModelValidation;

namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.ImportNetworkCard.Dto.Input
{
    /// <summary>
    /// 导入网卡记录分页
    /// </summary>
    public class GetImportNetworkCardPageListInput
    {
        /// <summary>
        /// 导入总卡数
        /// </summary>
        public int ImportSumCount { get; set; }

        /// <summary>
        /// 导入成功卡数
        /// </summary>
        public int ImportSuccessCount { get; set; }

        /// <summary>
        /// 运营商Id
        /// </summary>
        public Guid CarrierOperatorId { get; set; }

        /// <summary>
        /// 运营商套餐系列
        /// </summary>
        public Guid CarrierOperatorPackageDictionaryId { get; set; }

        /// <summary>
        /// 网卡类型
        /// </summary>
        public Guid NetworkCardDictionaryId { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreationTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Guid Id { get; set; }

    }
}