﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.ImportNetworkCard.Dto.Input
{
    /// <summary>
    /// 导入网卡记录添加
    /// </summary>
    public class AddSingleNetworkCardInput
    {
        /// <summary>
        /// ICCID
        /// </summary>
        [Required(ErrorMessage = "请输入ICCID")]
        public string ICCID { get; set; }

        /// <summary>
        /// 接入号码
        /// </summary>
        [Required(ErrorMessage = "请输入接入号码")]
        public string AccessPhone { get; set; }

        /// <summary>
        /// 运营商Id
        /// </summary>
        [Required(ErrorMessage = "请输入运营商Id")]
        public Guid CarrierOperatorId { get; set; }

        /// <summary>
        /// 运营商套餐系列
        /// </summary>
        [Required(ErrorMessage = "请输入运营商套餐系列")]
        public Guid CarrierOperatorPackageDictionaryId { get; set; }

        /// <summary>
        /// 网卡类型
        /// </summary>
        [Required(ErrorMessage = "请输入网卡类型")]
        public Guid NetworkCardDictionaryId { get; set; }

        /// <summary>
        /// 不轮询网卡
        /// </summary>
        public bool isPollingCard { get; set; }
    }
}
