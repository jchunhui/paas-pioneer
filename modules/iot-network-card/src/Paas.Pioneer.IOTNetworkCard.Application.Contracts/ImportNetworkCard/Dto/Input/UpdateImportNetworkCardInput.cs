﻿using System;
using Paas.Pioneer.Domain.Shared.ModelValidation;
using System.ComponentModel.DataAnnotations;

namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.ImportNetworkCard.Dto.Input
{
    /// <summary>
    /// 导入网卡记录修改
    /// </summary>
    public class UpdateImportNetworkCardInput
    {

        /// <summary>
        /// 导入总卡数
        /// </summary>
        [Required(ErrorMessage = "请输入导入总卡数")]
        public int ImportSumCount { get; set; }


        /// <summary>
        /// 导入成功卡数
        /// </summary>
        [Required(ErrorMessage = "请输入导入成功卡数")]
        public int ImportSuccessCount { get; set; }


        /// <summary>
        /// 运营商Id
        /// </summary>
        [Required(ErrorMessage = "请输入运营商Id")]
        public Guid CarrierOperatorId { get; set; }


        /// <summary>
        /// 运营商套餐系列
        /// </summary>
        [Required(ErrorMessage = "请输入运营商套餐系列")]
        public Guid CarrierOperatorPackageDictionaryId { get; set; }


        /// <summary>
        /// 网卡类型
        /// </summary>
        [Required(ErrorMessage = "请输入网卡类型")]
        public Guid NetworkCardDictionaryId { get; set; }


        /// <summary>
        /// 备注
        /// </summary>
        [Required(ErrorMessage = "请输入备注")]
        public string Remark { get; set; }


        /// <summary>
        /// 创建时间
        /// </summary>
        [Required(ErrorMessage = "请输入创建时间")]
        public DateTime CreationTime { get; set; }


        /// <summary>
        /// 
        /// </summary>
        [NotEqual("00000000-0000-0000-0000-000000000000", ErrorMessage = "请输入")]
        [Required(ErrorMessage = "请输入")]
        public Guid Id { get; set; }

    }
}