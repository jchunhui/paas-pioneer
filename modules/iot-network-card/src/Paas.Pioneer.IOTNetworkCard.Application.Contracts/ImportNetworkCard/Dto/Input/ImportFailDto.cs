﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.ImportNetworkCard.Dto.Input
{
    /// <summary>
    /// 导入网卡失败实体
    /// </summary>
    public class ImportFailDto
    {
        /// <summary>
        /// ICCID
        /// </summary>
        public string ICCID { get; set; }

        /// <summary>
        /// 接入号码
        /// </summary>
        public string AccessPhone { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Remark { get; set; }
    }
}
