﻿using Paas.Pioneer.Domain.Shared.Dto.Input;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;
using Paas.Pioneer.Domain.Shared.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.ImportNetworkCard.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.ImportNetworkCard.Dto.Input;
using Microsoft.AspNetCore.Mvc;

namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.ImportNetworkCard
{
    /// <summary>
    /// 导入网卡记录接口
    /// </summary>
    public interface IImportNetworkCardService : IApplicationService
    {
        /// <summary>
        /// 查询导入网卡记录
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        Task<GetImportNetworkCardOutput> GetAsync(Guid id);

        /// <summary>
        /// 查询分页导入网卡记录
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        Task<Page<GetImportNetworkCardPageListOutput>> GetPageListAsync(PageInput<GetImportNetworkCardPageListInput> model);

        /// <summary>
        /// 新增导入网卡记录
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        Task AddAsync(AddImportNetworkCardInput input);

        /// <summary>
        /// 新增单卡网卡记录
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        Task AddSingleCard([FromBody] AddSingleNetworkCardInput input);

        /// <summary>
        /// 修改导入网卡记录
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        Task UpdateAsync(UpdateImportNetworkCardInput input);

        /// <summary>
        /// 删除导入网卡记录
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        Task DeleteAsync(Guid id);

        /// <summary>
        /// 批量删除导入网卡记录
        /// </summary>
        /// <param name="ids">主键集合</param>
        /// <returns></returns>
        Task BatchSoftDeleteAsync(IEnumerable<Guid> ids);
    }
}