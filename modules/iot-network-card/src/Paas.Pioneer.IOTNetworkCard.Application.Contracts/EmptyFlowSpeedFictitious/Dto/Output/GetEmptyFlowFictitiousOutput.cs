﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Paas.Pioneer.Domain.Shared.ModelValidation;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.EmptyFlowSpeedFictitious.Dto.Input;

namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.EmptyFlowSpeedFictitious.Dto.Output
{
    /// <summary>
    /// 虚量配置表获取
    /// </summary>
    public class GetEmptyFlowSpeedFictitiousOutput
    {
        public Guid Id { get; set; }

        /// <summary>
        /// 套餐Id
        /// </summary>
        public Guid PackageId { get; set; }

        /// <summary>
        /// 虚拟Json
        /// </summary>
        public string EmptyFlowFictitiousJson { get; set; }

        /// <summary>
        /// 限速字典Id
        /// </summary>
        public Guid? LimitDictionaryId { get; set; }

        /// <summary>
        /// 虚拟Json
        /// </summary>
        public List<FictitiousConfigureDto> EmptyFlowFictitiousList => EmptyFlowFictitiousJson.IsNullOrEmpty()
            ? new List<FictitiousConfigureDto>()
            : JsonConvert.DeserializeObject<List<FictitiousConfigureDto>>(EmptyFlowFictitiousJson);

        /// <summary>
        /// 限速Json
        /// </summary>
        public string SpeedFictitiousJson { get; set; }

        public List<SpeedConfigureDto> SpeedFictitiousList => SpeedFictitiousJson.IsNullOrEmpty()
            ? new List<SpeedConfigureDto>()
            : JsonConvert.DeserializeObject<List<SpeedConfigureDto>>(
                SpeedFictitiousJson);
    }
}