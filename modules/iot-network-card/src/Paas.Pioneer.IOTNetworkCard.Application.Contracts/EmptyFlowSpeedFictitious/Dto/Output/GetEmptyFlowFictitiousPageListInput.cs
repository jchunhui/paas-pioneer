﻿using System;
using Paas.Pioneer.Domain.Shared.ModelValidation;

namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.EmptyFlowSpeedFictitious.Dto.Output
{
    /// <summary>
    /// 获取虚量配置表分页
    /// </summary>
    public class GetEmptyFlowSpeedFictitiousPageListOutput
    {

        /// <summary>
        /// 套餐Id
        /// </summary>
        public Guid PackageId { get; set; }


        /// <summary>
        /// 虚拟Json
        /// </summary>
        public string EmptyFlowFictitiousJson { get; set; }

        /// <summary>
        /// 限速Json
        /// </summary>
        public string SpeedFictitiousJson { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? LastModificationTime { get; set; }


        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreationTime { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public Guid Id { get; set; }

    }
}