﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.EmptyFlowSpeedFictitious.Dto.Input
{
    /// <summary>
    /// 虚拟配置
    /// </summary>
    public class FictitiousConfigureDto
    {
        /// <summary>
        /// 区间最小值
        /// </summary>
        public decimal Begin { get; set; }

        /// <summary>
        /// 区间最大值
        /// </summary>
        public decimal End { get; set; }

        /// <summary>
        /// 区间-真实流量（MB）
        /// </summary>
        public decimal Real => (End - Begin);

        /// <summary>
        /// 区间-虚量（MB）
        /// </summary>
        public decimal Fictitiou => this.Real * this.Ratio;

        /// <summary>
        /// 比例
        /// </summary>
        public decimal Ratio { get; set; }

        public int Sort { get; set; }
    }
}