﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.EmptyFlowSpeedFictitious.Dto.Input
{
    /// <summary>
    /// 虚量配置表添加
    /// </summary>
    public class AddEmptyFlowFictitiousInput
    {
        public Guid? Id { get; set; }
        
        /// <summary>
        /// 套餐Id
        /// </summary>
        [Required(ErrorMessage = "请输入套餐Id")]
        public Guid PackageId { get; set; }
        
        
        /// <summary>
        /// 虚量配置
        /// </summary>
        [Required(ErrorMessage = "请输入虚量配置")]
        public List<FictitiousConfigureDto> EmptyFlowFictitiousJson { get; set; }
    }
}