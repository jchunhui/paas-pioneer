﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.EmptyFlowSpeedFictitious.Dto.Input
{
    /// <summary>
    /// 限速配置
    /// </summary>
    public class SpeedConfigureDto
    {
        /// <summary>
        /// 区间最小值
        /// </summary>
        public decimal Begin { get; set; }

        /// <summary>
        /// 区间最大值
        /// </summary>
        public decimal End { get; set; }

        /// <summary>
        /// 限速-M
        /// </summary>
        public decimal LimitVelocity { get; set; }

        public int Sort { get; set; }
    }
}