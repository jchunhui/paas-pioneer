﻿using Paas.Pioneer.Domain.Shared.ModelValidation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.EmptyFlowSpeedFictitious.Dto.Input
{
    /// <summary>
    /// 限速配置表添加
    /// </summary>
    public class AddSpeedFictitiousInput
    {
        public Guid? Id { get; set; }

        /// <summary>
        /// 套餐Id
        /// </summary>
        [Required(ErrorMessage = "请输入套餐Id")]
        public Guid PackageId { get; set; }

        /// <summary>
        /// 限速字典Id
        /// </summary>
        [Required(ErrorMessage = "限速字典Id")]
        public Guid LimitDictionaryId { get; set; }
        
        /// <summary>
        /// 限速Json
        /// </summary>
        [Required(ErrorMessage = "请输入限速数据")]
        public List<SpeedConfigureDto> SpeedFictitiousJson { get; set; }
    }
}
