﻿using Paas.Pioneer.Domain.Shared.Dto.Input;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;
using Paas.Pioneer.Domain.Shared.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.EmptyFlowSpeedFictitious.Dto.Input;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.EmptyFlowSpeedFictitious.Dto.Output;

namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.EmptyFlowSpeedFictitious
{
    /// <summary>
    /// 虚量配置表接口
    /// </summary>
    public interface IEmptyFlowSpeedFictitiousService : IApplicationService
    {
        /// <summary>
        /// 查询虚量配置表
        /// </summary>
        /// <param name="packageId">套餐id</param>
        /// <returns></returns>
        Task<GetEmptyFlowSpeedFictitiousOutput> GetAsync(Guid packageId);

        /// <summary>
        /// 查询分页虚量配置表
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        Task<Page<GetEmptyFlowSpeedFictitiousPageListOutput>> GetPageListAsync(PageInput<GetEmptyFlowSpeedFictitiousPageListInput> model);

        /// <summary>
        /// 新增/编辑虚量配置表
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        Task AddOrUpdateEmptyQuantityAsync(AddEmptyFlowFictitiousInput input);

        /// <summary>
        /// 新增/编辑限速配置表
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        Task AddOrUpdateSpeedAsync(AddSpeedFictitiousInput input);

        /// <summary>
        /// 删除虚量配置表
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        Task DeleteAsync(Guid id);

        /// <summary>
        /// 批量删除虚量配置表
        /// </summary>
        /// <param name="ids">主键集合</param>
        /// <returns></returns>
        Task BatchSoftDeleteAsync(IEnumerable<Guid> ids);
    }
}