﻿using System;
using Paas.Pioneer.Domain.Shared.ModelValidation;
using System.ComponentModel.DataAnnotations;

namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCardPackageRecord.Dto.Input
{
    /// <summary>
    /// 网卡套餐记录修改
    /// </summary>
    public class UpdateNetworkCardPackageRecordInput
    {

        /// <summary>
        /// 网卡Id
        /// </summary>
        [Required(ErrorMessage = "请输入网卡Id")]
        public Guid NetworkCardId { get; set; }


        /// <summary>
        /// 套餐id
        /// </summary>
        [Required(ErrorMessage = "请输入套餐id")]
        public Guid PackageId { get; set; }


        /// <summary>
        /// 创建时间
        /// </summary>
        [Required(ErrorMessage = "请输入创建时间")]
        public DateTime CreationTime { get; set; }


        /// <summary>
        /// 
        /// </summary>
        [NotEqual("00000000-0000-0000-0000-000000000000", ErrorMessage = "请输入")]
        [Required(ErrorMessage = "请输入")]
        public Guid Id { get; set; }

    }
}