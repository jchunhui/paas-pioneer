﻿using System;
using Paas.Pioneer.Domain.Shared.ModelValidation;

namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCardPackageRecord.Dto.Output
{
    /// <summary>
    /// 获取网卡套餐记录分页
    /// </summary>
    public class GetNetworkCardPackageRecordPageListOutput
    {

        /// <summary>
        /// 网卡Id
        /// </summary>
        public Guid NetworkCardId { get; set; }


        /// <summary>
        /// 套餐id
        /// </summary>
        public Guid PackageId { get; set; }


        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreationTime { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public Guid Id { get; set; }

    }
}