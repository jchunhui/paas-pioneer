﻿using Paas.Pioneer.Domain.Shared.Dto.Input;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;
using Paas.Pioneer.Domain.Shared.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCardPackageRecord.Dto.Input;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCardPackageRecord.Dto.Output;

namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCardPackageRecord
{
    /// <summary>
    /// 网卡套餐记录接口
    /// </summary>
    public interface INetworkCardPackageRecordService : IApplicationService
    {
        /// <summary>
        /// 查询网卡套餐记录
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        Task<GetNetworkCardPackageRecordOutput> GetAsync(Guid id);

        /// <summary>
        /// 查询分页网卡套餐记录
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        Task<Page<GetNetworkCardPackageRecordPageListOutput>> GetPageListAsync(PageInput<GetNetworkCardPackageRecordPageListInput> model);

        /// <summary>
        /// 新增网卡套餐记录
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        Task AddAsync(AddNetworkCardPackageRecordInput input);

        /// <summary>
        /// 修改网卡套餐记录
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        Task UpdateAsync(UpdateNetworkCardPackageRecordInput input);

        /// <summary>
        /// 删除网卡套餐记录
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        Task DeleteAsync(Guid id);

        /// <summary>
        /// 批量删除网卡套餐记录
        /// </summary>
        /// <param name="ids">主键集合</param>
        /// <returns></returns>
        Task BatchSoftDeleteAsync(IEnumerable<Guid> ids);
    }
}