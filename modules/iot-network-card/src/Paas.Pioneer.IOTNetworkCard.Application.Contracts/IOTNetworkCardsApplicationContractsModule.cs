﻿using Paas.Pioneer.IOTNetworkCard.Domain.Shared;
using Volo.Abp.Modularity;
using Volo.Abp.ObjectExtending;
using Volo.Abp.TenantManagement;

namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts
{
    [DependsOn(
        typeof(IOTNetworkCardsDomainSharedModule),
        typeof(AbpTenantManagementApplicationContractsModule),
        typeof(AbpObjectExtendingModule)
        )]
    public class IOTNetworkCardsApplicationContractsModule : AbpModule
    {
        public override void PreConfigureServices(ServiceConfigurationContext context)
        {
            IOTNetworkCardsDtoExtensions.Configure();
        }
    }
}
