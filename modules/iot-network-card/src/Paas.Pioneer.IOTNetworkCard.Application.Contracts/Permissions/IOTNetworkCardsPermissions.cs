﻿namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.Permissions
{
    public static class IOTNetworkCardsPermissions
    {
        public const string GroupName = "IOTNetworkCards";

        //Add your own permission names. Example:
        //public const string MyPermission1 = GroupName + ".MyPermission1";
    }
}