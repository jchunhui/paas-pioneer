﻿using Volo.Abp.Authorization.Permissions;

namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.Permissions
{
    public class IOTNetworkCardsPermissionDefinitionProvider : PermissionDefinitionProvider
    {
        public override void Define(IPermissionDefinitionContext context)
        {
            var myGroup = context.AddGroup(IOTNetworkCardsPermissions.GroupName);
            //Define your own permissions here. Example:
            //myGroup.AddPermission(IOTNetworkCardsPermissions.MyPermission1, L("Permission:MyPermission1"));
        }
    }
}
