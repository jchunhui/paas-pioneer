﻿namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.CarrierOperator.Dto.Input;

public class BindIccidOutput
{
    public bool IsBindMobile { get; set; }
}