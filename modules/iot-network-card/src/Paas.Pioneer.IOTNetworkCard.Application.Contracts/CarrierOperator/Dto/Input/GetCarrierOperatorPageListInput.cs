﻿using System;
using Paas.Pioneer.Domain.Shared.ModelValidation;

namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.CarrierOperator.Dto.Input
{
    /// <summary>
    /// 运营商管理分页
    /// </summary>
    public class GetCarrierOperatorPageListInput
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 账户
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 密钥
        /// </summary>
        public string SecretKey { get; set; }

        /// <summary>
        /// 所属类别字典Id
        /// </summary>
        public Guid DictionaryId { get; set; }

        /// <summary>
        /// 绑定手机字典Id
        /// </summary>
        public Guid BindPhoneDictionaryId { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 停机阈值(MB)
        /// </summary>
        public int ShutdownThreshold { get; set; }

        /// <summary>
        /// 禁用
        /// </summary>
        public bool IsDisable { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? LastModificationTime { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreationTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Guid Id { get; set; }

    }
}