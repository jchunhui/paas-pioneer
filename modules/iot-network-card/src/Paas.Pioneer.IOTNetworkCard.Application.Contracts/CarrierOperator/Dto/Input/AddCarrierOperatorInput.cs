﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.CarrierOperator.Dto.Input
{
    /// <summary>
    /// 运营商管理添加
    /// </summary>
    public class AddCarrierOperatorInput
    {
        /// <summary>
        /// 名称
        /// </summary>
        [Required(ErrorMessage = "请输入名称")]
        public string Name { get; set; }

        /// <summary>
        /// 账户
        /// </summary>
        [Required(ErrorMessage = "请输入账户")]
        public string Account { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [Required(ErrorMessage = "请输入密码")]
        public string Password { get; set; }
        
        /// <summary>
        /// 密钥
        /// </summary>
        [Required(ErrorMessage = "请输入密钥")]
        public string SecretKey { get; set; }

        /// <summary>
        /// 所属类别字典Id
        /// </summary>
        [Required(ErrorMessage = "请输入所属类别字典Id")]
        public Guid DictionaryId { get; set; }

        /// <summary>
        /// 绑定手机字典Id
        /// </summary>
        [Required(ErrorMessage = "请输入绑定手机字典Id")]
        public Guid BindPhoneDictionaryId { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        [Required(ErrorMessage = "请输入排序")]
        public int Sort { get; set; }

        /// <summary>
        /// 停机阈值(MB)
        /// </summary>
        [Required(ErrorMessage = "请输入停机阈值(MB)")]
        public int ShutdownThreshold { get; set; }

        /// <summary>
        /// 禁用
        /// </summary>
        [Required(ErrorMessage = "请输入禁用")]
        public bool IsDisable { get; set; }
        
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
    }
}