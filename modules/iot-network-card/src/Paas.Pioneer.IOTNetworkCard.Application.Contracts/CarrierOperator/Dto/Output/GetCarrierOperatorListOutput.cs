﻿using System;

namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.CarrierOperator.Dto.Output;

public class GetCarrierOperatorListOutput
{
    /// <summary>
    /// 运营商Id
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// 运营商名称
    /// </summary>
    public string Name { get; set; }
}