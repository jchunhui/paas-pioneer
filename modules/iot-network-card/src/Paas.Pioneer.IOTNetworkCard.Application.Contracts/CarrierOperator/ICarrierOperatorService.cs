﻿using Paas.Pioneer.Domain.Shared.Dto.Input;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;
using Paas.Pioneer.Domain.Shared.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.CarrierOperator.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.CarrierOperator.Dto.Input;

namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.CarrierOperator
{
    /// <summary>
    /// 运营商管理接口
    /// </summary>
    public interface ICarrierOperatorService : IApplicationService
    {
        /// <summary>
        /// 查询运营商管理
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        Task<GetCarrierOperatorOutput> GetAsync(Guid id);

        /// <summary>
        /// 查询分页运营商管理
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        Task<Page<GetCarrierOperatorPageListOutput>> GetPageListAsync(PageInput<GetCarrierOperatorPageListInput> model);

        /// <summary>
        /// 新增运营商管理
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        Task AddAsync(AddCarrierOperatorInput input);

        /// <summary>
        /// 修改运营商管理
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        Task UpdateAsync(UpdateCarrierOperatorInput input);

        /// <summary>
        /// 删除运营商管理
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        Task DeleteAsync(Guid id);

        /// <summary>
        /// 批量删除运营商管理
        /// </summary>
        /// <param name="ids">主键集合</param>
        /// <returns></returns>
        Task BatchSoftDeleteAsync(IEnumerable<Guid> ids);

        /// <summary>
        /// 获取运营商列表
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<GetCarrierOperatorListOutput>> GetCarrierOperatorListAsync();
    }
}