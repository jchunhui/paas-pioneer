﻿using System;
using Paas.Pioneer.Domain.Shared.ModelValidation;

namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCardMonitor.Dto.Output
{
    /// <summary>
    /// 网卡监控获取
    /// </summary>
    public class GetNetworkCardMonitorOutput
    {

        /// <summary>
        /// 网卡Id
        /// </summary>
        public Guid NetworkCardId { get; set; }


        /// <summary>
        /// 使用流量
        /// </summary>
        public int UseFlow { get; set; }


        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreationTime { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public Guid Id { get; set; }

    }
}