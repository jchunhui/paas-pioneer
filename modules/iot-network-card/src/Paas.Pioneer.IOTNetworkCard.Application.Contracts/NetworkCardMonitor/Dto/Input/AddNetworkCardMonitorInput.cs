﻿using System;
using Paas.Pioneer.Domain.Shared.ModelValidation;
using System.ComponentModel.DataAnnotations;

namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCardMonitor.Dto.Input
{
    /// <summary>
    /// 网卡监控添加
    /// </summary>
    public class AddNetworkCardMonitorInput
    {

        /// <summary>
        /// 网卡Id
        /// </summary>
        [Required(ErrorMessage = "请输入网卡Id")]
        public Guid NetworkCardId { get; set; }


        /// <summary>
        /// 使用流量
        /// </summary>
        [Required(ErrorMessage = "请输入使用流量")]
        public int UseFlow { get; set; }


        /// <summary>
        /// 创建时间
        /// </summary>
        [Required(ErrorMessage = "请输入创建时间")]
        public DateTime CreationTime { get; set; }


        /// <summary>
        /// 
        /// </summary>
        [Required(ErrorMessage = "请输入")]
        public Guid Id { get; set; }

    }
}