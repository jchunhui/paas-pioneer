﻿using Paas.Pioneer.Domain.Shared.Dto.Input;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;
using Paas.Pioneer.Domain.Shared.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCardMonitor.Dto.Input;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCardMonitor.Dto.Output;

namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.NetworkCardMonitor
{
    /// <summary>
    /// 网卡监控接口
    /// </summary>
    public interface INetworkCardMonitorService : IApplicationService
    {
        /// <summary>
        /// 查询网卡监控
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        Task<GetNetworkCardMonitorOutput> GetAsync(Guid id);

        /// <summary>
        /// 查询分页网卡监控
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        Task<Page<GetNetworkCardMonitorPageListOutput>> GetPageListAsync(PageInput<GetNetworkCardMonitorPageListInput> model);

        /// <summary>
        /// 新增网卡监控
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        Task AddAsync(AddNetworkCardMonitorInput input);

        /// <summary>
        /// 修改网卡监控
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        Task UpdateAsync(UpdateNetworkCardMonitorInput input);

        /// <summary>
        /// 删除网卡监控
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        Task DeleteAsync(Guid id);

        /// <summary>
        /// 批量删除网卡监控
        /// </summary>
        /// <param name="ids">主键集合</param>
        /// <returns></returns>
        Task BatchSoftDeleteAsync(IEnumerable<Guid> ids);
    }
}