﻿using Paas.Pioneer.Domain.Shared.Dto.Input;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;
using Paas.Pioneer.Domain.Shared.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.RealNameVerify.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.RealNameVerify.Dto.Input;

namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.RealNameVerify
{
    /// <summary>
    /// 运营商实名验证管理接口
    /// </summary>
    public interface IRealNameVerifyService : IApplicationService
    {
        /// <summary>
        /// 查询运营商实名验证管理
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        Task<GetRealNameVerifyOutput> GetAsync(Guid id);

        /// <summary>
        /// 查询运营商实名验证管理
        /// </summary>
        /// <param name="carrierOperatorId">运营商主键</param>
        /// <returns></returns>
        Task<GetRealNameVerifyOutput> GetCarrierOperatorRealNameVerifyAsync(Guid carrierOperatorId);

        /// <summary>
        /// 查询分页运营商实名验证管理
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        Task<Page<GetRealNameVerifyPageListOutput>> GetPageListAsync(PageInput<GetRealNameVerifyPageListInput> model);

        /// <summary>
        /// 新增运营商实名验证管理
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        Task AddAsync(AddRealNameVerifyInput input);

        /// <summary>
        /// 修改运营商实名验证管理
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        Task UpdateAsync(UpdateRealNameVerifyInput input);

        /// <summary>
        /// 删除运营商实名验证管理
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        Task DeleteAsync(Guid id);

        /// <summary>
        /// 批量删除运营商实名验证管理
        /// </summary>
        /// <param name="ids">主键集合</param>
        /// <returns></returns>
        Task BatchSoftDeleteAsync(IEnumerable<Guid> ids);
    }
}