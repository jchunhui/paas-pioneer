﻿using System;
using Paas.Pioneer.Domain.Shared.ModelValidation;

namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.RealNameVerify.Dto.Output
{
    /// <summary>
    /// 运营商实名验证管理获取
    /// </summary>
    public class GetRealNameVerifyOutput
    {

        /// <summary>
        /// 运营商Id
        /// </summary>
        public Guid CarrierOperatorId { get; set; }


        /// <summary>
        /// 验证方式字典Id
        /// </summary>
        public Guid VerifyDictionaryId { get; set; }


        /// <summary>
        /// 官方实名跳转复制号码字典Id
        /// </summary>
        public Guid VerifyPhoneDictionaryId { get; set; }


        /// <summary>
        /// 验证地址
        /// </summary>
        public string VerifyUrl { get; set; }


        /// <summary>
        /// 验证提示
        /// </summary>
        public string VerifyTip { get; set; }


        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreationTime { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public Guid Id { get; set; }

    }
}