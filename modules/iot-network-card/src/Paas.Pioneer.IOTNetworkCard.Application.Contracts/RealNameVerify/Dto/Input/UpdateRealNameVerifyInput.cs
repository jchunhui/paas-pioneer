﻿using System;
using Paas.Pioneer.Domain.Shared.ModelValidation;
using System.ComponentModel.DataAnnotations;

namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.RealNameVerify.Dto.Input
{
    /// <summary>
    /// 运营商实名验证管理修改
    /// </summary>
    public class UpdateRealNameVerifyInput
    {
        public Guid? Id { get; set; }
        
        /// <summary>
        /// 运营商Id
        /// </summary>
        [Required(ErrorMessage = "请输入运营商Id")]
        public Guid CarrierOperatorId { get; set; }


        /// <summary>
        /// 验证方式字典Id
        /// </summary>
        [Required(ErrorMessage = "请输入验证方式字典Id")]
        public Guid VerifyDictionaryId { get; set; }


        /// <summary>
        /// 官方实名跳转复制号码字典Id
        /// </summary>
        [Required(ErrorMessage = "请输入官方实名跳转复制号码字典Id")]
        public Guid VerifyPhoneDictionaryId { get; set; }


        /// <summary>
        /// 验证地址
        /// </summary>
        [Required(ErrorMessage = "请输入验证地址")]
        public string VerifyUrl { get; set; }


        /// <summary>
        /// 验证提示
        /// </summary>
        [Required(ErrorMessage = "请输入验证提示")]
        public string VerifyTip { get; set; }

    }
}