﻿using System;
using Paas.Pioneer.Domain.Shared.ModelValidation;
using System.ComponentModel.DataAnnotations;

namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.Package.Dto.Input
{
    /// <summary>
    /// 套餐管理修改
    /// </summary>
    public class UpdatePackageInput
    {
        /// <summary>
        /// 
        /// </summary>
        [NotEqual("00000000-0000-0000-0000-000000000000", ErrorMessage = "请输入")]
        [Required(ErrorMessage = "请输入")]
        public Guid Id { get; set; }

        /// <summary>
        /// 运营商Id
        /// </summary>
        [Required(ErrorMessage = "请输入运营商Id")]
        public Guid CarrierOperatorId { get; set; }

        /// <summary>
        /// 运营商套餐系列
        /// </summary>
        [Required(ErrorMessage = "请输入运营商套餐系列")]
        public Guid CarrierOperatorPackageDictionaryId { get; set; }

        /// <summary>
        /// 充值类型
        /// </summary>
        [Required(ErrorMessage = "请输入充值类型")]
        public Guid RechargeDictionaryId { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Required(ErrorMessage = "请输入名称")]
        public string Name { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 套餐类型
        /// </summary>
        [Required(ErrorMessage = "请输入套餐类型")]
        public Guid DictionaryId { get; set; }

        /// <summary>
        /// 显示
        /// </summary>
        public bool IsShow { get; set; }

        /// <summary>
        /// 每月最大充值数
        /// </summary>
        [Required(ErrorMessage = "请输入每月最大充值数")]
        public int MaxRechargeCount { get; set; }

        /// <summary>
        /// 套餐数量
        /// </summary>
        [Required(ErrorMessage = "请输入套餐数量")]
        public int Count { get; set; }

        /// <summary>
        /// 成本价
        /// </summary>
        [Required(ErrorMessage = "请输入成本价")]
        public decimal CostPrice { get; set; }

        /// <summary>
        /// 套餐金额
        /// </summary>
        [Required(ErrorMessage = "请输入套餐金额")]
        public decimal PackageAmount { get; set; }

        /// <summary>
        /// 虚量（MB）
        /// </summary>
        [Required(ErrorMessage = "请输入虚量（MB）")]
        public int EmptyFlow { get; set; }

        /// <summary>
        /// 真实流量（MB）
        /// </summary>
        [Required(ErrorMessage = "请输入真实流量（MB）")]
        public int RealFlow { get; set; }

        /// <summary>
        /// 禁用
        /// </summary>
        public bool IsDisable { get; set; }

        /// <summary>
        /// 热卖
        /// </summary>
        public bool IsHeat { get; set; }

        /// <summary>
        /// 是否开户套餐
        /// </summary>
        public bool IsOpenAccount { get; set; }

        /// <summary>
        /// 开启时间
        /// </summary>
        public DateTime? OpenDateTime { get; set; }

        /// <summary>
        /// 关闭时间
        /// </summary>
        public DateTime? CloseDateTime { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [Required(ErrorMessage = "请输入备注")]
        public string Remark { get; set; }
    }
}