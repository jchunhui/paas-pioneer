﻿using System;
using Paas.Pioneer.Domain.Shared.ModelValidation;

namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.Package.Dto.Output
{
    /// <summary>
    /// 获取套餐管理分页
    /// </summary>
    public class GetPackagePageListOutput
    {
        /// <summary>
        /// 
        /// </summary>
        public Guid Id { get; set; }

        
        /// <summary>
        /// 运营商Id
        /// </summary>
        public Guid CarrierOperatorId { get; set; }

        /// <summary>
        /// 套餐类型
        /// </summary>
        public string DictionaryName { get; set; }

        /// <summary>
        /// 运营商套餐系列
        /// </summary>
        public Guid CarrierOperatorPackageDictionaryId { get; set; }


        /// <summary>
        /// 充值类型
        /// </summary>
        public Guid RechargeDictionaryId { get; set; }


        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }


        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }


        /// <summary>
        /// 套餐类型
        /// </summary>
        public Guid DictionaryId { get; set; }


        /// <summary>
        /// 显示
        /// </summary>
        public bool IsShow { get; set; }


        /// <summary>
        /// 每月最大充值数
        /// </summary>
        public int MaxRechargeCount { get; set; }


        /// <summary>
        /// 套餐数量
        /// </summary>
        public int Count { get; set; }


        /// <summary>
        /// 成本价
        /// </summary>
        public decimal CostPrice { get; set; }


        /// <summary>
        /// 套餐金额
        /// </summary>
        public decimal PackageAmount { get; set; }


        /// <summary>
        /// 虚量（MB）
        /// </summary>
        public int EmptyFlow { get; set; }


        /// <summary>
        /// 真实流量（MB）
        /// </summary>
        public int RealFlow { get; set; }


        /// <summary>
        /// 禁用
        /// </summary>
        public bool IsDisable { get; set; }


        /// <summary>
        /// 热卖
        /// </summary>
        public bool IsHeat { get; set; }


        /// <summary>
        /// 是否开户套餐
        /// </summary>
        public bool IsOpenAccount { get; set; }


        /// <summary>
        /// 开启时间
        /// </summary>
        public DateTime? OpenDateTime { get; set; }


        /// <summary>
        /// 关闭时间
        /// </summary>
        public DateTime? CloseDateTime { get; set; }


        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }


        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? LastModificationTime { get; set; }


        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreationTime { get; set; }
        
        /// <summary>
        /// 充值类型
        /// </summary>
        public string RechargeDictionaryName { get; set; }

        /// <summary>
        /// 运营商套餐系列
        /// </summary>
        public string CarrierOperatorPackageDictionaryName { get; set; }

        /// <summary>
        /// 运营商名称
        /// </summary>
        public string CarrierOperatorName { get; set; }
    }
}