﻿using Paas.Pioneer.Domain.Shared.Dto.Input;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;
using Paas.Pioneer.Domain.Shared.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.Package.Dto.Output;
using Paas.Pioneer.IOTNetworkCard.Application.Contracts.Package.Dto.Input;

namespace Paas.Pioneer.IOTNetworkCard.Application.Contracts.Package
{
    /// <summary>
    /// 套餐管理接口
    /// </summary>
    public interface IPackageService : IApplicationService
    {
        /// <summary>
        /// 查询套餐管理
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        Task<GetPackageOutput> GetAsync(Guid id);

        /// <summary>
        /// 查询分页套餐管理
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        Task<Page<GetPackagePageListOutput>> GetPageListAsync(PageInput<GetPackagePageListInput> model);

        /// <summary>
        /// 新增套餐管理
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        Task AddAsync(AddPackageInput input);

        /// <summary>
        /// 修改套餐管理
        /// </summary>
        /// <param name="input">入参</param>
        /// <returns></returns>
        Task UpdateAsync(UpdatePackageInput input);

        /// <summary>
        /// 删除套餐管理
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        Task DeleteAsync(Guid id);

        /// <summary>
        /// 批量删除套餐管理
        /// </summary>
        /// <param name="ids">主键集合</param>
        /// <returns></returns>
        Task BatchSoftDeleteAsync(IEnumerable<Guid> ids);
    }
}