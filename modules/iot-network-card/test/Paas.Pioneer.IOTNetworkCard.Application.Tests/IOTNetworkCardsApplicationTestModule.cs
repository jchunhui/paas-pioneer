﻿using Paas.Pioneer.IOTNetworkCard.Application;
using Paas.Pioneer.IOTNetworkCard.Domain.Tests;
using Volo.Abp.Modularity;

namespace Paas.Pioneer.IOTNetworkCard.Application.Tests
{
    [DependsOn(
        typeof(IOTNetworkCardsApplicationModule),
        typeof(IOTNetworkCardsDomainTestModule)
        )]
    public class IOTNetworkCardsApplicationTestModule : AbpModule
    {

    }
}