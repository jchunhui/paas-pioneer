﻿using Paas.Pioneer.IOTNetworkCard.EntityFrameworkCore.Tests.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace Paas.Pioneer.IOTNetworkCard.Domain.Tests
{
    [DependsOn(
        typeof(IOTNetworkCardsEntityFrameworkCoreTestModule)
        )]
    public class IOTNetworkCardsDomainTestModule : AbpModule
    {

    }
}