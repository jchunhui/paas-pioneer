﻿using Paas.Pioneer.IOTNetworkCards;
using Volo.Abp;

namespace Paas.Pioneer.IOTNetworkCard.EntityFrameworkCore.Tests.EntityFrameworkCore
{
    public abstract class IOTNetworkCardsEntityFrameworkCoreTestBase : IOTNetworkCardsTestBase<IOTNetworkCardsEntityFrameworkCoreTestModule>
    {

    }
}
