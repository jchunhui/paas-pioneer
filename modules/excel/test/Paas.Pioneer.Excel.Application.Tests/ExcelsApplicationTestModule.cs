﻿using Paas.Pioneer.Excel.Application;
using Paas.Pioneer.Excel.Domain.Tests;
using Volo.Abp.Modularity;

namespace Paas.Pioneer.Excel.Application.Tests
{
    [DependsOn(
        typeof(ExcelsApplicationModule),
        typeof(ExcelsDomainTestModule)
        )]
    public class ExcelsApplicationTestModule : AbpModule
    {

    }
}