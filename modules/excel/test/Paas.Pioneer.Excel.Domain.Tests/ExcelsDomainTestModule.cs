﻿using Paas.Pioneer.Excel.EntityFrameworkCore.Tests.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace Paas.Pioneer.Excel.Domain.Tests
{
    [DependsOn(
        typeof(ExcelsEntityFrameworkCoreTestModule)
        )]
    public class ExcelsDomainTestModule : AbpModule
    {

    }
}