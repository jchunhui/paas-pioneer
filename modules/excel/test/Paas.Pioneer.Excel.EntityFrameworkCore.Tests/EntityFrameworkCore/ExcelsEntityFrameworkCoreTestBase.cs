﻿using Paas.Pioneer.Excels;
using Volo.Abp;

namespace Paas.Pioneer.Excel.EntityFrameworkCore.Tests.EntityFrameworkCore
{
    public abstract class ExcelsEntityFrameworkCoreTestBase : ExcelsTestBase<ExcelsEntityFrameworkCoreTestModule>
    {

    }
}
