﻿using Paas.Pioneer.Excel.Application.Contracts;
using Paas.Pioneer.Excel.EntityFrameworkCore.EntityFrameworkCore;
using Volo.Abp.Autofac;
using Volo.Abp.Modularity;

namespace Paas.Pioneer.Excel.DbMigrator
{
    [DependsOn(
        typeof(AbpAutofacModule),
        typeof(ExcelsEntityFrameworkCoreModule),
        typeof(ExcelsApplicationContractsModule)
        )]
    public class ExcelsDbMigratorModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
           
        }
    }
}
