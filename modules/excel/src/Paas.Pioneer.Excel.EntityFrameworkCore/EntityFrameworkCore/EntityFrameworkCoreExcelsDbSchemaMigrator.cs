﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Paas.Pioneer.Excel.Domain.Data;
using Volo.Abp.DependencyInjection;

namespace Paas.Pioneer.Excel.EntityFrameworkCore.EntityFrameworkCore
{
    public class EntityFrameworkCoreExcelsDbSchemaMigrator
        : IExcelsDbSchemaMigrator, ITransientDependency
    {
        private readonly IServiceProvider _serviceProvider;

        public EntityFrameworkCoreExcelsDbSchemaMigrator(
            IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task MigrateAsync()
        {
            /* We intentionally resolving the ExcelsDbContext
             * from IServiceProvider (instead of directly injecting it)
             * to properly get the connection string of the current tenant in the
             * current scope.
             */

            await _serviceProvider
                .GetRequiredService<ExcelsDbContext>()
                .Database
                .MigrateAsync();
        }
    }
}
