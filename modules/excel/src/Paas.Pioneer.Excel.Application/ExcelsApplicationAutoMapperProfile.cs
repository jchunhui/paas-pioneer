﻿using AutoMapper;

namespace Paas.Pioneer.Excel.Application
{
    public class ExcelsApplicationAutoMapperProfile : Profile
    {
        public ExcelsApplicationAutoMapperProfile()
        {
            /* You can configure your AutoMapper mapping configuration here.
             * Alternatively, you can split your mapping configurations
             * into multiple profile classes for a better organization. */
        }
    }
}
