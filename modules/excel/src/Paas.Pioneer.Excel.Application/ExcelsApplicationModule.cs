﻿using Paas.Pioneer.Excel.Application.Contracts;
using Paas.Pioneer.Excel.Domain;
using Volo.Abp.AutoMapper;
using Volo.Abp.Modularity;
using Volo.Abp.TenantManagement;

namespace Paas.Pioneer.Excel.Application
{
    [DependsOn(
        typeof(ExcelsDomainModule),
        typeof(ExcelsApplicationContractsModule),
        typeof(AbpTenantManagementApplicationModule)
        )]
    public class ExcelsApplicationModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpAutoMapperOptions>(options =>
            {
                options.AddMaps<ExcelsApplicationModule>();
            });
        }
    }
}
