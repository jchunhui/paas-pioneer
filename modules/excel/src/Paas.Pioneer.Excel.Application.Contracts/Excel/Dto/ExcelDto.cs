﻿using Magicodes.ExporterAndImporter.Excel;

namespace Paas.Pioneer.Excel.Application.Contracts.Excel.Dto;

public class ExcelDto
{
    [ExcelImporter(SheetName = "网卡信息")] public NetworkCardDto NetworkCardDto { get; set; }

    [ExcelImporter(SheetName = "1月份收入")] public IncomeDto JanuaryIncomeDto { get; set; }

    [ExcelImporter(SheetName = "1月份支出")] public ExpenditureDto JanuaryExpenditureDto { get; set; }

    [ExcelImporter(SheetName = "2月份收入")] public IncomeDto FebruaryIncomeDto { get; set; }

    [ExcelImporter(SheetName = "2月份支出")] public ExpenditureDto FebruaryExpenditureDto { get; set; }

    [ExcelImporter(SheetName = "3月份收入")] public IncomeDto MarchIncomeDto { get; set; }

    [ExcelImporter(SheetName = "3月份支出")] public ExpenditureDto MarchExpenditureDto { get; set; }

    [ExcelImporter(SheetName = "4月份收入")] public IncomeDto AprilIncomeDto { get; set; }

    [ExcelImporter(SheetName = "4月份支出")] public ExpenditureDto AprilExpenditureDto { get; set; }

    [ExcelImporter(SheetName = "5月份收入")] public IncomeDto MayIncomeDto { get; set; }

    [ExcelImporter(SheetName = "5月份支出")] public ExpenditureDto MayExpenditureDto { get; set; }

    [ExcelImporter(SheetName = "6月份收入")] public IncomeDto JuneIncomeDto { get; set; }

    [ExcelImporter(SheetName = "6月份支出")] public ExpenditureDto JuneExpenditureDto { get; set; }

    [ExcelImporter(SheetName = "7月份收入")] public IncomeDto JulyIncomeDto { get; set; }

    [ExcelImporter(SheetName = "7月份支出")] public ExpenditureDto JulyExpenditureDto { get; set; }

    [ExcelImporter(SheetName = "8月份收入")] public IncomeDto AugustIncomeDto { get; set; }

    [ExcelImporter(SheetName = "8月份支出")] public ExpenditureDto AugustExpenditureDto { get; set; }

    [ExcelImporter(SheetName = "9月份收入")] public IncomeDto SeptemberIncomeDto { get; set; }

    [ExcelImporter(SheetName = "9月份支出")] public ExpenditureDto SeptemberExpenditureDto { get; set; }

    [ExcelImporter(SheetName = "10月份收入")] public IncomeDto OctoberIncomeDto { get; set; }

    [ExcelImporter(SheetName = "10月份支出")] public ExpenditureDto OctoberExpenditureDto { get; set; }

    [ExcelImporter(SheetName = "11月份收入")] public IncomeDto NovemberIncomeDto { get; set; }

    [ExcelImporter(SheetName = "11月份支出")] public ExpenditureDto NovemberExpenditureDto { get; set; }

    [ExcelImporter(SheetName = "12月份收入")] public IncomeDto DecemberIncomeDto { get; set; }

    [ExcelImporter(SheetName = "12月份支出")] public ExpenditureDto DecemberExpenditureDto { get; set; }
}