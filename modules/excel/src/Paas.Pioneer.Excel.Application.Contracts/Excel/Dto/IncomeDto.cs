﻿using Magicodes.ExporterAndImporter.Core;

namespace Paas.Pioneer.Excel.Application.Contracts.Excel.Dto;

public class IncomeDto
{
    // [ImporterHeader(Name = "接入号")] public string PhoneNumber { get; set; }

    [ImporterHeader(Name = "ICCID号")] public string ICCID { get; set; }

    // [ImporterHeader(Name = "虚拟号")] public string VirtualNo { get; set; }

    [ImporterHeader(Name = "下单金额")] public decimal PlaceOrderAmount { get; set; }

    [ImporterHeader(Name = "数量")] public int Count { get; set; }
}