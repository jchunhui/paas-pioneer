﻿using Magicodes.ExporterAndImporter.Core;

namespace Paas.Pioneer.Excel.Application.Contracts.Excel.Dto;

public class LiRunDto
{
    public int Month { get; set; }
    
    public string Income { get; set; }

    public string Expenditure { get; set; }

    public string Profit { get; set; }

    public string TotalFlow { get; set; }
}