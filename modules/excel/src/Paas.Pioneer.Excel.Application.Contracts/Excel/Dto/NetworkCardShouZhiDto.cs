﻿using Magicodes.ExporterAndImporter.Core;

namespace Paas.Pioneer.Excel.Application.Contracts.Excel.Dto
{
    public class NetworkCardShouZhiDto : NetworkCardDto
    {
        [ExporterHeader(DisplayName  = "1月份收入")]
        public decimal JanuaryIncome { get; set; }

        [ExporterHeader(DisplayName  = "1月份支出")]
        public decimal JanuaryExpenditure { get; set; }

        [ExporterHeader(DisplayName  = "1月份利润")]
        public decimal JanuaryProfit => JanuaryIncome - JanuaryExpenditure;

        [ExporterHeader(DisplayName  = "1月份总流量（GB）")]
        public decimal JanuaryTotalFlow { get; set; }

        [ExporterHeader(DisplayName  = "2月份收入")]
        public decimal FebruaryIncome { get; set; }

        [ExporterHeader(DisplayName  = "2月份支出")]
        public decimal FebruaryExpenditure { get; set; }

        [ExporterHeader(DisplayName  = "2月份利润")]
        public decimal FebruaryProfit => FebruaryIncome - FebruaryExpenditure;

        [ExporterHeader(DisplayName  = "2月份总流量（GB）")]
        public decimal FebruaryTotalFlow { get; set; }

        [ExporterHeader(DisplayName  = "3月份收入")]
        public decimal MarchIncome { get; set; }

        [ExporterHeader(DisplayName  = "3月份支出")]
        public decimal MarchExpenditure { get; set; }

        [ExporterHeader(DisplayName  = "3月份利润")]
        public decimal MarchProfit => MarchIncome - MarchExpenditure;

        [ExporterHeader(DisplayName  = "3月份总流量（GB）")]
        public decimal MarchTotalFlow { get; set; }

        [ExporterHeader(DisplayName  = "4月份收入")]
        public decimal AprilIncome { get; set; }

        [ExporterHeader(DisplayName  = "4月份支出")]
        public decimal AprilExpenditure { get; set; }

        [ExporterHeader(DisplayName  = "4月份利润")]
        public decimal AprilProfit => AprilIncome - AprilExpenditure;

        [ExporterHeader(DisplayName  = "4月份总流量（GB）")]
        public decimal AprilTotalFlow { get; set; }

        [ExporterHeader(DisplayName  = "5月份收入")]
        public decimal MayIncome { get; set; }

        [ExporterHeader(DisplayName  = "5月份支出")]
        public decimal MayExpenditure { get; set; }

        [ExporterHeader(DisplayName  = "5月份利润")]
        public decimal MayProfit => MayIncome - MayExpenditure;

        [ExporterHeader(DisplayName  = "5月份总流量（GB）")]
        public decimal MayTotalFlow { get; set; }

        [ExporterHeader(DisplayName  = "6月份收入")]
        public decimal JuneIncome { get; set; }

        [ExporterHeader(DisplayName  = "6月份支出")]
        public decimal JuneExpenditure { get; set; }

        [ExporterHeader(DisplayName  = "6月份利润")]
        public decimal JuneProfit => JuneIncome - JuneExpenditure;

        [ExporterHeader(DisplayName  = "6月份总流量（GB）")]
        public decimal JuneTotalFlow { get; set; }

        [ExporterHeader(DisplayName  = "7月份收入")]
        public decimal JulyIncome { get; set; }

        [ExporterHeader(DisplayName  = "7月份支出")]
        public decimal JulyExpenditure { get; set; }

        [ExporterHeader(DisplayName  = "7月份利润")]
        public decimal JulyProfit => JulyIncome - JulyExpenditure;

        [ExporterHeader(DisplayName  = "7月份总流量（GB）")]
        public decimal JulyTotalFlow { get; set; }

        [ExporterHeader(DisplayName  = "8月份收入")]
        public decimal AugustIncome { get; set; }

        [ExporterHeader(DisplayName  = "8月份支出")]
        public decimal AugustExpenditure { get; set; }

        [ExporterHeader(DisplayName  = "8月份利润")]
        public decimal AugustProfit => AugustIncome - AugustExpenditure;

        [ExporterHeader(DisplayName  = "8月份总流量（GB）")]
        public decimal AugustTotalFlow { get; set; }

        [ExporterHeader(DisplayName  = "9月份收入")]
        public decimal SeptemberIncome { get; set; }

        [ExporterHeader(DisplayName  = "9月份支出")]
        public decimal SeptemberExpenditure { get; set; }

        [ExporterHeader(DisplayName  = "9月份利润")]
        public decimal SeptemberProfit => SeptemberIncome - SeptemberExpenditure;

        [ExporterHeader(DisplayName  = "9月份总流量（GB）")]
        public decimal SeptemberTotalFlow { get; set; }

        [ExporterHeader(DisplayName  = "10月份收入")]
        public decimal OctoberIncome { get; set; }

        [ExporterHeader(DisplayName  = "10月份支出")]
        public decimal OctoberExpenditure { get; set; }

        [ExporterHeader(DisplayName  = "10月份利润")]
        public decimal OctoberProfit => OctoberIncome - OctoberExpenditure;

        [ExporterHeader(DisplayName  = "10月份总流量（GB）")]
        public decimal OctoberTotalFlow { get; set; }

        [ExporterHeader(DisplayName  = "11月份收入")]
        public decimal NovemberIncome { get; set; }

        [ExporterHeader(DisplayName  = "11月份支出")]
        public decimal NovemberExpenditure { get; set; }

        [ExporterHeader(DisplayName  = "11月份利润")]
        public decimal NovemberProfit => NovemberIncome - NovemberExpenditure;

        [ExporterHeader(DisplayName  = "11月份总流量（GB）")]
        public decimal NovemberTotalFlow { get; set; }

        [ExporterHeader(DisplayName  = "12月份收入")]
        public decimal DecemberIncome { get; set; }

        [ExporterHeader(DisplayName  = "12月份支出")]
        public decimal DecemberExpenditure { get; set; }

        [ExporterHeader(DisplayName  = "12月份利润")]
        public decimal DecemberProfit => DecemberIncome - DecemberExpenditure;

        [ExporterHeader(DisplayName  = "12月份总流量（GB）")]
        public decimal DecemberTotalFlow { get; set; }

        [ExporterHeader(DisplayName  = "12个月总收入")]
        public decimal TotalIncome12Months => JanuaryIncome + FebruaryIncome + MarchIncome + AprilIncome + MayIncome +
                                              JuneIncome + JulyIncome + AugustIncome + SeptemberIncome + OctoberIncome +
                                              NovemberIncome + DecemberIncome;

        [ExporterHeader(DisplayName  = "12个月总支出")]
        public decimal TotalExpenses12Months =>
            JanuaryExpenditure + FebruaryExpenditure + MarchExpenditure + AprilExpenditure + MayExpenditure +
            JuneExpenditure + JulyExpenditure + AugustExpenditure + SeptemberExpenditure + OctoberExpenditure +
            NovemberExpenditure + DecemberExpenditure;

        [ExporterHeader(DisplayName  = "12个月总利润")]
        public decimal TotalProfit12Months => JanuaryProfit + FebruaryProfit +
                                              MarchProfit + AprilProfit + MayProfit + JuneProfit + AugustProfit +
                                              SeptemberProfit + OctoberProfit + NovemberProfit + DecemberProfit;

        [ExporterHeader(DisplayName  = "12个月总流量")]
        public decimal TotalFlowOver12Months => JanuaryTotalFlow +
                                                FebruaryTotalFlow + MarchTotalFlow + AprilTotalFlow + MayTotalFlow +
                                                JuneTotalFlow + JulyTotalFlow + AugustTotalFlow + SeptemberTotalFlow +
                                                OctoberTotalFlow + NovemberTotalFlow + DecemberTotalFlow;
    }
}
