﻿using Magicodes.ExporterAndImporter.Core;

namespace Paas.Pioneer.Excel.Application.Contracts.Excel.Dto;

public class NetworkCardDto
{
    [ImporterHeader(Name = "编号")]
    [ExporterHeader(DisplayName = "编号", ColumnIndex = 1)]
    public string Number { get; set; }

    [ImporterHeader(Name = "接入电话")]
    [ExporterHeader(DisplayName = "接入电话", ColumnIndex = 2)]
    public string PhoneNumber { get; set; }

    [ImporterHeader(Name = "ICCID号")]
    [ExporterHeader(DisplayName = "ICCID号", ColumnIndex = 3)]
    public string ICCID { get; set; }

    [ImporterHeader(Name = "虚拟号")]
    [ExporterHeader(DisplayName = "虚拟号", ColumnIndex = 4)]
    public string VirtualNo { get; set; }

    [ImporterHeader(Name = "用户号码")]
    [ExporterHeader(DisplayName = "用户号码", ColumnIndex = 5)]
    public string SubscriberNumber { get; set; }

    [ImporterHeader(Name = "IMEI")]
    [ExporterHeader(DisplayName = "IMEI", ColumnIndex = 6)]
    public string IMEI { get; set; }

    [ImporterHeader(Name = "钱包余额")]
    [ExporterHeader(DisplayName = "钱包余额", ColumnIndex = 7)]
    public string WalletBalance { get; set; }

    [ImporterHeader(Name = "开卡公司")]
    [ExporterHeader(DisplayName = "开卡公司", ColumnIndex = 8)]
    public string Developer { get; set; }

    [ImporterHeader(Name = "到期时间")]
    [ExporterHeader(DisplayName = "到期时间", ColumnIndex = 9)]
    public string ExpirationTime { get; set; }

    [ImporterHeader(Name = "运营商名称")]
    [ExporterHeader(DisplayName = "运营商名称", ColumnIndex = 10)]
    public string CarrierName { get; set; }

    [ImporterHeader(Name = "基础套餐")]
    [ExporterHeader(DisplayName = "基础套餐", ColumnIndex = 11)]
    public string BasicPackage { get; set; }

    [ImporterHeader(Name = "当前套餐")]
    [ExporterHeader(DisplayName = "当前套餐", ColumnIndex = 12)]
    public string CurrentPackage { get; set; }

    [ImporterHeader(Name = "卡类型")]
    [ExporterHeader(DisplayName = "卡类型", ColumnIndex = 13)]
    public string CardType { get; set; }

    [ImporterHeader(Name = "分销商姓名")]
    [ExporterHeader(DisplayName = "分销商姓名", ColumnIndex = 14)]
    public string NameDistributor { get; set; }

    [ImporterHeader(Name = "代理商层级")]
    [ExporterHeader(DisplayName = "代理商层级", ColumnIndex = 15)]
    public string LevelAgents { get; set; }

    [ImporterHeader(Name = "卡状态")]
    [ExporterHeader(DisplayName = "卡状态", ColumnIndex = 16)]
    public string CardStatus { get; set; }

    [ImporterHeader(Name = "激活时间")]
    [ExporterHeader(DisplayName = "激活时间", ColumnIndex = 17)]
    public string ActivationTime { get; set; }

}