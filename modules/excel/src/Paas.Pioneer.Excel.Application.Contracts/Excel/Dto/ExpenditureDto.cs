﻿using Magicodes.ExporterAndImporter.Core;

namespace Paas.Pioneer.Excel.Application.Contracts.Excel.Dto;

public class ExpenditureDto
{
    // [ImporterHeader(Name = "号码")] public string PhoneNumber { get; set; }

    [ImporterHeader(Name = "ICCID")] public string ICCID { get; set; }

    [ImporterHeader(Name = "总流量（GB）")] public string TotalflowGB { get; set; }

    [ImporterHeader(Name = "折后费用")] public decimal DiscountedCost { get; set; }
}