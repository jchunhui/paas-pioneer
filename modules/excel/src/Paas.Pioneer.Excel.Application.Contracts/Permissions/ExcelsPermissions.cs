﻿namespace Paas.Pioneer.Excel.Application.Contracts.Permissions
{
    public static class ExcelsPermissions
    {
        public const string GroupName = "Excels";

        //Add your own permission names. Example:
        //public const string MyPermission1 = GroupName + ".MyPermission1";
    }
}