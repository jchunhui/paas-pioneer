﻿using Volo.Abp.Authorization.Permissions;

namespace Paas.Pioneer.Excel.Application.Contracts.Permissions
{
    public class ExcelsPermissionDefinitionProvider : PermissionDefinitionProvider
    {
        public override void Define(IPermissionDefinitionContext context)
        {
            var myGroup = context.AddGroup(ExcelsPermissions.GroupName);
            //Define your own permissions here. Example:
            //myGroup.AddPermission(ExcelsPermissions.MyPermission1, L("Permission:MyPermission1"));
        }
    }
}
