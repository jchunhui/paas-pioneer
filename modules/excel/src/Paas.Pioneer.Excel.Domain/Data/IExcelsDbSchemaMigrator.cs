﻿using System.Threading.Tasks;

namespace Paas.Pioneer.Excel.Domain.Data
{
    public interface IExcelsDbSchemaMigrator
    {
        Task MigrateAsync();
    }
}
