﻿using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;

namespace Paas.Pioneer.Excel.Domain.Data
{
    /* This is used if database provider does't define
     * IExcelsDbSchemaMigrator implementation.
     */
    public class NullExcelsDbSchemaMigrator : IExcelsDbSchemaMigrator, ITransientDependency
    {
        public Task MigrateAsync()
        {
            return Task.CompletedTask;
        }
    }
}