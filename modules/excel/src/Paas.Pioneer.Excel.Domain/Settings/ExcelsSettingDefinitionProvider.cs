﻿using Volo.Abp.Settings;

namespace Paas.Pioneer.Excel.Domain.Settings
{
    public class ExcelsSettingDefinitionProvider : SettingDefinitionProvider
    {
        public override void Define(ISettingDefinitionContext context)
        {
            //Define your own settings here. Example:
            //context.Add(new SettingDefinition(ExcelsSettings.MySetting1));
        }
    }
}
