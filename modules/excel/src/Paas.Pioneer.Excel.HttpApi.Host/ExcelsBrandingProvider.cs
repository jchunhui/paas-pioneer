﻿using Volo.Abp.DependencyInjection;
using Volo.Abp.Ui.Branding;

namespace Paas.Pioneer.Excel.HttpApi.Host
{
    [Dependency(ReplaceServices = true)]
    public class ExcelsBrandingProvider : DefaultBrandingProvider
    {
        public override string AppName => "Excels";
    }
}
