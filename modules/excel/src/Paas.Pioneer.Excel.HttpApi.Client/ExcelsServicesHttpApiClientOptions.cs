﻿using System;
using System.Net.Http;

namespace Paas.Pioneer.Excel.HttpApi.Client
{
    public class ExcelsServicesHttpApiClientOptions
    {
        public string RemoteServiceName { get; set; } = "Excels";
        public string RemoteSectionUrl
        {
            get
            {
                return $"RemoteServices:{RemoteServiceName}:BaseUrl";
            }
        }

        public Func<DelegatingHandler> DelegatingHandlerFunc { get; set; }

        public ExcelsServicesHttpApiClientOptions()
        {

        }
    }
}
