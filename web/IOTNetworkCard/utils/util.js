const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

const timeGreeting = date => {
  const hour = date.getHours()

  if (hour > 0 && hour < 6) {
    return '现在是凌晨' + hour + '点,注意休息呗';
  } else if (hour >= 6 && hour < 11) {
    return 'Hi,早上好吖';
  } else if (hour >= 11 && hour < 13) {
    return 'Hi,中午好吖';
  } else if (hour >= 13 && hour < 18) {
    return 'Hi,下午好吖';
  } else {
    return 'Hi,晚上好吖';
  }
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}

/**
 * 获得元素上的绑定的值
 * params：
 * event 元素的标识
 * key dataset的键
 */
const getDataSet = (event, key) => {
  return event.currentTarget.dataset[key];
}

/*
 * 提示窗口
 * params:
 * title - {string}标题
 * content - {string}内容
 * callback - 点击确定后需要执行的操作
 */
const showTips = (title, content, callback) => {
  wx.showModal({
    title: title,
    content: content,
    showCancel: false,
    success: function(res) {
      callback && callback(res);
    }
  });
}

/*
 * 操作确认窗口
 * params:
 * title - {string}标题
 * content - {string}内容
 * callback - 点击确定后需要执行的操作
 */
const confirmOperation = (title, content, callback) => {
  wx.showModal({
    title: title,
    content: content,
    showCancel: true,
    success: function(res) {
      callback && callback(res);
    }
  });
}

/**
 * 获取页面组件对应的信息
 * params:
 * params.name 节点的标识
 * params.success 成功后回调函数
 */
const getComponentInfo = (params) => {
  uni.createSelectorQuery().in(params.component).select(params.name).boundingClientRect((rect) => {
    params.success && params.success(rect);
  }).exec();
}

/**
 * 检查订阅消息的设置
 * @param {*} tmplID 
 */
const checkSubscribeSetting = (tmplID) => {
  return new Promise((resolve, reject) => {
    let is_allow_flag = true;
    uni.getSetting({
      withSubscriptions: true,
      success: (res) => {
        // console.log(res.subscriptionsSetting);
        if (!res.subscriptionsSetting || !res.subscriptionsSetting.mainSwitch) {
          uni.showToast({
            title: '在设置中打开订阅消息开关',
            icon: 'none'
          });
          resolve([]);
          return;
        }
        if (!res.subscriptionsSetting.itemSettings) {
          is_allow_flag = false;
        } else {
          tmplID.forEach((value) => {
            if (!res.subscriptionsSetting.itemSettings[value] || res.subscriptionsSetting.itemSettings[
                value] !== 'accept') {
              is_allow_flag = false;
              return;
            }
          })
        }
        if (!is_allow_flag) {
          // console.log(is_allow_flag);
          let allow_ids = [];
          uni.requestSubscribeMessage({
            tmplIds: tmplID,
            success: (res) => {
              //   console.log('request subscript success', res);
              tmplID.forEach((value) => {
                if (res[value] === 'accept') {
                  allow_ids.push(value)
                }
              })
              resolve(allow_ids);
            },
            fail: (res) => {
              //   console.log('request subscript fail', res);
              resolve([]);
            }
          });
        } else {
          resolve(tmplID);
        }
      },
      fail: (res) => {
        // console.log('fail', res);
        resolve([]);
      }
    })
  })
}

module.exports = {
  formatTime,
  timeGreeting,
  getDataSet,
  showTips,
  confirmOperation,
  getComponentInfo,
  checkSubscribeSetting
}
