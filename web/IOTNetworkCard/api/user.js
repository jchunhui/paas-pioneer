import {
	request
} from '@/utils/request'

export function login(data) {
	return request({
		url: 'login',
		method: 'post',
		data: data
	})
}

export function callbackUser(data) {
	return request({
		url: 'user/oauth2/callback-user',
		method: 'post',
		data: data
	})
}

export function refreshToken(data) {
	return request({
		url: 'login/refresh-token',
		method: 'post',
		data: data
	})
}

export function mobileRegister(data) {
	return request({
		url: 'login/mobile-register',
		method: 'post',
		data: data
	})
}

export function account(data) {
	return request({
		url: 'login/account',
		method: 'post',
		data: data
	})
}

export function syncInfo(data) {
	return request({
		url: 'user/sync-info',
		method: 'put',
		data: data
	})
}

export function updateAvatar(data) {
	return request({
		url: 'user/avatar',
		method: 'put',
		data: data
	})
}

export function updateName(data) {
	return request({
		url: 'user/name',
		method: 'put',
		data: data
	})
}

export function updateMobile(data) {
	return request({
		url: 'user/user/mobile',
		method: 'put',
		data: data
	})
}

export function bindICCID(data) {
	return request({
		url: 'iot/networkCard/bind-iccid/' + data.ICCID,
		method: 'post'
	})
}

export function updatePassword(data) {
	return request({
		url: 'user/password',
		method: 'put',
		data: data
	})
}
