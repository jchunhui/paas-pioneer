import { request } from '@/utils/request'

export function authCode(data) {
  return request({
    url: 'user/AuthCode',
    method: 'get',
    data: data
  })
}