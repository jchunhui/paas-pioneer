export const BASE_URL = 'http://127.0.0.1:9080/'

export const MP_API_URL = BASE_URL + 'api/'

export const TOKEN_NAME = 'tn_website_open_token'

export const APP_ID = "wxd184724c1d716f0f";

export const REDIRECT_URL = "https://u3255f5045.oicp.vip/api/user/oauth2/callback-user";

export const SHOPPING_CART_STORAGE_KEY = 'tn_website_open_shop_shopping_cart'
export const ORDER_STORAGE_KEY = 'tn_website_open_shop_order'
